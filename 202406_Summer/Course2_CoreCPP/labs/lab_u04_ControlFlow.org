# -*- mode: org -*-

#+ATTR_HTML: :class action-item
#+NAME: content
#+BEGIN_HTML
*What we're gonna do:*
- Practice with *if statements*
- Practice with *switch statements*
- Practice with *for loops*

*Using the command line:*

Navigate to your folder using the =cd= (change directory) command:
#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
$ cd FOLDERNAME
#+END_SRC


View what files are in the current folder using the =ls= (list) command:
#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
$ ls
graded_program_1  practice2_ifelse     practice5_switch
graded_program_2  practice3_ifelseif   practice6_while_program
practice1_if      practice4_operators
#+END_SRC



Build your source code using the =g++= compiler:
#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
g++ SOURCE.cpp
#+END_SRC


Run your program using =./= (Linux/Mac) or =.\= (Windows):

- Windows:
#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
.\a.exe
#+END_SRC

- Linux/Mac:
#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
./a.out
#+END_SRC

#+END_HTML

-----------------------------------------------------------------------------------------
#+INCLUDE: "starter_code/u04_ControlFlow/practice1_if/requirements.org"

-----------------------------------------------------------------------------------------
#+INCLUDE: "starter_code/u04_ControlFlow/practice2_ifelse/requirements.org"

-----------------------------------------------------------------------------------------
#+INCLUDE: "starter_code/u04_ControlFlow/practice3_ifelseif/requirements.org"

-----------------------------------------------------------------------------------------
#+INCLUDE: "starter_code/u04_ControlFlow/practice4_operators/requirements.org"

-----------------------------------------------------------------------------------------
#+INCLUDE: "starter_code/u04_ControlFlow/practice5_switch/requirements.org"

-----------------------------------------------------------------------------------------
#+INCLUDE: "starter_code/u04_ControlFlow/practice6_while_program/requirements.org"

-----------------------------------------------------------------------------------------
#+INCLUDE: "starter_code/u04_ControlFlow/graded_program_1/requirements.org"

-----------------------------------------------------------------------------------------
#+INCLUDE: "starter_code/u04_ControlFlow/graded_program_2/requirements.org"

-----------------------------------------------------------------------------------------
* *Turning in your work*

1. Go to your *GitLab* webpage.
2. Select the folder for this unit in the main view. Locate the file you want to update and click on it.
3. Click on the blue "*Edit*" button, then click "*Edit single file*".

   [[file:../../reference/images/gitlab_web_editfile2.png]]

4. COPY your work off your computer and PASTE it into the web editor on GitLab.
   Scroll to the bottom of the page and click "*Commit changes*" to finish up.

5. Go back to the unit folder and COPY the URL for this unit.

   [[file:../../reference/images/gitlab_web_editfile5b.png]]

6. On Canvas, locate the assignment. Click "*Start assignment*".

7. Paste the copied URL into the box and click "*Submit assignment*".

   [[file:../../reference/images/canvas-submit.png]]
