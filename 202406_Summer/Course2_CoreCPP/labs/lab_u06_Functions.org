# -*- mode: org -*-

#+ATTR_HTML: :class action-item
#+NAME: content
#+BEGIN_HTML
*What we're gonna do:*
- Practice writing functions with a void return type and no parameters
- Practice writing functions with a void return type but with parameters
- Practice writing functions that return data but have no parameters
- Practice writing functions that return data and with parameters

- *To build your code in the terminal:*
  - If the program has .h and .cpp files: =g++ *.h *.cpp=
  - If the program only has .cpp files: =g++ *.cpp=
- *To run the program:*
  - Windows: =.\a.exe=
  - Linux/Mac: =./a.out=

#+END_HTML

----------------------------------------------------------------
* *Practice programs*

Program documentation is inside the source files.

| Practice                                 | Location                               | Starter code |
|------------------------------------------+----------------------------------------+--------------|
| practice 1: DisplayMenu function         | =u06_Functions/practice1_NoInNoOut/=   | [[file:starter_code/u06_Functions/practice1_NoInNoOut/program1.cpp][program1.cpp]] |
| practice 2: DisplayFormattedUSD function | =u06_Functions/practice2_YesInNoOut/=  | [[file:starter_code/u06_Functions/practice2_YesInNoOut/program2.cpp][program2.cpp]] |
| practice 3: GetTaxPercent function       | =u06_Functions/practice3_NoInYesOut/=  | [[file:starter_code/u06_Functions/practice3_NoInYesOut/program3.cpp][program3.cpp]] |
| practice 4: GetPricePlusTax function     | =u06_Functions/practice4_YesInYesOut/= | [[file:starter_code/u06_Functions/practice4_YesInYesOut/program4.cpp][program4.cpp]] |

----------------------------------------------------------------
* *Graded program: Purchase program*
#+INCLUDE: "starter_code/u06_Functions/graded_program/requirements.org"

-----------------------------------------------------------------------------------------
* *Turning in your work*

Go to the *GitLab* webpage, locate the file you want to update, and click the blue "*Edit*" button,
then click "*Edit single file*". Paste in your changes and click the "*Commit changes*" at the bottom of the page.
Copy the URL to the file or unit folder and paste it in as your submission on the Canvas assignment.
