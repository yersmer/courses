# -*- mode: org -*-





* *Before computers*

#+CAPTION: Photo by Mike: https://www.pexels.com/photo/depth-of-field-photography-of-file-arrangement-1181772/
[[file:images/tec_u04_Filesystems_pexels-mike-1181772.jpg]]

Before computers were widely available to businesses and at home,
long-term storage of information was often stored in physical folders
organized by some method (alphabetical, purpose, etc.).

Within each *folder* there would be *files*, such as contracts, filled out forms, and so on.
Anything we do on a computer now had to be done on paper before.

While working on a *file*, people would work at their desk - on their *desktop*.
That's where work in progress items would go.

If you haven't noticed the similarity yet, a lot of computer terminology about
navigating our computers come from these office environments.
When computers were first created, there was a lot of things named based on real-world
items, as a way to give an "analogy" for what the software did.

--------------------------------------------------
* *Files and folders*

** *Files*

[[file:images/tec_u04_Filesystems_File.png]]

On our computers we we *files* where we store data.
This can be any type of data - text, images, video,
music files, code files, and so on.

Each file at least has a *name* and a *size* associated with it,
and usually files are given different *extensions* to help the
Operating System understand what /kind/ of file it is.

[[file:images/tec_u04_Filesystems_FileScreenshot.png]]

| File extension | File type                                  |
|----------------+--------------------------------------------|
| .mp3           | Music file                                 |
| .csv           | Comma-separated value table                |
| .jpg           | JPEG image file (good for photos)          |
| .png           | PNG image file (good for low color images) |
| .html          | HTML document (opened by web browsers)     |

Files have *contents* - the data it is storing within.
For a plain text file, it'd just be text information:

[[file:images/tec_u04_Filesystems_TextDocument.png]]

For an image file, its contents is data about the image itself -
what color is at each position.

[[file:images/tec_u04_Filesystems_PngDocument.png]]

Some file types are technically *plaintext*, but the way their contents
are written signify different things. An HTML webpage can be opened in a
text editor:

[[file:images/tec_u04_Filesystems_HtmlDocument.png]]

And a C++ program's code can be opened in a text editor:

[[file:images/tec_u04_Filesystems_CppDocument.png]]

These are both plaintext files, but we put ".html" at the end to specify
a basic HTML webpage, or ".cpp" at the end to specify that this file is
C++ source code.

When you *double-click* a file to open it, the Operating System may
look at the file extension (.html, .cpp, .mp3, etc.) to figure out
/which program/ to open it with: A music player won't do much good with
a .html file, and an image viewer won't know how to read a .mp3 file.


** *Folders*

[[file:images/tec_u04_Filesystems_Folder.png]]

A *folder* represents a *location* on your hard drive.
A folder can contain files adn other folders as well.
The folders /within a folder/ are called *subfolders*.

All together, all the folders on your computer make up a *tree structure*...

Diagram of an example filesystem:

[[file:images/tec_u04_Filesystems_Tree.png]]

A screenshot of several folders opened up on my computer:

[[file:images/tec_u04_Filesystems_FolderTree.png]]

Folders are also known as *directories*, so when we talk about
what directory we're working in, that means what folder are we
currently working in.


--------------------------------------------------
* *The directory tree*

** *Paths to documents*

As we traverse deeper into our filesystem tree, we grow and grow our
*path*. The path is the string that shows the sequence of folders
to follow to get to where we're currently at. The path is similar to
a URL to get to a website. Both use slashes like "/" to separate between "folders".

On a Windows computer, we might access our documents with a path like this:

=C:\Users\USERNAME\Documents\=

Or on Linux and Mac, it might look like this:

=/home/USERNAME/Documents/=

[[file:images/tec_u04_Filesystems_TreeView.png]]

We can type in the path to the folder want to get to in the "address bar"
in our file explorer, or we can double-click on folders to get into each one
and go folder by folder.

It can sometimes be useful to *highlight and copy* a folder path so that
we can easily point projects to a new directory or for other reasons.


# --------------------------------------------------
# * *Filesystems on Windows, Linux, and Mac*


--------------------------------------------------
* *Discussion board*

Find the *"🧠 Tech Literacy - Filesystems"* assignment on Canvas
and participate in the discussion board!
