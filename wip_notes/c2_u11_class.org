# -*- mode: org -*-

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/rworgmode.css" />
#+HTML_HEAD: <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
#+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">
#+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script> <script>hljs.initHighlightingOnLoad();</script>

#+TITLE: Q&A: Class
#+AUTHOR: Rachel Wil Sha Singh

*What is a class?*

*What is an object?*

*What is an access modifier?*

*What is public?*

*What is private?*

*What is a member variable?*

*What is a member function?*

*How do you tell a member variable from a parameter variable when inside of a class' function?*

*How do you declare a class? (Write down example code)*

*How do you declare an object variable of some class type? (Write down example code)*

*What kind of file do class declarations go in?*

*What kind of file do class function definitions go in?*

*What does a class function definition function header look like? (it's a little different from a non-class function)*

-----
