# -*- mode: org -*-

#+TITLE: Q&A Notes: Exceptions
#+AUTHOR: Rachel Wil Sha Singh

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/rworgmode.css" />
#+HTML_HEAD: <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
#+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">
#+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script> <script>hljs.initHighlightingOnLoad();</script>

-----

1. When should throw be used?
1. When should try be used?
1. When should catch be used?
1. Write down example code of a function with a throw, then a call to that function with a wrapping try/catch.
1. Documentation URLs for the exception family: https://cplusplus.com/reference/stdexcept/ 
1. Write down example code for creating a class that inherits from an exception to create a custom exception type.
