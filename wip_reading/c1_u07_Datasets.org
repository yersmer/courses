# -*- mode: org -*-

#+TITLE: Storing lists of data (Lists / Arrays)
#+AUTHOR: Rachel Wil Sha Singh

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/rworgmode.css" />
#+HTML_HEAD: <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
#+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">
#+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script> <script>hljs.initHighlightingOnLoad();</script>

-----

[[file:images/c1_u07_Datasets_menulist.png]]

-----

* Storing lists of data

** Moving away from separate variables for /related data/

Before now, if we wanted to store a list of students
in a class (or similar kind of data), we would have to declare a bunch of
separate variables and write the same code over and over to manage it:

#+BEGIN_SRC cpp :class cpp
string student1, student2, student3;

cout << "Enter student 1 name: ";
cin >> student1;

cout << "Enter student 2 name: ";
cin >> student2;

cout << "Enter student 3 name: ";
cin >> student3;
#+END_SRC

  This would quickly become unmanagable if you were writing a program
  with tens, hundreds, or thousands of students stored in it.
  Instead, we can make use of *arrays* to store a series of
  related data together.

#+BEGIN_SRC cpp :class cpp
string students[100];
for ( int i = 0; i < 100; i++ )
{
  cout << "Enter student " << (i+1) << " name: ";
  cin >> student[i];
}
#+END_SRC

  Arrays allow us to operate on the same name (e.g., =student=),
  but addressing different *elements* of the array with an *index* number.
  This way, we can write code to act on the data /once/, just modifying
  that index to work with different pieces of data.


  *Example: student array of size 4:*

#+ATTR_HTML: :border 2 :rules all :frame border :class col-5 :style width: 100%;
| *Element* | Rai | Anuj | Rebekah | Rose |
| *Index*   |   0 |    1 |       2 |    3 |


With all the student data stored under one name (=students=), then we can access each one with its *index number*:

*Pseudocode:*

#+BEGIN_SRC pseudocode
Display( students[0] )
Display( students[1] )
Display( students[2] )
Display( students[3] )
#+END_SRC

But we could also use a *variable* to choose an index at any time:

#+BEGIN_SRC pseudocode
// Ask for the # of the favorite student
Display( "What's your favorite student?" )

// Get the user's input, store in index variable
index = InputInt()

// Display which student is the favorite
Display( students[index] )
#+END_SRC




#+ATTR_HTML: :class extra-space
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** Terminology: Lists? Arrays? Vectors?

- In *Python* we use *Lists*.
- In *C++*, we could use *Arrays*, but we are going to use *Vectors*, which are like Array++.
  - Old C++ arrays can't be resized. C++ vectors can be resized.
  - C++ Vectors are closer to Python Lists.
  - I may still use the term "Arrays" interchangibly here. You'll learn the differences in later classes, but for now, consider "Lists" / "Arrays" / "Vectors" interchangible.



#+ATTR_HTML: :class extra-space
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** Creating a list/vector

With Python, we use =[ ]= to signify an empty list. We use this with our normal
variable declaration to turn a normal variable to a list.

In C++, we declare a =vector= type object, and we have to tell it /what it's going to store/,
such as an integer, float, or string. "What it stores" goes between the less-than and greater-than symbols.

#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML

Python:
#+BEGIN_SRC python :class python
  studentList = []
#+END_SRC

#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

In C++, you'll need to have =#include <vector>= at the top of the file.

C++:
#+BEGIN_SRC cpp :class cpp
  vector<string> studentList;
#+END_SRC

#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML




#+ATTR_HTML: :class extra-space
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** Adding items to a list

[[file:images/c1_u07_Datasets_append.png]]

To add items to the end of a list, we need to either =append= (Python) or =push_back= (C++).

#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML

Python:
#+BEGIN_SRC python :class python
  studentList.append( "Kabe" );
  studentList.append( "Pixel" );
  studentList.append( "Luna" );
  studentList.append( "Korra" );
#+END_SRC

#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

C++:
#+BEGIN_SRC cpp :class cpp
  studentList.push_back( "Kabe" );
  studentList.push_back( "Pixel" );
  studentList.push_back( "Luna" );
  studentList.push_back( "Korra" );
#+END_SRC

#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML





#+ATTR_HTML: :class extra-space
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** Accessing items in a list

[[file:images/c1_u07_Datasets_access.png]]

Once items are in the list, they're stored starting at an index of *0* and going up.
The *index* is basically the *position the item is in* in the list, but we begin counting at 0 instead of 1.
This is common for /most/, but not /all/ programming languages. (Lua starts at 1.)

The individual item in the list - basically, the single variable /at some index/, is also known as an *element*.

Python:
#+BEGIN_SRC python :class python
  print( "The first student is:", studentList[0] )
  print( "The second student is:", studentList[1] )
  print( "The third student is:", studentList[2] )
  print( "The fourth student is:", studentList[3] )
#+END_SRC


C++:
#+BEGIN_SRC cpp :class cpp
  cout << "The first student is: " << studentList[0] << endl;
  cout << "The second student is: " << studentList[1] << endl;
  cout << "The third student is: " << studentList[2] << endl;
  cout << "The fourth student is: " << studentList[3] << endl;
#+END_SRC



#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*** Variable placeholders for index
Since we can access specific elements of a list now using plain-old integers (0, 1, 2, 3...),
this means we can now store a position in an *integer variable* and access items that way.
This can be useful when selecting a single item, or if we want to keep looping, doing the same
operation on each item in the list.


Python:
#+BEGIN_SRC python :class python
  index = int( input( "Who is your favorite student? " ) )
  print( "Favorite student is:", studentList[index] )
#+END_SRC


C++:
#+BEGIN_SRC cpp :class cpp
  int index;
  cout << "Who is your favorite student? ";
  cin >> index;
  cout << "Favorite student is: " << studentList[index] << endl;
#+END_SRC



#+ATTR_HTML: :class extra-space
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** Iterating over a list

[[file:images/c1_u07_Datasets_index_elements.png]]

There are two ways we can iterate over a list. Sometimes, we want the *index* of an element. Sometimes, we don't need it.

You'll want to take note of these forms and refer back to them. You don't necessarily need to have them /memorized/ at this point in time.

*** For loop 1: getting an index and element

Here we use a *counter variable*, which we usually name =i=. =i= will begin at 0, go up to 1, then 2, and so on until it hits the last index of the list of items.
=i= represents the *index* and =studentList[i]= represents the *element*.

Python:
#+BEGIN_SRC python :class python
  for i in range( len( studentList ) ):
     # Display index (i) and then the element (studentList[i]):
     print( i, studentList[i] )
#+END_SRC


C++:
#+BEGIN_SRC cpp :class cpp
  for ( int i = 0; i < studentList.size(); i++ )
  {
    // Display index (i) and then the element (studentList[i]):
    cout << i << ". " << studentList[i] << endl;
  }
#+END_SRC



*** For loop 2: getting just the element

For these loops, they use an *alias variable* - each time through the loop, the =student= placeholder is filled with the next item in the list.

Python:
#+BEGIN_SRC python :class python
  for student in studentList:
      print( student )
#+END_SRC


C++:
#+BEGIN_SRC cpp :class cpp
  for ( auto& student : studentList )
  {
    cout << student << endl;
  }
#+END_SRC



-----

