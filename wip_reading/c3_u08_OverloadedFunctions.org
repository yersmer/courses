# -*- mode: org -*-

#+TITLE: Function and constructor overloading
#+AUTHOR: Rachel Wil Sha Singh

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/rworgmode.css" />

** Function overloading

In C++, you can also write multiple functions that have the
*same name*, but a different *parameter list*.
This is known as *function overloading*.
   
Let's say you want to be able to sum numbers, and you make a version
for floats and a version for integers:
   
#+BEGIN_SRC cpp :class cpp
int Sum( int a, int b )
{
    return a + b;
}

float Sum( float a, float b )
{
    return a + b;
}
#+END_SRC
    
You can write as many versions of the function as you like, so long
as the function headers are *uniquely identifiable* to the compiler,
which means:

- The functions have a different *amount* of parameters, or
- The *data types* of the parameters are different, or
- The parameters are in a *different order* (when mixing data types).

The return type doesn't factor into the *function's uniqueness*, so make sure
that you're changing up the *parameter list*.

#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML

- Example usage - Config manager ::

Here we have a program that uses a =ConfigManager= class to deal with saving and loading
program settings to a file. While the program runs, we probably want to be able to update
settings, and those settings will be stored as a =key, value= pair of some kind.

Some settings, like "=screen_width=", might be an integer, while other settings, like
"=last_save_game=" might be a string. Within the ConfigManager, we provide two different functions
to be able to update either type of option:

#+BEGIN_SRC cpp :class cpp
  class ConfigManager
  {
  public:
    void Load( std::ifstream& input );
    void Save();

    std::string Get( const std::string& key );
    int GetInt( const std::string& key );
    void Set( const std::string& key, const std::string& value );
    void Set( const std::string& key, int value );

    // ... etc ...
  };
#+END_SRC

To call the function, let's say we have a =ConfigManager config;= variable declared and set up.
We could call the Set function in either of these ways:

#+BEGIN_SRC cpp :class cpp
  config.Set( "screen_width", 800 );
  config.Set( "last_save_game", "autosave.gam" );
#+END_SRC

#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML

- Example usage - MenuManager ::

Here is a =MenuManager= that stores things like =UILabel= objects and other elements.
These elements go on separate layers and each element has a unit =name=.

The =GetLabel= function can be overloaded so that when we call it, we don't necessarily
need the layer name - that version of the function does a *search*. Or, we can call
the version and provide the layer name, and the element can be accessed directly
and much more quickly.

#+BEGIN_SRC cpp :class cpp
  class MenuManager
  {

  public:
    UILabel&             GetLabel( const std::string& layer, const std::string& name );
    UILabel&             GetLabel( const std::string& name );
    // ... etc ...

  private:
    std::map< std::string, UILayer > m_layers;
    // ... etc ...
  };
#+END_SRC

To call the function, let's say we have a =MenuManager menuManager;= variable declared and set up.
We could call the GetLabel function in either of these ways:

#+BEGIN_SRC cpp :class cpp
  // Slow version
  UILabel& currentLabel = menuManager.GetLabel( "filename" );

  // Fast version
  UILabel& currentLabel = menuManager.GetLabel( "file_info", "filename" );
#+END_SRC


#+ATTR_HTML: :class extra-space
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML

** Constructor overloading

Class *constructors* are special functions that are called automatically when a new
object (of that class type) is *instantiated*. (In other words, when you declare
a new variable and the data type is that class.)

There are three main types of constructors we can write, each of them overloaded.

*** Default constructor

The *default constructor* has *no parameters* and is called automatically when we
declare a class object variable like this:

#+BEGIN_SRC cpp :class cpp
  MyClass variablename;
#+END_SRC

Default constructors are usually used to initialize an object to get it ready for usage.
This can include:

- Setting any internal *pointers* to =nullptr= for safety.
- Opening files for any member *ifstream/ofstream* variables to get ready to write to.
- Setting *member variables* to some default values.

#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML

MyClass.h

#+BEGIN_SRC cpp :class cpp
  class MyClass
  {
    public:
    MyClass(); // default constructor

    private:
    int m_memberVariable;
  };
#+END_SRC


#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

MyClass.cpp

#+BEGIN_SRC cpp :class cpp
  MyClass::MyClass()
  {
    m_memberVariable = 100;
  }
#+END_SRC


#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML


#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
- Example usage - Logger class ::

Many programs have some kind of =Logger= that writes out events while the program is running
to a external text file (or other format). This can help diagnose issues if errors occur.

If we were creating a Logger class, it would be useful to open the log file automatically when the =Logger= object
is created, and close the log file automatically when the object is destroyed.


#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML

Logger.h

#+BEGIN_SRC cpp :class cpp
  class Logger
  {
    public:
    Logger();  // default constructor
    ~Logger(); // destructor
    // ... etc ...

    private:
    ofstream m_outfile;
    // ... etc ...
  };
#+END_SRC


#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

Logger.cpp

#+BEGIN_SRC cpp :class cpp
  Logger::Logger()
  {
    m_outfile.open( "log.txt" );
  }

  Loger::~Logger()
  {
    m_outfile.close();
  }
#+END_SRC


#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML

Elseware in the program, the Logger will be created, and that log file will be opened automatically:
 
#+BEGIN_SRC cpp :class cpp
  Logger logger;
#+END_SRC



#+ATTR_HTML: :class extra-space
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML

*** Parameterized constructor

A *parameterized constructor* is when we have one or more parameter in the constructor's
parameter list. These are values that will be passed into the object as it's being instantiated,
and these values can be used to set values to the object's member variables immediately.


#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML

MyClass.h

#+BEGIN_SRC cpp :class cpp
  class MyClass
  {
    public:
    MyClass();                 // default ctor
    MyClass( int new_value );  // parameterized ctor

    private:
    int m_memberVariable;
  };
#+END_SRC


#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

MyClass.cpp

#+BEGIN_SRC cpp :class cpp
  MyClass::MyClass( int new_value )
  {
    m_memberVariable = new_value;
  }
#+END_SRC


#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML

Then the object will be instantiated like this:

#+BEGIN_SRC cpp :class cpp
  MyClass variablename( 100 );
#+END_SRC




#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
- Example usage - Logger ::

Similarly to the default constructor example, but maybe there is a time where
we want to specify what that logger file is. Perhaps the parameterized constructor
will be called most of the time, but if we don't know what file to use we lean back
on the default constructor.


#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML

Logger.h

#+BEGIN_SRC cpp :class cpp
  class Logger
  {
    public:
    Logger();                       // default
    Logger( std::string filename ); // parameterized
    ~Logger();                      // destructor
    // ... etc ...

    private:
    ofstream m_outfile;
    // ... etc ...
  };
#+END_SRC


#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

Logger.cpp

#+BEGIN_SRC cpp :class cpp
  Logger::Logger() // default
  {
    m_outfile.open( "log.txt" );
  }
  
  Logger::Logger( std::string filename ) // parameterized
  {
    m_outfile.open( filename );
  }

  Loger::~Logger()
  {
    m_outfile.close();
  }
#+END_SRC


#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML

Elseware in the program, the Logger will be created, and that log file will be opened automatically:
 
#+BEGIN_SRC cpp :class cpp
  Logger logger( "log-sept-28.txt" );
#+END_SRC




#+ATTR_HTML: :class extra-space
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML

*** Copy constructor

With a *copy constructor* the idea is that we want to copy information from
one object to another object - to make a copy of that original object.

In some cases, maybe we want to copy all of the member variable values over.
In other cases, perhaps we /don't/ want to copy over all the data - such as
if the class has a pointer to a memory address.


#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML

MyClass.h

#+BEGIN_SRC cpp :class cpp
  class MyClass
  {
    public:
    MyClass();                       // default
    MyClass( int new_value );        // parameterized
    MyClass( const MyClass& other ); // copy ctor

    private:
    int m_memberVariable;
    int* m_pointer;
  };
#+END_SRC


#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

MyClass.cpp

#+BEGIN_SRC cpp :class cpp
  MyClass::MyClass( const MyClass& other )
  {
    // Copy over the member variable
    m_memberVariable = other.memberVariable;
    // Don't copy over the pointer
  }
#+END_SRC


#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML

For this constructor, we need another object of the same type declared already,
and we pass in that other object to make a copy of it.

#+BEGIN_SRC cpp :class cpp
  MyClass original;
  MyClass copied( original );
#+END_SRC


#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML

- Shallow copy vs. deep copy ::
  - A *Shallow Copy* is where values of variables are copied over. This is
     generally fine for any sort of non-pointer-based variables. If the class
     contains a pointer that is pointing to some address, the shallow-copy
     of the pointer will point to the same address.
  - A *Deep Copy* is where values are copied over like with a shallow copy,
    but also will allocate new memory for a dynamic array (if the class has
    one) in order to copy over values of the element of the array to the new class copy.

#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML
[[file:images/c3_u08_OverloadedFunctions_shallow-copy.png]]
#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

*Example of a shallow copy:*

With the implicit copy constructor, any pointers in the copied version will
be pointing to the same address as in the original. If the class contains a dynamic array, both the copy and the
original will end up pointing to the same address of the array in memory.

#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML

#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML
[[file:images/c3_u08_OverloadedFunctions_deep-copy.png]]
#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

*Example of a deep copy:*

A new block of memory has been allocated for the =int * numArr=. The values from InstanceA’s =numArr= would be
copied to InstanceB’s =numArr= via a for loop during construction.

#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML


#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
- Example usage - Data ::

A program might have some sort of *data* that you might want to make a copy of to
do work on but not potentially mess up the original data set. This can be good
if you want to *sort the data* or perform operations on the data.



#+ATTR_HTML: :class extra-space
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML

*** Additional notes
- If no constructors are declared, a default parameter is generated automatically at compile-time.
- If a parameterized or copy constructor is declared but not a default parameter, then /no default constructor/ will be generated at compile-time.

-----
