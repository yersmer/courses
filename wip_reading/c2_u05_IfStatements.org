# -*- mode: org -*-

#+TITLE: If statements
#+AUTHOR: Rachel Wil Sha Singh

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/rworgmode.css" />
#+HTML_HEAD: <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
#+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">
#+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script> <script>hljs.initHighlightingOnLoad();</script>
-----

WIP: Need to convert my LaTeX file to orgmode



\begin{figure}[h]
\begin{center}
    \includegraphics[width=12cm]{images/controlflow.png}
\end{center}
\end{figure}


** Boolean Expressions %------------------------------------%

*** Introduction

In order to start writing questions that a computer can understand,
we need to understand *boolean expressions*.  

A math expression looks like this:

$$ 2 + 3 $$


A boolean expression is a statement that result in either *true* or *false*:

\begin{center}
/ "is it daytime?" /

/ "did the user save the document?" /

/ "is the user's age greater than 12 and less than 20?" /
\end{center}

\begin{center}
\includegraphics[width=4cm]{images/cat.png}

\begin{verbatim}
if ( iHaveNotBeenFed && itIs6am ) { MakeLotsOfNoise(); }
\end{verbatim}
\end{center}


Logic in computer programs are based around these types of questions:
something that can only be *true* or *false*. Statements
include:

\begin{center}
\begin{tabular}{p{3cm} c p{7cm}}
  *Type* 				& *Example* 		& *Description* \\ \hline
  Is true?					& = a = 			& If =a= is a boolean variable, we can ask if it's true.
  \\ \\
  Is false?					& = !a = 		& If =a= is a boolean variable, we can ask if it's false.
  \\ \\ 
  Equivalence?				& = a == b = 	& Are the values of =a= and =b= the same?
  \\  \\
  Not equivalent?				& = a != b = 	& Are the values of =a= and =b= different?
  \\  \\
  Less than?					& = a < b = 		& Is =a= less than =b=?
  \\  \\
  Less than or equal to?		& = a <= b = 	& Is =a= less than or equal to =b=?
  \\ \\
  Greater than?				& = a > b = 		& Is =a= greater than =b=?
  \\ \\
  Greater than or equal to?	& = a >= b = 	& Is =a= greater than or equal to =b=?
\end{tabular}
\end{center}

For the first two, we can check the value of a *boolean variable*.
The rest of them, we can use any data type to compare if two values are equal,
greater than, less than, etc. =a= and =b= can be replaced
with variables and/or values...

	
- 	= if ( age < 18 ) = ...
- 	= if ( myState == yourState ) = ...
- 	= if ( word1 < word2 ) = ... 
    When using $<$ and $>$ with strings or chars, it will compare them
    based on alphabetical order - if they're both the same case (both uppercase or both lowercase).
- 	= if ( documentSaved == false ) =... 
    For boolean variables, you can check to see if they are equal to =true= or =false=.



*** And, Or, and Not operators

We can also combine boolean expressions together to ask more sophisticated questions.

=Not operator: =!==
If we're checking a boolean variable, or a boolean expression, and we happen
to want to execute some code for when that result is *false*, we can
use the not operator...

 Using with *boolean variable *documentSaved*:*	
#+BEGIN_SRC cpp :class cpp
// Just checking documentSaved = true
if ( documentSaved ) {
Quit();
}

// Checking if documentSaved = false with the not operator
if ( !documentSaved ){
Save();
Quit();
}
#+END_SRC

 Using with *boolean expressions *( age >= 21 )*:*
#+BEGIN_SRC cpp :class cpp
// If the age is not 21 or over...
if ( !( age >= 21 ) ) {
NoBeer();
}
#+END_SRC


=And operator: =\&\&==

When using the and operator, we can check to see if *all boolean variables/expressions are true*.
The full question of "is this and this and this true?" only results to *true*
if *all boolean variables/expressions are true*. If even one of them
are false, then the entire statement is false.

#+BEGIN_SRC cpp :class cpp
if ( wantsBeer && isAtLeast21 ) {
GiveBeer();
}
#+END_SRC

To break it down, the customer would only get beer if they 
/want beer/ AND if /they are over 21/. If they don't
want beer but are 21 or over, they don't get beer. If they want beer 
but are under 21, then they don't get beer.

=Or operator: =||==

For an or operator, we can check to see if *at least one boolean variable/expression is true*.
If at least one item is true, then the whole statement is true.
The only way for it to be false is when each boolean variable/expression is false.

#+BEGIN_SRC cpp :class cpp
if ( isBaby || isSenior || isVet ) {
GiveDiscount();
}
#+END_SRC

In this case, discounts are given to babies, seniors, and vets. A customer
wouldn't have to be all three to qualify (and clearly, couldn't be all three
at the same time!). If the customer is /not/ a baby and /not/ a senior
and /not/ a vet, then they don't get the discount - all three criteria
have to be false for the entire expression to be false.


*** Truth tables

We can use *truth tables* to help us visualize the logic that we're working with,
and to validate if our assumptions are true. Truth tables are for when we have
an expression with more than one variable (usually) and want to see the result
of using ANDs, ORs, and NOTs to combine them.

- Truth table for NOT: ::  

On the top-left of the truth table we will have the boolean variables or expressions
written out, and we will write down all its possible states. With just one
variable $p$, we will only have two states: when it's true, or when it's false.


On the right-hand side (I put after the double lines) will be the result of the
expression - in this case, "not-p" $!p$. The not operation simply takes the
value and flips it to the opposite: true $\to$ false, and false $\to$ true.

\begin{center}
\begin{tabular}{c | | c}
  $p$ & $!p$ \\ \hline
  \true & \false
  \\
  \false & \true
\end{tabular}
\end{center}



- Truth table for AND: ::  
For a truth table that deals with two variables, $p$ and $q$, the total
amount of states will be 4:
\begin{enumerate}
- 	$p$ is true and $q$ is true.
- 	$p$ is true and $q$ is false.
- 	$p$ is false and $q$ is true.
- 	$p$ is false and $q$ is false.
\end{enumerate}

\begin{center}
\begin{tabular}{c | c | | c}
  $p$ & $q$ & $p \&\& q$ \\ \hline
  \true & \true & \true \\
  \true & \false & \false \\
  \false & \true & \false \\
  \false & \false & \false
\end{tabular}
\end{center}

This is just a generic truth table. We can replace
the values with boolean expressions in C++ to check our logic.


For example, we will only quit the program if the game is saved and the user wants to quit:

\begin{center}
\begin{tabular}{c | c | | c}
  =gameSaved= & =wantToQuit= & =gameSaved \&\& wantToQuit=
  \\ \hline
  \true & \true & \true \\
  \true & \false & \false \\
  \false & \true & \false \\
  \false & \false & \false
\end{tabular}
\end{center}

The states are:

\begin{enumerate}
- 	=gameSaved= is true and =wantToQuit= is true: Quit the game. \checkmark
- 	=gameSaved= is true and =wantToQuit= is false: The user doesn't want to quit; don't quit. \xmark
- 	=gameSaved= is false and =wantToQuit= is true: The user wants to quit but we haven't saved yet; don't quit. \xmark
- 	=gameSaved= is false and =wantToQuit= is false: The user doesn't want to quit and the game hasn't been saved; don't quit. \xmark
\end{enumerate}



- Truth table for OR: ::  
Generic form:

\begin{center}
\begin{tabular}{c | c | | c}
  $p$ & $q$ & $p$ =||= $q$ \\ \hline
  \true & \true & \true \\
  \true & \false & \true \\
  \false & \true & \true \\
  \false & \false & \false
\end{tabular}
\end{center}

Again, if at least one of the expressions is *true*,
then the entire expression is true.

 Example: If the store has cakes OR ice cream, then suggest it to the user that wants dessert.

\begin{center}
\begin{tabular}{c | c | | c}
  =hasCake= & =hasIcecream= & =hasCake || hasIceCream=
  \\ \hline
  \true & \true & \true \\
  \true & \false & \true \\
  \false & \true & \true \\
  \false & \false & \false
\end{tabular}
\end{center}

The states are:

\begin{enumerate}
- 	=hasCake= is true and =hasIcecream= is true: The store has desserts; suggest it to the user. \checkmark
- 	=hasCake= is true and =hasIcecream= is false: The store has cake but no ice cream; suggest it to the user. \checkmark
- 	=hasCake= is false and =hasIcecream= is true: The store has no cake but it has ice cream; suggest it to the user. \checkmark
- 	=hasCake= is false and =hasIcecream= is false: The store doesn't have cake and it doesn't have ice cream; don't suggest it to the user. \xmark
\end{enumerate}


*** DeMorgan's Laws

Finally, we need to cover DeMorgan's Laws, which tell us what the /opposite/ of an expression means.


For example, if we ask the program *"is a $>$ 20?"* and the result is *false*,
then what does this imply? If $a > 20$ is false, then $a \leq 20$ is true... notice that the opposite of
"greater than" is "less than OR equal to"!

=Opposite of =a \&\& b=:=

\begin{center}
\begin{tabular}{c | c | | c}
  =a= & =b= & =a \&\& b= \\ \hline
  \true & \true & \true \\
  \true & \false & \false \\
  \false & \true & \false \\
  \false & \false & \false
\end{tabular}
\end{center}

If we're asking "is $a$ true and is $b$ true?" together, and the result is *false*, that means either:
\begin{enumerate}
- 	$a$ is false and $b$ is true, or
- 	$a$ is true and $b$ is false, or
- 	$a$ is false and $b$ is false.
\end{enumerate}

These are the states where the result of =a \&\& b= is false in the truth table.

In other words, 

\begin{center}
=!( a \&\& b ) $\equiv$ !a || !b=
\end{center}

If $a$ is false, or $b$ is false, or both are false, then the result of =a \&\& b= is false.



=Opposite of =a || b=:=

\begin{center}
\begin{tabular}{c | c | | c}
  =a= & =b= & =a || b= \\ \hline
  \true & \true & \true \\
  \true & \false & \true \\
  \false & \true & \true \\
  \false & \false & \false
\end{tabular}
\end{center}

If we're asking "is $a$ true or $b$ true?", if the result of that is *false*
that means only one thing:
\begin{enumerate}
- 	Both $a$ and $b$ were false.
\end{enumerate}

This is the only state where the result of =a || b= is false.

In other words,

\begin{center}
=!( a || b ) $\equiv$ !a \&\& !b=
\end{center} 

=a \&\& b= is false only if $a$ is false AND $b$ is false.

*** Summary

In order to be able to write *if statements* or *while loops*,
you need to understand how these boolean expressions work. If you're not familiar
with these concepts, they can lead to you writing *logic errors*
in your programs, leading to behavior that you didn't want.

\begin{center}
\includegraphics[width=5cm]{images/evilbug.png}
\end{center}

** Branching

Branching is one of the core forms of *controlling the flow of the program*.
We ask a question, and based on the result we perhaps do /this code over here/
or maybe /that code over there/ - the program results change based
on variables and data accessible to it.


We will mostly be using *if / else if / else statements*,
though *switch* statements have their own use as well.
Make sure to get a good understanding of how each type of branching
mechanism "flows".

*** If statements
If statements (usually the general term encompassing if / else if / else as well)
are a way we can ask a question and respond: If $a$ is less than $b$ then...
or if $a$ is greater than $c$ then... Otherwise do this default thing...


Let's look at some examples while ironing out how it works.


\subsubsection{If statements}

For a basic *if statement*,
we use the syntax:

#+BEGIN_SRC cpp :class cpp
// Do things 1

if ( CONDITION )
{
// Do things 1-a
}

// Do things 2
#+END_SRC

The CONDITION will be some *boolean expression*. If the
boolean expression result is *true*, then any code within
this if statement's *code block* (what's between \* and \*)
will get executed. If the result of the expression is *false*,
then the entire if statement's code block is skipped and the program
continues.	

 Example:

#+BEGIN_SRC cpp :class cpp
cout << "Bank balance: " << balance;

if ( balance < 0 )
{
cout << " (OVERDRAWN!)";
}

cout << " in account #" << accountNumber << endl;
#+END_SRC

     Output with =balance = -50=
    
\begin{lstlisting}[style=output]
Bank balance: -50 (OVERDRAWN!) in account #1234
#+END_SRC

     Output with =balance = 100=
    
\begin{lstlisting}[style=output]
Bank balance: 100 in account #1234
#+END_SRC


- 	The special message "OVERDRAWN!" only gets displayed when the balance is less than 0.
- 	The following =cout= that gives the account number is displayed in all cases.


 Diagram:
\begin{center}
\begin{tikzpicture}
  \draw (-5,10) -- (5,10) -- (5,12) -- (-5,12) -- (-5,10);
  \node at (0, 11) = = cout << "Bank balance: " << balance; = =;
  
  \draw (-5, 8) -- (0, 7) -- (5, 8) -- (0, 9) -- (-5,8);
  \node at (0, 8) = =balance < 0= ? =;
  
  \draw[-stealth,ultra thick]      (0,10) -- (0,9);			
  
  \draw (-7,4) -- (-1,4) -- (-1,6) -- (-7,6) -- (-7,4);
  \node at (-4, 5) = = cout << " (OVERDRAWN!)"; = =;
  
  \draw[-stealth,ultra thick]      (0, 7) -- (-3, 6);
  \draw[-stealth,ultra thick]      (0, 7) -- (0,2);
  \draw[-stealth,ultra thick]      (-3,4) -- (-1,2);
  
  \node at (-2, 6.7) {true};
  \node at (0.5, 5) {false};
        
  \draw (-6,0) -- (6,0) -- (6,2) -- (-6,2) -- (-6,0);
  \node at (0, 1) = = cout << " in account \#" << accountNumber << endl; = =;
\end{tikzpicture}
\end{center}



\subsubsection{If/Else statements}

In some cases, we will have code that we want to execute for the *false*
result as well. For an *if/else* statement, either the *if*
code block will be entered, or the *else* code block will be.


*NOTE:* The else statement NEVER has a CONDITION.

#+BEGIN_SRC cpp :class cpp
// Do things 1

if ( CONDITION )
{
// Do things 1-a
}
else
{
// Do things 1-b
}

// Do things 2
#+END_SRC

The *else* block is executed when the *if* check fails.
If the CONDITION is false, we can use *DeMorgan's Laws*
to figure out what the program's state is during the *else*.

 Example:
#+BEGIN_SRC cpp :class cpp
cout << "Enter your age: ";
cin >> age;

if ( age < 18 )
{
result = "can't vote";
}
else
{
result = "can vote";
}

cout << "Result: " << result << endl;
#+END_SRC

If the =age= is less than 18, it will set the =result=
variable to "can't vote". If that boolean expression is *false*,
that means =age= is $\geq$ 18, and then it will set the =result=
to "can vote". Finally, either way, it will display "Result: "
with the value of the =result= variable.


 Diagram:
\begin{center}
\begin{tikzpicture}
  \draw (-5,10) -- (5,10) -- (5,12) -- (-5,12) -- (-5,10);
  \node at (0, 11) = = cout << "Enter your age: "; cin >> age; = =;
  
  \draw (-5, 8) -- (0, 7) -- (5, 8) -- (0, 9) -- (-5,8);
  \node at (0, 8) = =age < 18= ? =;
  
  \draw[-stealth,ultra thick]      (0,10) -- (0,9);			
  
  \draw (-7,4) -- (-1,4) -- (-1,6) -- (-7,6) -- (-7,4);
  \node at (-4, 5) = = result = "can't vote"; = =;
  
  \draw (7,4) -- (1,4) -- (1,6) -- (7,6) -- (7,4);
  \node at (4, 5) = = result = "can vote"; = =;
  
  \draw[-stealth,ultra thick]      (0, 7) -- (-3, 6);
  \draw[-stealth,ultra thick]      (0, 7) -- (3, 6);

  \draw[-stealth,ultra thick]      (-3,4) -- (-1,2);
  \draw[-stealth,ultra thick]      (3,4) -- (1,2);
  
  \node at (-2, 6.7) {true};
  \node at (2, 6.7) {false};
        
  \draw (-6,0) -- (6,0) -- (6,2) -- (-6,2) -- (-6,0);
  \node at (0, 1) = = cout << "Result: " << result << endl; = =;
\end{tikzpicture}
\end{center}


\subsubsection{If/Else if/Else statements}

In some cases, there will be multiple scenarios we want to search for,
each with their own logic. We can add as many *else if* statements
as we want - there must be a starting *if* statement, and each
*else if* statement will have a condition as well.


We can also end with a final *else* statement as a catch-all:
if none of the previous if / else if statements are true, then *else*
gets executed instead. However, the else is not required.

#+BEGIN_SRC cpp :class cpp
// Do things 1
if ( CONDITION1 )
{
// Do things 1-a
}
else if ( CONDITION2 )
{
// Do things 1-b
}
else if ( CONDITION3 )
{
// Do things 1-c
}
else
{
// Do things 1-d
}
// Do things 2
#+END_SRC

 Example:

#+BEGIN_SRC cpp :class cpp
cout << "Enter grade: ";
cin >> grade;

if ( grade >= 90 ) 		{ letterGrade = 'A'; }
else if ( grade >= 80 ) { letterGrade = 'B'; }
else if ( grade >= 70 ) { letterGrade = 'C'; }
else if ( grade >= 60 ) { letterGrade = 'D'; }
else					{ letterGrade = 'F'; }

cout << "Grade: " << letterGrade << endl;
#+END_SRC

With this example, I'm first checking if the grade is 90 or above.
If it is, then I know the letter grade is ='A'=.


However, if it's false, it will go on and check the next *else if*
statement. At this point, I know that since =grade >= 90= was FALSE,
that means =grade < 90=. Since the next statement checks if 
=grade >= 80=, if this one is true I know that =grade < 90 AND grade >= 80=.


 Diagram:
\begin{center}
\includegraphics[width=12cm]{images/diagrams/branching.png}
\end{center}

