# -*- mode: org -*-

[[file:../../images/topics/arrays.png]]

* *Smart fixed array structure*

In previous programming courses you've probably worked with *arrays*
to store data. You've probably encountered out-of-bounds errors and
had to deal with array indices, moving items around, and generally
micro-managing your array's data as the program goes.

What we're going to do here is *wrap* our array - and
everything we need to work with it - inside of a class,
creating our first data structure: A slightly smarter fixed-length array.


** *Functionality for our data type*

What kind of things do we want to \textit{do} to a data structure?
What is some functionality that others will need to store and retrieve
data, without having to see inside the class? How do we keep the data stored?

*Creating the array:*
For example, if we weren't storing our array inside a class, we might
declare it in a program like this:


#+BEGIN_SRC cpp :class cpp
// Create an array + helper variables
const int ARR_SIZE = 100;
int totalItems = 0;
string data[ARR_SIZE];
#+END_SRC

With a fixed-length array, we have to keep track of the array size =ARRAY_SIZE=
to ensure we don't go out-of-bounds in the array... valid indices will be =0=
until =ARRAY_SIZE - 1=.

We are also keeping track of the =totalItems= in the array, because
even though we have an array with some size, it could be useful to know that
currently no data is stored in it. Then, when new data is added, we could
increment =totalItems= by 1.

An empty array:

| *index* |  0 |  1 |  2 |  3 |  4 |  5 |
| *value* | "" | "" | "" | "" | "" | "" |

=totalItems = 0=





#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*Adding onto the array:*
Adding on to the array would mean choosing an index to put new data.
If we were wanting to fill the array from \textbf{left-to-right},
we would start at index 0, then 1, then 2, then 3, and so on.

The =totalItems= variable begins at 0. Once the first item is added,
it is then 1. Once a second item is added, it is then 2. Basically, the
=totalItems= corresponds to the /next index to store new data at/...

#+BEGIN_SRC cpp :class cpp
// Add new item
data[totalItems] = myNewData;
totalItems++;
#+END_SRC

One item added:

| *index* |      0 |  1 |  2 |  3 |  4 |  5 |
| *value* | "Cats" | "" | "" | "" | "" | "" |

=totalItems = 1=




#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*Removing items from the array:*
We may also want to remove an item at a specific index from the array.
There's not really a way to "delete" an item from an array, but we can
overwrite it, if we wanted to...

#+BEGIN_SRC cpp :class cpp
// Remove an item
int index;
cout << "Remove which index? ";
cin >> index;

data[index] = "";
totalItems--;
#+END_SRC

But if we did it this way we would create an array with gaps in it.
Additionally now =totalItems= doesn't align to the /next index to insert at/.


Before removing an item:

| *index* |      0 |      1 |      2 |       3 |        4 |  5 |
| *value* | "Cats" | "Dogs" | "Mice" | "Birds" | "Snakes" | "" |

=totalItems = 6=

After removing an item:

| *index* |      0 |      1 |      2 |  3 |        4 |  5 |
| *value* | "Cats" | "Dogs" | "Mice" | "" | "Snakes" | "" |

=totalItems = 5=


We could design our array data structure this way, but to find a new
place to add an item we'd have to use a for loop to look for an empty
spot to use - this means every time we add a new item it would be less efficient.

Instead, we could design our arrays so that after a remove, we shift elements
backwards to "overwrite" the empty spot. This also solves our issue with
=totalItems= not aligning to the /next-index-to-insert-at/.

After removing an item with a "Shift Left":

| *index* |      0 |      1 |      2 |        3 |  4 |  5 |
| *value* | "Cats" | "Dogs" | "Mice" | "Snakes" | "" | "" |

=totalItems = 5=


#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*Searching for an item:*
Another thing a user of this data structure might want to do is search for an item -
is this thing stored in the array? If so, what is the index?
If the array is *unsorted* (we're not going to do sorting yet),
then really the only way to search for the item is to start at the beginning
and keep searching until the end.

If we find a match, we return that index number.

If we get to the end of the loop and nothing has been returned,
that means it isn't in the array so we would have to return something
to symbolize "it's not in the arrayI -- such as returning =-1=,
or perhaps throwing an exception.

Populated array:

| *index* |      0 |      1 |      2 |       3 |       4 |  5 |
| *value* | "Cats" | "Dogs" | "Mice" | "Birds" | "Snakes | "" |

=totalItems = 5=

Searching through the array would look like this:

#+BEGIN_SRC cpp :class cpp
// Search for an item
string findme;
cout << "Find what? ";
getline( cin, findme );

int foundIndex;

for ( int i = 0; i < totalItems; i++ )
{
    if ( data[i] == findme )
    {
        foundIndex = i;   // Found it
        break;
    }
}

// Did not find it
foundIndex = -1;
#+END_SRC


Once we implement this structure within a class, we will turn this into
a function that returns data - this is just an example as if we were
writing a program in main().




#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*Retrieving an item at some index:*
Besides adding, removing, and searching for items, users will
want to retrieve the data located at some index. This would be a simple
return once we're writing our class, but generally accessing /item at index/
looks like:

#+BEGIN_SRC cpp :class cpp
// Display element at index
int index;
cout << "Which index? ";
cin >> index;
cout << data[index] << endl;
#+END_SRC




#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*Visualizing the array:*
Another feature could be to display all elements of the array, which
might look something like this:

#+BEGIN_SRC cpp :class cpp
// Display all items
for ( int i = 0; i < totalItems; i++ )
{
    cout << i << ". " << data[i] << endl;
}
#+END_SRC



#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *Predicting errors/exceptions*

*Invalid indices:*
When working with arrays one of the most common errors encountered
are *out-of-bounds* errors: When we have an index that is less
than 0, or equal to or greater than the size of the array...

#+BEGIN_SRC cpp :class cpp
// Out of bounds!
cout << data[-1]        << endl;    // error!
cout << data[ARR_SIZE]  << endl;    // error!
#+END_SRC

Additionally, if the user tries to access an index $\geq$ =totalItems=
then they would retrieve data that is invalid, though this wouldn't
crash the program if it is still less than =ARR_SIZE=.

#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*Array is full:*
With the fixed-length array, we could also run out of space, and we
would have to design a way to deal with it. For example,
if =totalItems= was equal to =ARR_SIZE=, then we are out
of space and doing this would also cause an out-of-bounds error.

#+BEGIN_SRC cpp :class cpp
data[totalItems] = "Ferrets";       // error if full!
totalItems++;
#+END_SRC

As part of designing our data structure, we need to make sure we account
for reasonable scenarios that would cause the program to crash and
guard against logic errors. This would all be part of how we implement
the functionality.



#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *Creating a class to wrap the array*

For our wrapper class we will need the array and array size / item count
integers as part of the private member variables. The public functions
would include the "interface" of what the user will want to do with
the array, and we could also add private/protected helper functions.

#+ATTR_HTML: :class uml
| Class Name                                     |             |
|------------------------------------------------+-------------|
| - =m_array=                                    | : TYPE[100] |
| - =m_itemCount=                                | : int       |
| - =m_arraySize=                                | : const int |
|------------------------------------------------+-------------|
| + =SmartFixedArray()=                          |             |
|                                                |             |
| + =PushFront( newData : TYPE )=                | : void      |
| + =PushBack( newData : TYPE )=                 | : void      |
| + =PushAtIndex( index : int, newData : TYPE )= | : void      |
|                                                |             |
| + =PopFront()=                                 | : void      |
| + =PopBack()=                                  | : void      |
| + =PopAtIndex( index : int )=                  | : void      |
|                                                |             |
| + =GetFront()=                                 | : TYPE&     |
| + =GetBack()=                                  | : TYPE&     |
| + =GetAtIndex( index : int )=                  | : TYPE&     |
|                                                |             |
| + =Search( item : TYPE )=                      | : int       |
| + =Clear()=                                    | : void      |
| + =IsEmpty()=                                  | : bool      |
| + =IsFull()=                                   | : bool      |
| + =Size()=                                     | : int       |
|                                                |             |
| - =ShiftLeft()=                                | : int       |
| - =ShiftRight()=                               | : int       |
| - =IsValidIndex( index : int )=                | : bool      |



The class declaration could look like this:
#+BEGIN_SRC cpp :class cpp
template <typename T>
class SmartFixedArray
{
public:
    SmartFixedArray();

    void PushBack( T newItem );
    void PushFront( T newItem );
    void PushAt( T newItem, int index );

    void PopBack();
    void PopFront();
    void PopAt( int index );

    T GetBack() const;
    T GetFront() const;
    T GetAt( int index ) const;

    int Search( T item ) const;
    void Display() const;
    int Size() const;
    bool IsEmpty() const;
    bool IsFull() const;

private:
    const int m_arraySize;
    T m_array[100];
    int m_itemCount;

    void ShiftLeft( int index );
    void ShiftRight( int index );
    bool IsValidIndex( int index );
};
#+END_SRC


For this example data structure, we are hard-coding the array size to 100 elements.
It is unlikely that anyone would use this data structure for anything,
but I wanted to start off with a basic fixed-length array. Next,
we will move to a dynamic array, which /will/ be more useful.

With this class structure set up, lets look into how to
actually implement the functionality.




#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*** *Constructor - Preparing the array to be used*
When the constructor is called, we need to set the value of =m_arraySize=
as part of the initializer list since it is a const member and needs to be
initialized right away.

Otherwise, we can initialize =m_itemCount= to 0 as well,
since are creating an empty array.

#+BEGIN_SRC cpp :class cpp
template <typename T>
SmartFixedArray<T>::SmartFixedArray()
    : m_arraySize( 100 )
{
    m_itemCount = 0;
}
#+END_SRC




#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*** *Helper functions*
The helper functions here are easier to implement, and we will be
using them in the main interface functions.

**** *int Size() const*
  This function returns the value from =m_itemCount= - the current
  amount of items in the array.



**** *bool IsEmpty() const*
  Returns =true= if the array is empty and =false= if not.
  The array is empty if =m_itemCount= is 0.




**** *bool IsFull() const*
  Returns =true= if the array is full and =false= if not.
  The array is full if =m_itemCount= is equal to =m_arraySize=.



**** *bool IsValidIndex( int index )*
  Returns =true= if the index is valid and =false= otherwise.
  - Valid index: The index is valid if $0 \leq $ =index= and =index= $ < $ =m_itemCount=.
  - Invalid index: The index is invalid if =index= $<$ 0 or =index= $\geq$ =m_itemCount=.
    #+BEGIN_SRC cpp :class cpp
bool SmartFixedArray<T>::IsValidIndex( int index )
{
    return ( 0 <= index && index < m_itemCount );
}
    #+END_SRC



**** *void ShiftRight( int index )*

*Error checks:*
- If the array is full, then throw an exception.
- If the index is invalid, then throw an exception.


*Functionality:*
- Iterate over the array, starting at the index of the first empty space.
  Moving backwards, copy each item over from the previous index.
  Loop until hitting the /index/.
  #+BEGIN_SRC cpp :class cpp
void SmartFixedArray<T>::ShiftRight( int index )
{
    if ( !IsValidIndex( index ) )
    {
        throw out_of_range( "Index is out of bounds!" );
    }
    else if ( IsFull() )
    {
        throw length_error( "Array is full!" );
    }

    for ( int i = m_itemCount; i > index; i-- )
    {
        m_array[i] = m_array[i-1];
    }
}
  #+END_SRC


**** *void ShiftLeft( int index )*

*Error checks:*
- If the index is invalid, then throw an exception.

*Functionality:*
- Iterate over the array, starting at the index given and going
  until the last element. Copy each neighbor from the right
  to the current index until we get to the end.
  #+BEGIN_SRC cpp :class cpp
void SmartFixedArray<T>::ShiftLeft( int index )
{
    if ( !IsValidIndex( index ) )
    {
        throw out_of_range( "Index is out of bounds!" );
    }

    for ( int i = index; i < m_itemCount-1; i++ )
    {
        m_array[i] = m_array[i+1];
    }
}
  #+END_SRC


**** *void Display() const*
Iterates over all the elements of the array displaying each item.

#+BEGIN_SRC cpp :class cpp
void SmartFixedArray<T>::Display() const
{
    for ( int i = 0; i < m_itemCount; i++ )
    {
        cout << i << ". " << m_array[i] << endl;
    }
}
#+END_SRC




**** *int Search( T item ) const*
Iterates over all the elements of the array searching for the \texttt{item}.
If a match is found, it returns the index of that element.
Otherwise, if it finishes looping over the array and has not yet returned,
it means that nothing was found. Return -1 in this case, or
throw an exception (this is a design decision).

#+BEGIN_SRC cpp :class cpp
int SmartFixedArray<T>::Search( T item ) const
{
    for ( int i = 0; i < m_itemCount; i++ )
    {
        if ( m_array[i] == item )
        {
            return i;
        }
    }
    throw runtime_error( "Could not find item!" );
}
#+END_SRC




#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*** *Push - adding items to the array*

We have three different Push functions in order to add an item to
different parts of the array, which will each require slightly
different logic:

*PushBack:* Add a new item to the end of the list of elements...

#+ATTR_HTML: :class center
*BEFORE:* =totalItems = 4=

| *index* |      0 |      1 |      2 |       3 |  4 |  5 |
| *value* | "Cats" | "Dogs" | "Mice" | "Birds" | "" | "" |



#+ATTR_HTML: :class center
*AFTER:* =totalItems = 5=

| *index* |      0 |      1 |      2 |       3 |         4 |  5 |
| *value* | "Cats" | "Dogs" | "Mice" | "Birds" | "Ferrets" | "" |




#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*PushFront:* Add a new item to the beginning of the list of elements.
This requires shifting everything forward to make space for the new item...

#+ATTR_HTML: :class center
*BEFORE:* =totalItems = 4=

| *index* |      0 |      1 |      2 |       3 |  4 |  5 |
| *value* | "Cats" | "Dogs" | "Mice" | "Birds" | "" | "" |



#+ATTR_HTML: :class center
*AFTER:* =totalItems = 5=

| *index* |         0 |      1 |      2 |      3 |       4 |  5 |
| *value* | "Ferrets" | "Cats" | "Dogs" | "Mice" | "Birds" | "" |




#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*PushAt:* Add a new item to the index given.
This requires shifting everything forward to make space for the new item...

#+ATTR_HTML: :class center
*BEFORE:* =totalItems = 4=

| *index* |      0 |      1 |      2 |       3 |  4 |  5 |
| *value* | "Cats" | "Dogs" | "Mice" | "Birds" | "" | "" |



#+ATTR_HTML: :class center
*AFTER:* =totalItems = 5=

| *index* |      0 |      1 |         2 |      3 |       4 |  5 |
| *value* | "Cats" | "Dogs" | "Ferrets" | "Mice" | "Birds" | "" |






#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*Additional design considerations:*

The =PushFront= and =PushAt= functions require shifting other elements
over, so I've added a helper function called =ShiftRight= so that we
don't write the same functionality in both of these functions.

Additionally, for each of these functions we will need to check to see
if the array is full prior to adding any new items or shifting things.
Implementing a =IsFull= function can help with this as well.


**** *Implementing =void PushBack( T newItem )=*

*Error checks:*
- If the array is full, then throw an exception.

#+BEGIN_SRC cpp :class cpp
if ( IsFull() )
    throw length_error( "Array is full!" );
#+END_SRC

*Functionality:*
- Add the new item to the first empty space.
- Increment the item count.


#+BEGIN_SRC cpp :class cpp
m_array[ m_itemCount ] = newItem;
m_itemCount++;
#+END_SRC


#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
**** *Implementing =void PushFront( T newItem )=*

*Error checks:*
- If the array is full, then throw an exception.


*Preparation:*
- Shift everything to the right, starting at index 0.

#+BEGIN_SRC cpp :class cpp
ShiftRight( 0 );
#+END_SRC

*Functionality:*
- Add the new item at position 0.
- Increment the item count.


#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
**** *Implementing =void PushAt( T newItem )=*

*DRY (Don't Repeat Yourself) checks:*
- If the index is 0, call PushFront instead.
- If the index is =m_itemCount=, call PushBack instead.

*Error checks:*
- If the array is full, then throw an exception.
- If the index is invalid, throw an exception.

*Preparation:*
- Shift everything to the right, starting at the index.

#+BEGIN_SRC cpp :class cpp
  ShiftRight( index );
#+END_SRC

*Functionality:*
- Add the new item at position index.
- Increment the item count.



#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*** *Pop - removing items from the array*

**** *Implementing =void PopBack()=*
*Error checks:* If the array is empty, throw exception.

*Functionality:* Decrement =m_itemCount=.


#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML

**** *Implementing =void PopFront()=*
*Error checks:* If the array is empty, throw exception.

*Functionality:* Call ShiftLeft at 0. Decrement =m_itemCount=.


#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML

**** *Implementing =void PopAt( int index )=*
*Error checks:* If the array is empty, throw exception. If the index is invalid, throw an exception.

*Functionality:* Call ShiftLeft at index. Decrement =m_itemCount=.



#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*** *Get - accessing items in the array*

**** *Implementing =T GetBack()=*
*Error checks:* If the array is empty, throw exception.

*Functionality:* Return the item in the array at position =m_itemCount-1=.



#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML

**** *Implementing =T GetFront()=*
*Error checks:* If the array is empty, throw exception.

*Functionality:* Return the item in the array at position 0.


#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML

**** *Implementing =T GetAt( int index )=*
*Error checks:* If the array is empty, throw exception.
If the index is invalid, throw an exception.

*Functionality:* Return the item in the array at position index.


#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML

*** *Search*
Use a for loop to iterate over all elements of the array (0 to =m_itemCount=). If the
item is found, return its index. Otherwise, after the loop has concluded, throw an exception
if nothing was found.

#+BEGIN_SRC cpp :class cpp
int SmartFixedArray<T>::Search( T item ) const
{
    for ( int i = 0; i < m_itemCount; i++ )
    {
        if ( m_array[i] == item )
        {
            return i;
        }
    }
    throw ItemNotFoundException( "SmartFixedArray<T>::Search", "Could not find item!" );
}
#+END_SRC



#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*** *SmartFixedArray code*


#+BEGIN_SRC cpp :class cpp
template <typename T>
class SmartFixedArray
{
public:
  SmartFixedArray();
  ~SmartFixedArray();

  void PushBack( T newItem );
  void PushFront( T newItem );
  void PushAt( T newItem, int index );

  void PopBack();
  void PopFront();
  void PopAt( int index );

  T GetBack() const;
  T GetFront() const;
  T GetAt( int index ) const;

  int Search( T item ) const;
  void Display() const;
  void Display( ostream& outstream ) const;
  int Size() const;
  bool IsEmpty() const;
  bool IsFull() const;
  void Clear();

private:
  T m_array[100];
  const int ARRAY_SIZE;
  int m_itemCount;

  void ShiftLeft( int index );
  void ShiftRight( int index );
};

template <typename T>
SmartFixedArray<T>::SmartFixedArray()
  : ARRAY_SIZE( 100 )
{
  Clear();
}

template <typename T>
SmartFixedArray<T>::~SmartFixedArray()
{
  Clear();
}

template <typename T>
void SmartFixedArray<T>::Clear()
{
  m_itemCount = 0;
}

template <typename T>
int SmartFixedArray<T>::Size() const
{
  return m_itemCount;
}

template <typename T>
bool SmartFixedArray<T>::IsFull() const
{
  return ( m_itemCount == ARRAY_SIZE );
}

template <typename T>
bool SmartFixedArray<T>::IsEmpty() const
{
  return ( m_itemCount == 0 );
}

template <typename T>
void SmartFixedArray<T>::Display() const
{
  Display( cout );
}

template <typename T>
void SmartFixedArray<T>::Display( ostream& outstream ) const
{
  for ( int i = 0; i < m_itemCount; i++ )
  {
    outstream << i << ". " << m_array[i] << endl;
  }
}

template <typename T>
void SmartFixedArray<T>::ShiftLeft( int index )
{
  if ( index < 0 || index >= m_itemCount )
  {
    // THROW EXCEPTION
  }

  for ( int i = index; i < m_itemCount-1; i++ )
  {
    m_array[i] = m_array[i+1];
  }
}

template <typename T>
void SmartFixedArray<T>::ShiftRight( int index )
{
  if ( index < 0 || index >= m_itemCount )
  {
    // THROW EXCEPTION
  }
  else if ( m_itemCount == ARRAY_SIZE )
  {
    // THROW EXCEPTION
  }

  for ( int i = m_itemCount; i > index; i-- )
  {
    m_array[i] = m_array[i-1];
  }
}

template <typename T>
void SmartFixedArray<T>::PushBack( T newItem )
{
  if ( IsFull() )
  {
    // THROW EXCEPTION
  }

  m_array[ m_itemCount ] = newItem;
  m_itemCount++;
}

template <typename T>
void SmartFixedArray<T>::PushFront( T newItem )
{
  if ( IsFull() )
  {
    // THROW EXCEPTION
  }
  if ( !IsEmpty() )
  {
    ShiftRight( 0 );
  }

  m_array[ 0 ] = newItem;
  m_itemCount++;
}

template <typename T>
void SmartFixedArray<T>::PushAt( T newItem, int index )
{
  if ( index == 0 )
  {
    PushFront( newItem );
    return;
  }
  else if ( index == m_itemCount )
  {
    PushBack( newItem );
    return;
  }

  if ( index < 0 || index > m_itemCount )
  {
    // THROW EXCEPTION
  }

  if ( IsFull() )
  {
    // THROW EXCEPTION
  }

  ShiftRight( index );
  m_array[ index ] = newItem;
  m_itemCount++;
}

template <typename T>
void SmartFixedArray<T>::PopBack()
{
  if ( IsEmpty() )
  {
    // THROW EXCEPTION
  }
  m_itemCount--;
}

template <typename T>
void SmartFixedArray<T>::PopFront()
{
  if ( IsEmpty() )
  {
    // THROW EXCEPTION
  }
  ShiftLeft( 0 );
  m_itemCount--;
}

template <typename T>
void SmartFixedArray<T>::PopAt( int index )
{
  if ( IsEmpty() )
  {
    // THROW EXCEPTION
  }
  else if ( index < 0 || index >= m_itemCount )
  {
    // THROW EXCEPTION
  }
  ShiftLeft( index );
  m_itemCount--;
}

template <typename T>
T SmartFixedArray<T>::GetBack() const
{
  if ( IsEmpty() )
  {
    // THROW EXCEPTION
  }

  return m_array[m_itemCount - 1];
}

template <typename T>
T SmartFixedArray<T>::GetFront() const
{
  if ( IsEmpty() )
  {
    // THROW EXCEPTION
  }

  return m_array[0];
}

template <typename T>
T SmartFixedArray<T>::GetAt( int index ) const
{
  if ( IsEmpty() )
  {
    // THROW EXCEPTION
  }
  else if ( index < 0 || index >= m_itemCount )
  {
    // THROW EXCEPTION
  }

  return m_array[index];
}

template <typename T>
int SmartFixedArray<T>::Search( T item ) const
{
  for ( int i = 0; i < m_itemCount; i++ )
  {
    if ( m_array[i] == item )
    {
      return i;
    }
  }
  // THROW EXCEPTION
}
#+END_SRC


-----------------------------------------------------
* *Smart dynamic array structure*

With the dynamic array structure, most of it will be similar to the Fixed Array Structure,
except we now have to deal with memory management and pointers.

Instead of throwing exceptions if the array is full, with a dynamic array
we can resize it instead.
However, we also now need to check for the array pointer (=m_array= in this example)
pointing to =nullptr=.


The class declaration could look like this:

#+BEGIN_SRC cpp :class cpp
class SmartDynamicArray
{
public:
    SmartDynamicArray();
    ~SmartDynamicArray();

    void PushBack( T newItem );
    void PushFront( T newItem );
    void PushAt( T newItem, int index );

    void PopBack();
    void PopFront();
    void PopAt( int index );

    T GetBack() const;
    T GetFront() const;
    T GetAt( int index ) const;

    int Search( T item ) const;

    void Display() const;
    int Size() const;
    bool IsEmpty() const;
    void Clear();

private:
    T* m_array;
    int m_arraySize;
    int m_itemCount;

    void ShiftLeft( int index );
    void ShiftRight( int index );

    void AllocateMemory( int size );
    void Resize( int newSize );

    bool IsFull() const;
};
#+END_SRC




#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*** *Constructor - Preparing the array to be used*
With the constructor here, it is important to assign =nullptr= to
the pointer so that we don't try to dereference a garbage address.
The array size and item count variables should also be assigned to 0.

#+BEGIN_SRC cpp :class cpp
template <typename T>
SmartDynamicArray<T>::SmartDynamicArray()
{
  m_array = nullptr;
  m_itemCount = 0;
  m_arraySize = 0;
}
#+END_SRC


#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*** *Destructor - Cleaning up the array*
Having a destructor is also very important since we're working with pointers.
Before our data structure is destroyed, we need to make sure that we free
any allocated memory.

#+BEGIN_SRC cpp :class cpp
template <typename T>
SmartDynamicArray<T>::~SmartDynamicArray()
{
  if ( m_array != nullptr )
  {
    delete [] m_array;
    m_array = nullptr;
  }
  m_arraySize = 0;
  m_itemCount = 0;
}
#+END_SRC

We can also put this functionality into the Clear() function, and call Clear()
from the destructor.



#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*** *void AllocateMemory( int size )*
When the array is not in use, the =m_array= pointer will be pointing to
=nullptr=. In this case, we need to allocate space for a new array before
we can begin putting items into it.

*Error checks:*
- If the =m_array= pointer is NOT pointing to nullptr, throw an exception.


*Functionality:*
1. Allocate space for the array via the =m_array= pointer.
2. Assign =m_arraySize= to the size passed in as a parameter.
3. Set =m_itemCount= to 0.


#+BEGIN_SRC cpp :class cpp
template <typename T>
void SmartDynamicArray<T>::AllocateMemory( int size )
{
  if ( m_array != nullptr )
  {
    throw logic_error( "Can't allocate memory, m_array is already pointing to a memory address!" );
  }

  m_array = new T[ size ];
  m_arraySize = size;
  m_itemCount = 0;
}
#+END_SRC


For this function, if the array is already pointing somewhere, we don't want
to just erase all that data and allocate space for a new array. We want to try
to prevent data loss, so it would be better to throw an exception instead
so that the caller can decide how they want to handle it.


#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*** *void Resize( int newSize )*
We can't technically resize an array that has been created. We can, however,
allocate more space for a bigger array and copy all the data over and then update
the pointer to point to the new array. And that's exactly how we "resize" our dynamic array.

*Error checks:*
- If the new size is smaller than the old size, throw an exception.

*Functionality:*
1. If =m_array= is pointing to nullptr, then just call AllocateMemory instead.
2. Otherwise:
   1. Allocate space for a new array with the new size using a new pointer.
   2. Iterate over all the elements of the old array, copying each element to the new array.
   3. Free space from the old array.
   4. Update the =m_array= pointer to point to the new array address.
   5. Update the =m_arraySize= to the new size.

#+BEGIN_SRC cpp :class cpp
template <typename T>
void SmartDynamicArray<T>::Resize( int newSize )
{
  if ( newSize < m_arraySize )
  {
    throw logic_error( "Invalid size!" );
  }
  else if ( m_array == nullptr )
  {
    AllocateMemory( newSize );
  }
  else
  {
    T* newArray = new T[newSize]; // New array

    // Copy values over
    for ( int i = 0; i < m_itemCount; i++ )
    {
      newArray[i] = m_array[i];
    }

    delete [] m_array;      // Deallocate old space
    m_array = newArray;     // Update pointer
    m_arraySize = newSize;  // Update size
  }
}
#+END_SRC



#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*** *Updating other functions*

- *ShiftLeft:*   If =m_array= is pointing to =nullptr=, throw an exception.
- *ShiftRight:*
  - If =m_array= is pointing to =nullptr=, throw an exception.
  - If the array is full, call Resize.

- *PushBack, PushFront, PushAt:*
  - If =m_array= is pointing to =nullptr= call AllocateMemory.
  - If the array is full, call Resize.




