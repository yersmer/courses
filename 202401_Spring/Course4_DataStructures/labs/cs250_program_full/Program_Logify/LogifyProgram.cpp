#include "LogifyProgram.h"

#include "../Namespace_Utilities/Menu.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <functional>
#include <string>

namespace Logify
{

LogifyProgram::LogifyProgram()
	: m_employees(101)
{
    std::cout << std::setfill( ' ' );
}

void LogifyProgram::SetDataPath(std::string path)
{
	m_dataPath = path;
	std::cout << "Program data should be located at \"" << m_dataPath << "\"..." << std::endl;
}

void LogifyProgram::Setup()
{
	m_dataGen.Setup(m_dataPath);
	std::hash<std::string> stringHash;

	std::cout << std::left;

	// Create employees
	for (int i = 0; i < 100; i++)
	{
		std::string fname = m_dataGen.GetName();
		Employee newEmployee;
		newEmployee.m_id = 1337 + i*3;
		newEmployee.m_email = m_dataGen.GetEmail();
		newEmployee.m_name = fname + " " + m_dataGen.GetLetter() + ".";
		newEmployee.m_passwordHash = stringHash(fname);

		std::cout << std::setw(30) << newEmployee.m_name << std::setw( 10 );

		m_employees.Push(newEmployee.m_id, newEmployee);
	}
}

void LogifyProgram::Cleanup()
{
}

void LogifyProgram::Run()
{
	Setup();

	std::cout << std::endl << std::string(80, '-') << std::endl;
	std::cout << "LOGIN" << std::endl << std::endl;
	std::cout << "Enter employee ID: ";
	size_t id;
	std::cin >> id;

	Employee employee;

	try
	{
		employee = m_employees.Get(id);
	}
	catch (const Exception::ItemNotFoundException& ex)
	{
		std::cout << "ERROR: " << ex.what() << std::endl;
		return;
	}

	std::cout << "EMAIL: " << employee.m_email << std::endl;

	std::string password;
	std::cout << "(Note: passwords are the first name)..." << std::endl;
	std::cout << "Enter password: ";
	std::cin.ignore();
	getline(std::cin, password);

	std::hash<std::string> stringHash;
	size_t hash;

	hash = stringHash(password);
	std::cout << "YOUR    PASSWORD HASH: [" << hash << "]" << std::endl;

	std::cout << "CORRECT PASSWORD HASH: [" << employee.m_passwordHash << "]" << std::endl;


	// Check to see if password hashes match
	if (hash == employee.m_passwordHash)
	{
		std::cout << std::endl << "LOGIN SUCCESSFUL" << std::endl;
	}
	else
	{
		std::cout << std::endl << "LOGIN FAILED" << std::endl;
		return;
	}

	std::cout << std::endl << "EMPLOYEE INFO:" << std::endl;
	employee.Display();

	std::cout << std::endl;
}

}
