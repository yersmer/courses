#ifndef _DATAGEN_CALLWAVE
#define _DATAGEN_CALLWAVE

#include <string>
#include <vector>
#include <fstream>
#include <iostream>

namespace Callwave
{

class DataGen
{
public:
	void Setup(std::string dataPath)
	{
		std::cout << "Input data path: \"" << dataPath << "\"" << std::endl;
		std::ifstream input(dataPath);
		std::string buffer;
		while (getline(input, buffer))
		{
			m_names.push_back(buffer);
		}
	}

	std::string GetRandomName()
	{
		std::string name = m_names[rand() % m_names.size()] + " " + char(rand() % 25 + 65) + ".";
		return name;
	}

private:
	std::vector<std::string> m_names;
};

}

#endif
