#include "StringUtil.h"

// C++ Library includes
#include <iostream>
#include <string>
#include <sstream>
#include <stdexcept>

namespace Utilities
{

//! Converts a string to an integer
int StringUtil::StringToInt( const std::string& str )
{
    int value = 0;
    try
    {
        value = stoi( str );
    }
    catch( const std::invalid_argument& ex )
    {
        return -1;
    }

    return value;

}

//! Converts a string to an unsigned integer
unsigned int StringUtil::StringToUInt( const std::string& str )
{
    std::istringstream ss( str );
    unsigned int val;
    ss >> val;
    return val;
}

//! Converts a string to a float value
float StringUtil::StringToFloat( const std::string& str )
{
    float value = 0;
    try
    {
        value = stof( str );
    }
    catch( const std::invalid_argument& ex )
    {
//        std::cout << "Cannot convert \"" << str << "\" to int!" << std::endl;
        return -1;
    }

    return value;
}

//! Changes an entire string to upper case
std::string StringUtil::ToUpper( const std::string& val )
{
    std::string upper = "";
    for ( unsigned int i = 0; i < val.size(); i++ )
    {
        upper += toupper( val[i] );
    }
    return upper;
}

//! Changes an entire string to lower case
std::string StringUtil::ToLower( const std::string& val )
{
    std::string upper = "";
    for ( unsigned int i = 0; i < val.size(); i++ )
    {
        upper += tolower( val[i] );
    }
    return upper;
}

//! Aligns text to a column, returning a string.
std::string StringUtil::ColumnText( int colWidth, const std::string& text )
{
    std::string adjusted = text;
    for ( int i = 0; i < colWidth - text.size(); i++ )
    {
        adjusted += " ";
    }
    return adjusted;
}

//! Returns true if the full string (haystack) contains the search term (needle), with optional case sensitivity.
bool StringUtil::Contains( std::string haystack, std::string needle, bool caseSensitive /* = true */ )
{
    std::string a = haystack;
    std::string b = needle;

    if ( !caseSensitive )
    {
        for ( unsigned int i = 0; i < a.size(); i++ )
        {
            a[i] = tolower( a[i] );
        }

        for ( unsigned int i = 0; i < b.size(); i++ )
        {
            b[i] = tolower( b[i] );
        }
    }

    return ( a.find( b ) != std::string::npos );
}

//! Find the position of a term (needle) in the full string (haystack)
int StringUtil::Find( std::string haystack, std::string needle )
{
    return haystack.find( needle );
}

//! Searches the fulltext for an item (findme) and replaces it with the replacewith string.
std::string StringUtil::Replace( std::string fulltext, std::string findme, std::string replacewith )
{
    std::string updated = fulltext;
    int index = updated.find( findme );

    while ( index != std::string::npos )
    {
        // Make the replacement
        updated = updated.replace( index, findme.size(), replacewith );

        // Look for the item again
        index = updated.find( findme );
    }

    return updated;
}

//! Splits up a delimited string into a vector of strings
std::vector<std::string> StringUtil::Split( std::string str, std::string delim )
{
    std::vector<std::string> data;

    int begin = 0;
    int end = 0;

    while ( str.find( delim ) != std::string::npos )
    {
        end = str.find( delim );

        // Get substring up until delimiter
        int length = end - begin;
        std::string substring = str.substr( begin, length );
        data.push_back( substring );

        // Remove this chunk of std::string
        str = str.erase( begin, length+1 );
    }

    // Put last std::string in structure
    data.push_back( str );

    return data;
}

//! Splits up a delimited string into a vector of strings, pays attention to opening and closing quotations.
std::vector<std::string> StringUtil::CsvSplit( std::string str, char delim )
{
    std::vector<std::string> data;

    int begin = 0;

    bool singleQuote = false;
    bool doubleQuote = false;

    for ( unsigned int i = 0; i < str.size(); i++ )
    {

        if ( str[i] == '\'' )
        {
            singleQuote = !singleQuote;
        }
        else if ( str[i] == '"' )
        {
            doubleQuote = !doubleQuote;
        }
        else if ( str[i] == delim )
        {
            if ( singleQuote || doubleQuote )
            {
                // We're inside quotes, ignore this bastard.
            }
            else
            {
                // OK we wanna pull this data
                int length = i - begin;
                std::string substring = str.substr( begin, length );
                data.push_back( substring );
                begin = i+1;
            }
        }
    }

    // Pull the last chunk
    int length = str.size() - begin;
    std::string substring = str.substr( begin, length );
    data.push_back( substring );

    return data;
}

//! Converts a boolean to "TRUE" or "FALSE" string for pretty printing
std::string StringUtil::BoolToText( const bool value )
{
    return ( value ) ? "TRUE" : "FALSE";
}

void StringUtil::PrintPwd()
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
        system( "echo %cd%" );
    #else
        system( "pwd" );
    #endif
}

} // End of namespace
