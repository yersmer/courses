#include "Namespace_Utilities/Logger.h"
#include "Program_Base/Program.h"

#include <iostream>
using namespace std;

int main()
{
    // Initialize the debug logger
    Utilities::Logger::Setup();

    Program program;
    program.Setup();
    program.Run();
    program.Teardown();

    // Clean up the debug logger
    Utilities::Logger::Cleanup();

    return 0;
}







