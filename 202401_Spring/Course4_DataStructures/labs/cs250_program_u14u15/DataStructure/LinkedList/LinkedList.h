#ifndef LINKED_LIST_HPP
#define LINKED_LIST_HPP

// C++ Library includes
#include <iostream>
#include <string>
#include <stdexcept>
using namespace std;

// Project includes
#include "LinkedListNode.h"
#include "../Interfaces/ILinearDataStructure.h"
#include "../../Exceptions/NotImplementedException.h"
#include "../../Exceptions/InvalidIndexException.h"
#include "../../Exceptions/NullptrException.h"
#include "../../Exceptions/NotImplementedException.h"
#include "../../Exceptions/StructureFullException.h"
#include "../../Exceptions/StructureEmptyException.h"
#include "../../Exceptions/InvalidIndexException.h"
#include "../../Exceptions/ItemNotFoundException.h"
#include "../../Exceptions/NullptrException.h"
#include "../../Namespace_Utilities/Logger.h"
#include "../../Namespace_Utilities/StringUtil.h"

namespace DataStructure
{

template <typename T>
class LinkedList : public ILinearDataStructure<T>
{
private:
    /* Member Variables */
    DoublyLinkedListNode<T>* m_ptrFirst;
    DoublyLinkedListNode<T>* m_ptrLast;
    int m_itemCount;

public:
    /* Member Functions */
    LinkedList();
    virtual ~LinkedList();

    virtual void PushFront( T newData );
    virtual void PushBack( T newData );
    virtual void PushAt( T newItem, int index );

    virtual void PopFront();
    virtual void PopBack();
    virtual void PopAt( int index );

    virtual T& GetFront();
    virtual T& GetBack();
    virtual T& GetAt( int index );

    virtual int Size() const;

    virtual bool IsEmpty() const;

    virtual void Clear();

    bool IsInvalidIndex( int index ) const;

    friend class LinkedListTester;
    friend class QueueTester;
    friend class StackTester;
};

/* -----------------------------------------------------------------------------------------*/
/* ----------------------------------------------------------------------------- LinkedList */
template <typename T>
LinkedList<T>::LinkedList()
{
    m_ptrFirst = nullptr;
    m_ptrLast = nullptr;
    m_itemCount = 0;
}

/* -----------------------------------------------------------------------------------------*/
/* ---------------------------------------------------------------------------- ~LinkedList */
template <typename T>
LinkedList<T>::~LinkedList()
{
    // TODO: Call Clear() once it is implemented
    Clear();
}

/* -----------------------------------------------------------------------------------------*/
/* ---------------------------------------------------------------------------------- Clear */

template <typename T>
void LinkedList<T>::Clear()
{
    while ( m_ptrFirst != nullptr )
    {
        if ( m_itemCount == 1 )
        {
            delete m_ptrFirst;
            m_ptrFirst = nullptr;
            m_ptrLast = nullptr;
        }
        else
        {
            m_ptrFirst = m_ptrFirst->m_ptrNext;
            delete m_ptrFirst->m_ptrPrev;
            m_ptrFirst->m_ptrPrev = nullptr;
        }

        m_itemCount--;
    }
}


/* -----------------------------------------------------------------------------------------*/
/* ------------------------------------------------------------------------------ PushFront */
template <typename T>
void LinkedList<T>::PushFront( T newData )
{
    throw Exception::NotImplementedException( "PushFront" ); // Erase this once you work on this function
}


/* -----------------------------------------------------------------------------------------*/
/* ------------------------------------------------------------------------------- PushBack */
template <typename T>
void LinkedList<T>::PushBack( T newData )
{
    throw Exception::NotImplementedException( "PushBack" ); // Erase this once you work on this function
}


/* -----------------------------------------------------------------------------------------*/
/* --------------------------------------------------------------------------------- PushAt */
template <typename T>
void LinkedList<T>::PushAt( T newItem, int index )
{
    throw Exception::NotImplementedException( "PushAt" ); // Erase this once you work on this function
}

/* -----------------------------------------------------------------------------------------*/
/* ------------------------------------------------------------------------------- PopFront */
template <typename T>
void LinkedList<T>::PopFront()
{
    throw Exception::NotImplementedException( "PopFront" ); // Erase this once you work on this function
}


/* -----------------------------------------------------------------------------------------*/
/* -------------------------------------------------------------------------------- PopBack */
template <typename T>
void LinkedList<T>::PopBack()
{
    throw Exception::NotImplementedException( "PopBack" ); // Erase this once you work on this function
}

/* -----------------------------------------------------------------------------------------*/
/* ---------------------------------------------------------------------------------- PopAt */
template <typename T>
void LinkedList<T>::PopAt( int index )
{
    throw Exception::NotImplementedException( "PopAt" ); // Erase this once you work on this function
}

/* -----------------------------------------------------------------------------------------*/
/* ------------------------------------------------------------------------------- GetFront */
template <typename T>
T& LinkedList<T>::GetFront()
{
    throw Exception::NotImplementedException( "GetFront" ); // Erase this once you work on this function
}


/* -----------------------------------------------------------------------------------------*/
/* -------------------------------------------------------------------------------- GetBack */
template <typename T>
T& LinkedList<T>::GetBack()
{
    throw Exception::NotImplementedException( "GetBack" ); // Erase this once you work on this function
}


/* -----------------------------------------------------------------------------------------*/
/* ---------------------------------------------------------------------------------- GetAt */
template <typename T>
T& LinkedList<T>::GetAt( int index )
{
    throw Exception::NotImplementedException( "GetAt" ); // Erase this once you work on this function
}


/* -----------------------------------------------------------------------------------------*/
/* -------------------------------------------------------------------------------- IsEmpty */
/**
    @return bool    Return true if there are no items stored in the list, and false otherwise.
*/
template <typename T>
bool LinkedList<T>::IsEmpty() const
{
    return ( m_itemCount == 0 );
}


/* -----------------------------------------------------------------------------------------*/
/* ----------------------------------------------------------------------------------- Size */
template <typename T>
int LinkedList<T>::Size() const
{
    return m_itemCount;
}

/* -----------------------------------------------------------------------------------------*/
/* ------------------------------------------------------------------------- IsInvalidIndex */
template <typename T>
bool LinkedList<T>::IsInvalidIndex( int index ) const
{
    return ( index < 0 || index >= m_itemCount );
}

}

#endif
