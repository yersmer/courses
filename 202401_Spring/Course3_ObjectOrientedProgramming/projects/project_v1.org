# -*- mode: org -*-

*Important links*

- Section 300 Team:
  - Repository: https://gitlab.com/rsingh13-student-repos/2024-01_cs235/shopazon-team-300
  - Documentation: https://shopazon-team-300-rsingh13-student-repos-2024-01-7133028e3413fb.gitlab.io/
- Section 301 Team:
  - Repository: https://gitlab.com/rsingh13-student-repos/2024-01_cs235/shopazon-team-301
  - Documentation: https://shopazon-team-301-rsingh13-student-repos-2024-01-ef37076ccee798.gitlab.io/

---------------------------------------------

*About*

[[file:images/shopazon-program.png]]

For the semester project I've built out a codebase based on *Amazon*.
There are objects created for different parts of the underlying shopping platform system.
You will be selecting one area to work in.

We will also be making use of more of the tools available on the GitLab webpage,
such as Issues (tickets) for pulling tasks, merge request reviews,
and an automated build system.

We will also have *documentation* for the codebase as a whole, and you will be
familiarizing yourself with this codebase as you work with it.

This is meant to imitate a real-world development scenario and teach you more
about software development.

- *Project v1: Due March 31st EOD*
- *Project v2: Due April 14th EOD*
- *Project v3: Due April 28th EOD*

---------------------------------------------

*Overview*

- You will CLONE the shared group repository to your computer to work on the item.
  - Use =git clone= and then the HTTPS link from the GitLab repository page.
  - Clone this new repository somewhere on your hard drive - NOT IN YOUR OTHER CS235 REPOSITORY FOLDER!
- You will select an *Issue* to work on. (GitLab page $\to$ Issues page $\to$ Select an issue and assign it to yourself).
  - The issue will include the *acceptance criteria* that you will use to help implement the required tickets.
  - v1, v2, and v3 of the project will have different sets of tickets to work on.
- You will *submit a merge request* once you've implemented the feature.
- You will *review someone else's merge request* changes and give productive feedback.
- After each "version" (v1, v2, v3), the instructor will review your changes, give feedback, and
  if the changes are satisfactory, they will *merge your work into the main branch* and *mark the ticket as complete*.

---------------------------------------------

*Where do I find information?*

- *Information about using GitLab* is located on the Repository front page README (Follow Repository link and scroll down).
  - The README here covers how to pull an issue, create a merge request, and review someone else's code.
- *Information about project requirements* is located in the Issue ticket description (view on GitLab).
  - The ticket description explains what is required for the feature you're implementing.
- *Information about the codebase* is located on the documentation webpage.
  - The documentation has information on all of the structures in the program and their relationships.
  - The documentation also covers details about how the program itself works.

---------------------------------------------

*What do I turn in on Canvas?*

- You will turn in the URL to your merge request. Also include the URL to the request that YOU REIVEWED in the comment field of the submission.
- You don't have to have YOUR merge request reviewed to submit, but you should have reivewed someone else's work.
- Feedback will be given and a follow up ticket for v2 will be given after grading.
