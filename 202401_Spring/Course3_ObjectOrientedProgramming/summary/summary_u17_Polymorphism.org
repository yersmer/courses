# -*- mode: org -*-

#+TITLE: Q&A Notes: Polymorphism
#+AUTHOR: Rachel Wil Sha Singh

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/rworgmode.css" />
#+HTML_HEAD: <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
#+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">
#+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script> <script>hljs.initHighlightingOnLoad();</script>

-----

* Interfaces
1. How do you create an "interface" in C++?
2. What does the virtual keyword to do a function?
3. What is a pure virtual function and how do you make a function pure virtual?
4. What is an abstract class?

* Polymorphism
1. What is polymorphism?
2. How do you utilize polymorphism?
