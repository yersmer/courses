# -*- mode: org -*-

* *Static Variables and Functions*

Static variables are a special type of variable in a class where
**all instances of the class** share the same member.
Another term you might hear is a **Class Variable**, whereas a normal member variable of a class would be an **Instance Variable**.

Let's say we are going to declare a **Cat** class,
and each cat has its own name, but we also want a counter
to keep track of how many Cats there are. The Cat counter
could be a static variable and we could write a static method
to return that variable's value.

#+BEGIN_SRC cpp
class Cat
{
public:
  Cat()
  {
    catCount++;
  }

  Cat( string name )
  {
    catCount++;
    m_name = name;
  }

  static int GetCount()
  {
    return catCount;
  }

private:
  string m_name;
  static int catCount;
};
#+END_SRC

Within a source file, we will need to initialize this static member.
This may go in the class' .cpp file outside of any of the function definitions.

#+BEGIN_SRC cpp
// Initialize static variable
int Cat::catCount = 0;
#+END_SRC

And then any time we create a new Cat object, that variable will
automatically add up, and every instance of the Cat will share
that variable and its value.

#+BEGIN_SRC cpp
int main()
{
  Cat catA, catB, catC;

  ;; These all display 3
  cout << catA.GetCount() << endl;
  cout << catB.GetCount() << endl;
  cout << catC.GetCount() << endl;
  cout << Cat::GetCount() << endl;

  return 0;
}
#+END_SRC

Beyond accessing a static method or member directly through an
**instantiated object**, we can also access it through
the class name itself, like this:

#+BEGIN_SRC cpp
cout << Cat::GetCount() << endl;
#+END_SRC



#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML

- Example usage: Manager class ::

In my game engine I use static member variables and functions for my *Manager* classes. These Managers
are meant to manage parts of the game, such as the Texture library, Audio library, Inputs, Menus, and so on.
Throughout the entire game, I don't create multiple *instances* of the =TextureManager=. Because the
functions and data are =static=, I can use this class across the entire project directly.

Declaring the TextureManager:

#+BEGIN_SRC cpp :class cpp
  class TextureManager
  {
  public:
    static std::string CLASSNAME;

    static void Add( const std::string& key, const std::string& path );
    static const sf::Texture& AddAndGet( const std::string& key, const std::string& path );
    static void Clear();
    static const sf::Texture& Get( const std::string& key );

  private:
    static std::map<std::string, sf::Texture> m_assets;
  };
#+END_SRC


Defining the member variables (top of TextureManager.cpp):

#+BEGIN_SRC cpp :class cpp
std::string TextureManager::CLASSNAME = "TextureManager";
std::map<std::string, sf::Texture> TextureManager::m_assets;
#+END_SRC

Function definitions look the same:

#+BEGIN_SRC cpp :class cpp
  void TextureManager::Add( const std::string& key, const std::string& path )
  {
    sf::Texture texture;
    if ( !texture.loadFromFile( path ) )
      {
        // Error
        Logger::Error( "Unable to load texture at path \"" + path + "\"", "TextureManager::Add" );
        return;
      }

    m_assets[ Helper::ToLower( key ) ] = texture;
  }

  const sf::Texture& TextureManager::Get( const std::string& key )
  {
    if ( m_assets.find( Helper::ToLower( key ) ) == m_assets.end() )
      {
        // Not found
        Logger::Error( "Could not find texture with key " + key, "TextureManager::Get" );
        throw std::runtime_error( "Could not find texture with key " + key + " - TextureManager::Get" );
      }

    return m_assets[ Helper::ToLower( key ) ];
  }
#+END_SRC

Calling the TextureManager functions:

#+BEGIN_SRC cpp :class cpp
  chalo::TextureManager::Add( "moose",   "Content/Graphics/Demos/moose.png" );

  // ...etc...
  sf::Sprite m_player;
  m_player.setTexture( chalo::TextureManager::Get( "moose" ) );
#+END_SRC



#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML


- Example usage: Singleton pattern ::

Further, we can use the *Singleton pattern* to create a class that can /only/ have one instance.

#+ATTR_HTML: :class hint
#+NAME: content
#+BEGIN_HTML
*Context:* A "Design Pattern" is kind of like a blueprint for a way to implement a structure.
These are structures that people have figured out how to build that end up being useful
in a lot of scenarios.
#+END_HTML

You can learn more about the Singleton pattern here: https://en.wikipedia.org/wiki/Singleton_pattern .

And more about Design Patterns here: https://en.wikipedia.org/wiki/Design_pattern .

