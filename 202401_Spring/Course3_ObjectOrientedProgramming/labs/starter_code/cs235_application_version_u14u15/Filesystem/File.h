#ifndef _FILE
#define _FILE

#include <string>
#include <fstream>
#include <vector>
using namespace std;

class File
{
public:
    File();
    File( string path, string name );
    File( string path, string name, string ex );
    File( const File& other );

    void OpenFile( string path, string name, string ext="txt" );
    void Write( string line );

    void Display() const;

    std::string GetFullname() const;
    std::vector<std::string> GetContents() const;

private:
    string path;
    string name;
    string ext;
    vector<string> contents;

    friend class FileTester; // This helps the tester work
};

#endif
