#include "AccountTester.h"

#include <iostream>
#include <iomanip>
#include <string>

AccountTester::AccountTester()
    : RED( "\033[0;31m" ), GRN( "\033[0;32m" ), BOLD( "\033[0;35m" ), CLR( "\033[0m" )
{
    std::cout << std::left;
}

void AccountTester::Run()
{
    Test_Constructors();
    Test_SetUsername();
    Test_SetPasswordHash();
    Test_GetId();
    Test_GetUsername();
    Test_CorrectPassword();
}

void AccountTester::Test_Constructors()
{
    std::string test_set_name = "** AccountTester Test_Constructors";
    std::cout << std::endl << test_set_name << std::string( 80 - test_set_name.size(), '*' ) << std::endl;

    {
        std::string test_name = "Account account( 0 ), check values";
        Account account( 0 );

        size_t      expected_id = 0;
        std::string expected_username = "";
        size_t      expected_password_hash = 0;

        if (
            account.id              == expected_id &&
            account.username        == expected_username &&
            account.password_hash   == expected_password_hash
        )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << std::setw( 40 ) << "VARIABLE"              << std::setw( 20 ) << "EXPECTED"                << std::setw( 20 ) << "ACTUAL" << std::endl << std::string( 80, '-' ) << std::endl;
            std::cout << std::setw( 40 ) << "account.id"            << std::setw( 20 ) << expected_id               << std::setw( 20 ) << account.id << std::endl;
            std::cout << std::setw( 40 ) << "account.username"      << std::setw( 20 ) << expected_username         << std::setw( 20 ) << account.username << std::endl;
            std::cout << std::setw( 40 ) << "account.password_hash" << std::setw( 20 ) << expected_password_hash    << std::setw( 20 ) << account.password_hash << std::endl;
            std::cout << std::endl;
        }
    } std::cout << CLR;

    {
        std::string test_name = "Account account( 1, \"Bob\" ), check values";
        Account account( 1, "Bob" );

        size_t      expected_id = 1;
        std::string expected_username = "Bob";
        size_t      expected_password_hash = 0;

        if (
            account.id              == expected_id &&
            account.username        == expected_username &&
            account.password_hash   == expected_password_hash
        )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << std::setw( 40 ) << "VARIABLE"              << std::setw( 20 ) << "EXPECTED"                << std::setw( 20 ) << "ACTUAL" << std::endl << std::string( 80, '-' ) << std::endl;
            std::cout << std::setw( 40 ) << "account.id"            << std::setw( 20 ) << expected_id               << std::setw( 20 ) << account.id << std::endl;
            std::cout << std::setw( 40 ) << "account.username"      << std::setw( 20 ) << expected_username         << std::setw( 20 ) << account.username << std::endl;
            std::cout << std::setw( 40 ) << "account.password_hash" << std::setw( 20 ) << expected_password_hash    << std::setw( 20 ) << account.password_hash << std::endl;
            std::cout << std::endl;
        }
    } std::cout << CLR;

    {
        std::string test_name = "Account account( 1, \"Bob\" ), check values";
        Account account( 2, "Rai", 10 );

        size_t      expected_id = 2;
        std::string expected_username = "Rai";
        size_t      expected_password_hash = 10;

        if (
            account.id              == expected_id &&
            account.username        == expected_username &&
            account.password_hash   == expected_password_hash
        )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << std::setw( 40 ) << "VARIABLE"              << std::setw( 20 ) << "EXPECTED"                << std::setw( 20 ) << "ACTUAL" << std::endl << std::string( 80, '-' ) << std::endl;
            std::cout << std::setw( 40 ) << "account.id"            << std::setw( 20 ) << expected_id               << std::setw( 20 ) << account.id << std::endl;
            std::cout << std::setw( 40 ) << "account.username"      << std::setw( 20 ) << expected_username         << std::setw( 20 ) << account.username << std::endl;
            std::cout << std::setw( 40 ) << "account.password_hash" << std::setw( 20 ) << expected_password_hash    << std::setw( 20 ) << account.password_hash << std::endl;
            std::cout << std::endl;
        }
    } std::cout << CLR;

    std::cout << CLR;
}

void AccountTester::Test_SetUsername()
{
    std::string test_set_name = "** AccountTester Test_SetUsername";
    std::cout << std::endl << test_set_name << std::string( 80 - test_set_name.size(), '*' ) << std::endl;

    {
        std::string test_name = "account.SetUsername( \"xX_Gandalf_Xx\" ), check .username after";
        Account account( 0 );
        account.SetUsername( "xXX_Gandalf_Xx" );

        std::string expected_username = "xXX_Gandalf_Xx";

        if (
            account.username        == expected_username
        )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << std::setw( 40 ) << "VARIABLE"              << std::setw( 20 ) << "EXPECTED"                << std::setw( 20 ) << "ACTUAL" << std::endl << std::string( 80, '-' ) << std::endl;
            std::cout << std::setw( 40 ) << "account.username"      << std::setw( 20 ) << expected_username         << std::setw( 20 ) << account.username << std::endl;
            std::cout << std::endl;
        }
    } std::cout << CLR;

    {
        std::string test_name = "account.SetUsername( \"ectoBiologist\" ), check .username after";
        Account account( 0 );
        account.SetUsername( "ectoBiologist" );

        std::string expected_username = "ectoBiologist";

        if (
            account.username        == expected_username
        )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << std::setw( 40 ) << "VARIABLE"              << std::setw( 20 ) << "EXPECTED"                << std::setw( 20 ) << "ACTUAL" << std::endl << std::string( 80, '-' ) << std::endl;
            std::cout << std::setw( 40 ) << "account.username"      << std::setw( 20 ) << expected_username         << std::setw( 20 ) << account.username << std::endl;
            std::cout << std::endl;
        }
    } std::cout << CLR;


    std::cout << CLR;
}

void AccountTester::Test_SetPasswordHash()
{
    std::string test_set_name = "** AccountTester Test_SetPasswordHash";
    std::cout << std::endl << test_set_name << std::string( 80 - test_set_name.size(), '*' ) << std::endl;

    {
        std::string test_name = "account.SetPasswordHash( 123 ), check .password_hash after";
        Account account( 0 );
        account.SetPasswordHash( 123 );

        size_t expected_password_hash = 123;

        if (
            account.password_hash == expected_password_hash
        )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << std::setw( 40 ) << "VARIABLE"              << std::setw( 20 ) << "EXPECTED"                << std::setw( 20 ) << "ACTUAL" << std::endl << std::string( 80, '-' ) << std::endl;
            std::cout << std::setw( 40 ) << "account.password_hash" << std::setw( 20 ) << expected_password_hash    << std::setw( 20 ) << account.password_hash << std::endl;
            std::cout << std::endl;
        }
    } std::cout << CLR;

    {
        std::string test_name = "account.SetPasswordHash( 1337 ), check .password_hash after";
        Account account( 0 );
        account.SetPasswordHash( 1337 );

        size_t expected_password_hash = 1337;

        if (
            account.password_hash == expected_password_hash
        )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << std::setw( 40 ) << "VARIABLE"              << std::setw( 20 ) << "EXPECTED"                << std::setw( 20 ) << "ACTUAL" << std::endl << std::string( 80, '-' ) << std::endl;
            std::cout << std::setw( 40 ) << "account.password_hash" << std::setw( 20 ) << expected_password_hash    << std::setw( 20 ) << account.password_hash << std::endl;
            std::cout << std::endl;
        }
    } std::cout << CLR;

    std::cout << CLR;
}

void AccountTester::Test_GetId()
{
    std::string test_set_name = "** AccountTester Test_GetId";
    std::cout << std::endl << test_set_name << std::string( 80 - test_set_name.size(), '*' ) << std::endl;

    {
        std::string test_name = "Set .id = 10, check account.GetId()";
        Account account( 0 );
        account.id = 10;

        size_t expected_id = 10;

        if (
            account.GetId() == expected_id
        )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << std::setw( 40 ) << "VARIABLE"              << std::setw( 20 ) << "EXPECTED"                << std::setw( 20 ) << "ACTUAL" << std::endl << std::string( 80, '-' ) << std::endl;
            std::cout << std::setw( 40 ) << "account.GetId()"       << std::setw( 20 ) << expected_id               << std::setw( 20 ) << account.GetId() << std::endl;
            std::cout << std::endl;
        }
    } std::cout << CLR;

    {
        std::string test_name = "Set .id = 1337, check account.GetId()";
        Account account( 0 );
        account.id = 1337;

        size_t expected_id = 1337;

        if (
            account.GetId() == expected_id
        )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << std::setw( 40 ) << "VARIABLE"              << std::setw( 20 ) << "EXPECTED"                << std::setw( 20 ) << "ACTUAL" << std::endl << std::string( 80, '-' ) << std::endl;
            std::cout << std::setw( 40 ) << "account.GetId()"       << std::setw( 20 ) << expected_id               << std::setw( 20 ) << account.GetId() << std::endl;
            std::cout << std::endl;
        }
    } std::cout << CLR;

    std::cout << CLR;
}

void AccountTester::Test_GetUsername()
{
    std::string test_set_name = "** AccountTester Test_GetUsername";
    std::cout << std::endl << test_set_name << std::string( 80 - test_set_name.size(), '*' ) << std::endl;

    {
        std::string test_name = "Set .username = \"carcinoGeneticist\", check account.GetUsername()";
        Account account( 0 );
        account.username = "carcinoGeneticist";

        std::string expected_username = "carcinoGeneticist";

        if (
            account.GetUsername() == expected_username
        )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << std::setw( 40 ) << "VARIABLE"                  << std::setw( 20 ) << "EXPECTED"                << std::setw( 20 ) << "ACTUAL" << std::endl << std::string( 80, '-' ) << std::endl;
            std::cout << std::setw( 40 ) << "account.GetUsername()"     << std::setw( 20 ) << expected_username         << std::setw( 20 ) << account.GetUsername() << std::endl;
            std::cout << std::endl;
        }
    } std::cout << CLR;

    {
        std::string test_name = "Set .username = \"gallowsCalibrator\", check account.GetUsername()";
        Account account( 0 );
        account.username = "gallowsCalibrator";

        std::string expected_username = "gallowsCalibrator";

        if (
            account.GetUsername() == expected_username
        )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << std::setw( 40 ) << "VARIABLE"                  << std::setw( 20 ) << "EXPECTED"                << std::setw( 20 ) << "ACTUAL" << std::endl << std::string( 80, '-' ) << std::endl;
            std::cout << std::setw( 40 ) << "account.GetUsername()"     << std::setw( 20 ) << expected_username         << std::setw( 20 ) << account.GetUsername() << std::endl;
            std::cout << std::endl;
        }
    } std::cout << CLR;

    std::cout << CLR;
}

void AccountTester::Test_CorrectPassword()
{
    std::string test_set_name = "** AccountTester Test_CorrectPassword";
    std::cout << std::endl << test_set_name << std::string( 80 - test_set_name.size(), '*' ) << std::endl;

    {
        std::string test_name = "set .password_hash = 100, check .CorrectPassword( 100 )";
        Account account( 0 );
        account.password_hash = 100;

        bool expected_out = true;
        bool actual_out = account.CorrectPassword( 100 );

        if (
            actual_out == expected_out
        )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << std::setw( 40 ) << "VARIABLE"                       << std::setw( 20 ) << "EXPECTED"                << std::setw( 20 ) << "ACTUAL" << std::endl << std::string( 80, '-' ) << std::endl;
            std::cout << std::setw( 40 ) << "account.CorrectPassword( 100 )" << std::setw( 20 ) << BoolToStr( expected_out ) << std::setw( 20 ) << BoolToStr( actual_out ) << std::endl;
            std::cout << std::endl;
        }
    } std::cout << CLR;

    {
        std::string test_name = "set .password_hash = 1337, check .CorrectPassword( 1337 )";
        Account account( 0 );
        account.password_hash = 1337;

        bool expected_out = true;
        bool actual_out = account.CorrectPassword( 1337 );

        if (
            actual_out == expected_out
        )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << std::setw( 40 ) << "VARIABLE"                       << std::setw( 20 ) << "EXPECTED"                << std::setw( 20 ) << "ACTUAL" << std::endl << std::string( 80, '-' ) << std::endl;
            std::cout << std::setw( 40 ) << "account.CorrectPassword( 100 )" << std::setw( 20 ) << BoolToStr( expected_out ) << std::setw( 20 ) << BoolToStr( actual_out ) << std::endl;
            std::cout << std::endl;
        }
    } std::cout << CLR;

    {
        std::string test_name = "set .password_hash = 1337, check .CorrectPassword( 100 )";
        Account account( 0 );
        account.password_hash = 1337;

        bool expected_out = false;
        bool actual_out = account.CorrectPassword( 100 );

        if (
            actual_out == expected_out
        )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << std::setw( 40 ) << "VARIABLE"                       << std::setw( 20 ) << "EXPECTED"                << std::setw( 20 ) << "ACTUAL" << std::endl << std::string( 80, '-' ) << std::endl;
            std::cout << std::setw( 40 ) << "account.CorrectPassword( 100 )" << std::setw( 20 ) << BoolToStr( expected_out ) << std::setw( 20 ) << BoolToStr( actual_out ) << std::endl;
            std::cout << std::endl;
        }
    } std::cout << CLR;

    {
        std::string test_name = "set .password_hash = 100, check .CorrectPassword( 1337 )";
        Account account( 0 );
        account.password_hash = 100;

        bool expected_out = false;
        bool actual_out = account.CorrectPassword( 1337 );

        if (
            actual_out == expected_out
        )
        {
            std::cout << GRN << "[PASS] " << test_name << std::endl;
        }
        else
        {
            std::cout << RED << "[FAIL] " << test_name << std::endl;
            std::cout << std::setw( 40 ) << "VARIABLE"                       << std::setw( 20 ) << "EXPECTED"                << std::setw( 20 ) << "ACTUAL" << std::endl << std::string( 80, '-' ) << std::endl;
            std::cout << std::setw( 40 ) << "account.CorrectPassword( 100 )" << std::setw( 20 ) << BoolToStr( expected_out ) << std::setw( 20 ) << BoolToStr( actual_out ) << std::endl;
            std::cout << std::endl;
        }
    } std::cout << CLR;

    std::cout << CLR;
}
