#ifndef _ACCOUNT_MANAGER_TESTER
#define _ACCOUNT_MANAGER_TESTER

#include "AccountManager.h"

// !! The tests have already been implemented !!

class AccountManagerTester
{
public:
    AccountManagerTester();
    void Run();
    void Test_CreateAccount();
    void Test_GetAccount();
    void Test_GetIndexOfAccount();
    void Test_DeleteAccount();

private:
    const std::string RED;
    const std::string GRN;
    const std::string BOLD;
    const std::string CLR;

    std::string BoolToStr( bool val )
    {
        return ( val ) ? "true" : "false";
    }
};

#endif
