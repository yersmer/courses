# -*- mode: org -*-

#+ATTR_HTML: :class action-item
#+NAME: content
#+BEGIN_HTML

[[file:../../images/comics/pixel-goals.png]] *Goals:*
  - Practice writing and using overloaded functions
  - Practice writing and using functions with default parameters



[[file:../../images/comics/pixel-turnin.png]] *Turn-in:*
  - 1. You'll commit your code to your repository, create a *merge request*, and submit the merge request URL in Canvas. (Instructions in document.)



[[file:../../images/comics/pixel-practice.png]] *Practice programs and graded program:*
- This assignment contains several "practice" program folders,
  and a "graded" program folder. Only the "graded" program is required.
- The practice programs are there to help you practice with the topics before
  tackling the larger graded program. If you feel confident in the topic already,
  you may go ahead and work on the graded program. If you feel like you need
  additional practice first, that's what the practice assignments are for.
- The *graded program* assignment contains unit tests to help verify your code.
- You can score up to 100% turning in just the *graded program* assignment.
- You can turn in the *practice* assignments for *extra credit points*.



[[file:../../images/comics/pixel-fight.png]] *Stuck on the assignment? Not sure what to do next?*
- Continue scrolling down in the documentation and see if you're missing anything.
- Try skimming through the entire assignment before getting started,
  to get a high-level overview of what we're going to be doing.
- If you're stuck on a practice program, try working on a different one and come back to it later.
- If you're stuck on the graded program, try the practice programs first.

#+END_HTML


------------------------------------------------
* *Setup: Starter code and new branch*

Start this assignment by checking out =main=, pulling latest to get the files, and creating a new branch...

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
git checkout main
git pull
git checkout -b u12u13
#+END_SRC

TWO DIFFERENT FOLDERS WILL BE PULLED!

*PRACTICE PROGRAMS:* Practice programs for this lab are located in the =u12u13_Overloading_DefaultParams= folder
in your repository.

#+begin_src artist
.
├── practice1_overload_functions
│   └── overloadfx.cpp
├── practice2_default_params
│   └── defaultparams.cpp
└── practice3_in_classes
    ├── File.cpp
    ├── File.h
    ├── fileout
    └── main.cpp
#+end_src

*GRADED PROGRAM:* For this and future assignents we will be using the =cs235_application= for graded assignments.

#+begin_src artist
.
├── Filesystem
│   ├── File.cpp
│   ├── File.h
│   ├── FileTester.cpp
│   └── FileTester.h
├── main.cpp
├── Namespace_Utilities
│   ├── ScreenDrawer.cpp
│   └── ScreenDrawer.hpp
├── Program
│   ├── Program.cpp
│   └── Program.h
├── Project_CodeBlocks
│   ├── cs235_program.cbp
│   ├── cs235_program.depend
│   └── cs235_program.layout
├── Project_Makefile
│   └── Makefile
└── Project_VisualStudio2022
    ├── CS235_Application.sln
    ├── CS235_Application.vcxproj
    └── CS235_Application.vcxproj.filters
#+end_src


------------------------------------------------
* *Practice programs*

#+ATTR_HTML: :class action-item
#+NAME: content
#+BEGIN_HTML
SPRING 2024 NOTE: I accidentally added the "solution" version of all the practice programs to everybody's repositories
and I don't feel like fixing this so you can look over these to learn from if you want,
or you can erase the code and implement them for practice.
#+END_HTML


** *=practice1_overload_functions=*


#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
NAME                                    PRICE               IN STOCK
--------------------------------------------------------------------------------
Playstation 1 game
Playstation 2 game                      39.99
Playstation 3 game                      49.99
Playstation 4 game                      59.99               10
Playstation 5 game                      79.99               30
#+END_SRC

For this program, we're going to create 3 overloaded standalone functions that will format data in a table:

1. =void DisplayProduct( string name )=
2. =void DisplayProduct( string name, float price )=
3. =void DisplayProduct( string name, float price, int quantity )=

Within =main()=, a table header is set up:

#+BEGIN_SRC cpp :class cpp
cout << left << setprecision( 2 ) << fixed;
cout << setw( 40 ) << "NAME" << setw( 20 ) << "PRICE" << setw( 20 ) << "IN STOCK" << endl;
cout << string( 80, '-' ) << endl;
#+END_SRC

The "Name" column gets 40 spaces, the "Price" column gets 20 spaces, and the "In stock" column gets 20 spaces.

Within each of the overloaded =DisplayProduct= functions, follow this example
to write out whatever data is available (e.g., just name, or name and price, or name and price and quantity).

The resulting output should look like above.


#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *=practice2_default_params=*

This is the same program as practice1, except we create ONE =DisplayProduct= function, and set
default parameters for the =price= and =quantity= parameters.

Note that usually you'd be putting your function declaration in a separate .h file:

#+BEGIN_SRC cpp :class cpp
  // Default parameters specified for price and quantity
  void DisplayProduct( string name, float price = 0, int quantity = 0 );
#+END_SRC

And the function definition goes in a .cpp file, and does not have the default parameter values in the parameter list:

#+BEGIN_SRC cpp :class cpp
  void DisplayProduct( string name, float price, int quantity )
  {
      // ...
  }
#+END_SRC


#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *=practice3_in_classes=*

This program has a =File= class that will use overloaded functions and default parameters.
It is /similar/ to the graded program, but it is not exactly the same, so don't get confused by the differences!

UML Diagram:

#+ATTR_HTML: :class uml
| File                                                      |          |
|-----------------------------------------------------------+----------|
| - =name=                                                  | : string |
| - =ext=                                                   | : string |
|-----------------------------------------------------------+----------|
| + =File()=                                                |          |
| + =File( name: string )=                                  |          |
| + =File( name: string, ex: string )=                      |          |
| + =File( other: const File& )=                            |          |
| + =CreateFile( name: string, ext: string, text: string )= | : void   |

The =CreateFile= function should use default parameters, setting =ext= to "txt" and =text= to "hello" if nothing is given.


#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML

*Function implementation:*

- *=File::File()=*
  Call the =CreateFile= function, passing in "Default" as the name.

- *=File::File( string name )=*
  Call the =CreateFile= function, passing in =name=.

- *=File::File( string name, string ext )=*
  Call the =CreateFile= function, passing in =name= and =ext=.

- *=File::File( const File& other )=*
  Call the =CreateFile= function, passing in the =other.name=, =other.ext=, and some text like "Copied file".

- *=void File::CreateFile( string name, string ext, string text )=*
  - Set =this->name= to the =name= passed in.
  - Set =this->ext= to the =ext= passed in.
  - Create an =ofstream= object, open the file at ="fileout/" + name + "." + ext=.
  - Write the =text= to the ofstream object.
  - Close the ofstream object.

#+ATTR_HTML: :class hint
#+NAME: content
#+BEGIN_HTML
*Context:* Using the =this->= pointer refers back to the class object that you're currently within.
=this->= can be used to access its own member variables and functions. It is optional, but can be used for clarity.
#+END_HTML


#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML

*Creating a basic program:*

Within =main()=, test the various ways to create a file like this:

#+BEGIN_SRC cpp :class cpp
#include <iostream>
using namespace std;

#include "File.h"

int main()
{
    File fileA;

    File fileB( "fileb" );

    File fileC( "filec", "html" );

    File fileD( fileC );

    return 0;
}
#+END_SRC

#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*Testing the program:*


After building and running the program, your text files will be created in the *fileout* folder:

[[file:images/lab_u12u13_OverloadFunctionsDefaultParams_outputs.png]]

Open them up and make sure they have the proper contents within.


------------------------------------------------
* *Graded program*
** *FYI: cs235 application*

For this set of units and future units the graded part of the labs will be in the =cs235_application=.

At the moment, the menus are here but the program won't work until you implement the =File= class.
These screenshots show a finished version.

Starting off, the application will show a main menu:

[[file:images/lab_u12u13_OverloadFunctionsDefaultParams_program1.png]]

From the main menu, you can select *Create document*.

[[file:images/lab_u12u13_OverloadFunctionsDefaultParams_program2.png]]

Here, you select what type of document to create.

[[file:images/lab_u12u13_OverloadFunctionsDefaultParams_program3.png]]

Then you enter in lines of text to the file, then type =:q= and hit ENTER to finish.

Back at the main menu, if you select *View documents*, the documents you've created will show up.
You can select a document and it will show the contents of the file:

[[file:images/lab_u12u13_OverloadFunctionsDefaultParams_program4.png]]

At the moment, no /actual/ files are created on your computer, these are just abstractions within the =File= class.

*Note: You can also run the automated tests from the main menu with the "11" option.*

------------------------------------------------
** *The File class*

The File class declaration has already been declared, and you will be implementing its functions.
Please take note of the File class and its member variables to help you with your implementation.

UML Diagram:

#+ATTR_HTML: :class uml
| File                                                    |                  | Notes                 |
|---------------------------------------------------------+------------------+-----------------------|
| - =path=                                                | : string         |                       |
| - =name=                                                | : string         |                       |
| - =ext=                                                 | : string         |                       |
| - =contents=                                            | : vector<string> |                       |
|---------------------------------------------------------+------------------+-----------------------|
| + =File()=                                              |                  |                       |
| + =File( path: string, name: string )=                  |                  |                       |
| + =File( path: string, name: string, ex: string )=      |                  |                       |
| + =File( other: const File& )=                          |                  |                       |
| + =OpenFile( path: string, name: string, ext: string )= | : void           | ext defaults to "txt" |
| + =Write( line: string )=                               | : void           |                       |
| + =Display()=                                           | : void           |                       |
| + =GetFullname()=                                       | : string         |                       |
| + =GetContents()=                                       | : vector<string> |                       |





#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *=File::File()=*

Call the OpenFile function, passing in default values (like "NOTSET") for all the arguments.

#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *=File::File( string path, string name )=*

Call the OpenFile function, passing the path, and name to that function.

#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *=File::File( string path, string name, string ext )=*

Call the OpenFile function, passing the path, name, and ext to that function.

#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *=File::File( const File& other )=*

Call the OpenFile function, passing in the =other= object's path, name plus " (copy)", and extension.
Afterwards, set =this->contents= to the =other= object's contents.

#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *=void File::OpenFile( string path, string name, string ext )=*

Set the member variables =this->path=, =this->name=, and =this->ext= to the
values passed in as the parameters.

#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *=void File::Write( string line )=*

Push the =line= given into the =this->contents= vector.

#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *=void File::Display() const=*

Iterate over all the lines in the file, displaying each one on its own line.
Also display the file's path, name, and extention.

#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *=std::string File::GetFullname() const=*

Return the full path of the file, in this form: <PATH><NAME>.<EXT>

#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *=std::vector<std::string> File::GetContents() const=*

Return the =this->contents= member variable.


#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *Testing the program*

Run the automated tester by selecting option 11 from the main menu. All tests should pass.

You can also manually test the program by choosing option 1 to create a document or two,
and selecting option 2 to view the documents and view their contents.



------------------------------------------------
* *Turning in the assignment*

*Screenshot:* Before finishing up, run the automated tests and take a screenshot
of all of your tests passing. Save the file somewhere as a .png.

*Back up your code:* Open Git Bash in the base directory of your repository folder.

1. Use =git status= to view all the changed items (in red).
2. Use =git add .= to add all current changed items.
3. Use =git commit -m "INFO"= to commit your changes, change "INFO" to a more descriptive message.
4. Use =git push -u origin BRANCHNAME= to push your changes to the server.
5. On GitLab.com, go to your repository webpage. There should be a button to
   *Create a new Merge Request*. You can leave the default settings and create the merge request.
6. Copy the URL of your merge request.

*Turn in on Canvas:*
1. Find the assignment on Canvas and click *"Start assignment"*.
2. Select *Upload file* and upload your screenshot.
3. *Paste your GitLab URL* into the Comments box.
4. Click *"Submit assignment"* to finish.
