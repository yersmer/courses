# -*- mode: org -*-

Make sure the instructor knows your GitLab profile name. For example: =https://gitlab.com/YOURUSERNAME= .
They will set up your *class repository*, where you will be able to upload your code for the semester.

#+ATTR_HTML: :class hint
#+NAME: content
#+BEGIN_HTML
*Why are we using a repository?* Using *source control* software such as *git* is an important
tool of software developers in the real world. Now is a great chance for us to get practice
working with source control /before/ entering the real world. Plus, it has these benefits:

- Back up your source code to a server so you can access it anywhere and don't lose your code! (And you can refer back to it once the semester ends,
  for future class reference! :)
- Your instructor can view your repository and code from their own machine, which helps with debugging issues!
#+END_HTML

Once you're given access to your repository you'll be able to find it at a URL like =https://gitlab.com/users/YOURUSERNAME/contributed= , though you might want to bookmark the repository link as well!

A repositoroy link looks like this: =https://gitlab.com/rsingh13-student-repos/GROUP_NAME/REPOSITORY_NAME= - the instructor
will create the repository for you and have a basic structure set up, and then you can upload your work for each assignment.

[[file:reference/images/accessing_your_gitlab_repo_repoview.png]]

Your starting repository will probably look different from this one - this is just a past example! :)
