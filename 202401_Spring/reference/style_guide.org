# -*- mode: org -*-

* *Repository management*

- *Your merge request should only contain files related to the current feature or project you're working on* ::
  It shouldn't contain code from multiple assignments.


--------------------------------------------------------------------------------
* *User Interface design*

** *Prompts before inputs*

Before using an input statement (e.g., =cin=), use an output statement (e.g., =cout=) to display a message to the user.
Make sure you're clear about what kind of data you're expecting them to enter.


#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML

No:

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
_
#+END_SRC

If you use an input statement it doesn't show anything on the screen, so it looks like your program has crashed.

#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

Yes:

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
Please enter pet name: _
#+END_SRC

Use an output statement to display a message before getting the user's input.

#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML



--------------------------------------------------------------------------------

* *C++ Style Guide*

** *Naming conventions*

- Variables ::
  =lowerCamelCase= - Variable names should begin with a lower-case letter and each subsequent letter
  use an Upper Case, in a camel-case format.

  Additionally:

  - Variable names should be descriptive.
  - Avoid using single-letter variable names except with for loops.



- Member variables ::
  =m_lowerCamelCase= - Variables that belong to a class should be prefixed with =m_= to signify "member".



- Functions ::
  =CamelCase= - Function names should begin with an upper-case letter and be in CamelCase form.
  In some cases, using an underscore might be okay to group like-functions together, if it makes
  things more clear.



- Classes ::
  =CamelCase= - Class names should begin with an upper-case letter and be in CamelCase form.




#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *Best practices*

- *Basics*
  - You must have =int main=, not =void main=.
  - *Don't put an entire program in main()* - Split out the program into separate menu functions; ideally use a =Program= class that contains functions within it.
- *Naming conventions*
  - Variable, function, and class names should be descriptive.
- *Arrays*
  - Do not use a variable for an array's size; some compilers might support this but not all do. Traditional C arrays and STL arrays must have hard-coded integer literals or named constant integers as its size.
- *Functions*
  - Functions should perform the minimal amount of operations - a function should have a specific purpose.
- *Structs and Classes*
  - Structs should be used for structures that only contain a few amount of variables and no functions. Classes should be used for more sophisticated structures that contain functions and variables.
  - Prefer making member variables *private* with member functions as an interface for those items.
- *try/catch*
  - The =try{}= statement should wrap the most minimal amount of code (*NOT THE ENTIRE FUNCTION BODY!*) - generally, try should wrap the one function that could throw an exception.
- *NEVER USE GLOBAL VARIABLES.*
- *NEVER USE =goto= STATEMENTS.*



#+ATTR_HTML: :class extra-space
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
* *Cross platform compatibility*

- *Treat everything (including files) as case-sensitive* ::
  When using an =#include= statement for a file in your project, make sure your casing matches the file itself. E.g., if the file is =Product.h=, use =#include "Product.h"=, not =#include "product.h"=.



- *Use =#ifndef= fileguards, not =#pragma once=...* ::
  Use the =#ifndef _LABEL=, =#define _LABEL=, =#endif= style of file guards as they are supported by more compilers.
  - Fileguards ONLY go in .h files.



- =system("cls");= only works on Windows ::
  Prefer creating a cross-platform function like this instead:
  #+BEGIN_SRC cpp :class cpp
    void ClearScreen()
    {
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
      system( "cls" );
    #else
      system( "clear" );
    #endif
    }
  #+END_SRC



- =system("pause");= only works on Windows. ::
  Prefer creating a cross-platform function like this instead:
  #+BEGIN_SRC cpp :class cpp
    // It's klugey but it's the simplest.
    void PressEnterToContinue()
    {
      cout << endl << "PRESS ENTER TO CONTINUE" << endl;
      string temp;
      getline( cin, temp );
      getline( cin, temp );
    }
  #+END_SRC

- File guards ::
  Always use =ifndef= instead of =pragma once= for your file guards.

Yes:
#+BEGIN_SRC cpp :class cpp
  #ifndef _MYFILE_H
  #define _MYFILE_H

  // My code

  #endif
#+END_SRC

No:
#+BEGIN_SRC cpp :class cpp
  #pragma once

  // My code
#+END_SRC

--------------------------------------------------------------------------------
* *File organization*

- Function/class declarations ::
  Function and class declarations should go in a header (.h) file.



- Function definitions ::
  Function definitions should go in a source (.cpp) file, EXCEPT TEMPLATED FUNCTIONS (those all go in .h).



- *Each class/struct should have its own set of .h and .cpp (as needed) files* ::
  - Don't put multiple class declarations in one file.



- *Function definitions must go in .cpp file* ::
  Don't define class functions within the class or in the .h file.
  - *Except for templated classes* - Everything goes in the .h file for templated classes.



- *Don't define structs/classes inside other classes* ::
  Each struct/class should have its own .h file/set of .h and .cpp files.]



- *Never include .cpp files!* ::
Includes should only include .h or .hpp header files.



- *If a project has many files, separate them into folders* ::
  Logically separate related files into subfolders, such as /Exceptions/, /DataStructures/, /Menus/, etc.



#+ATTR_HTML: :class extra-space
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
* *Clean code*
- *Erase unused codeblocks* ::
  If you want to make sure to be able to reference old code later, back it up to GitLab, then delete the comment. Don't leave commented out code blocks in turn-in assignments.



- *Be consistent with naming conventions* ::
  Choose either UpperCaseFunctionNames or lowerCaseFunctionNames but don't interchange them. Same with variable and class naming. Be consistent.



- Whitespace ::
  Adequate whitespace should be used to differentiate logical sections of code.
  At the same time, don't use /too much whitespace/. The goal is to have readable code.





- If ( true ) return true; else return false; ::
  If you're writing a condition to check if something is =true= in order to return =true=, just return the result of the condition instead.

#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML

No:

#+BEGIN_SRC cpp :class cpp
  if ( a == b )
    return true;
  else
    return false;
#+END_SRC

#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

Yes:

#+BEGIN_SRC cpp :class cpp
  return ( a == b );
#+END_SRC

#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML



#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *Curly braces*

- Follow the existing code base ::
  - If you're writing code from scratch you can follow whatever style you'd like as long as it is consistent.
  - When working with existing code-bases, use the same style as the existing code.
  - See https://en.wikipedia.org/wiki/Indentation_style for indentation styles

Preferred style:

#+BEGIN_SRC cpp :class cpp
  if ( a == b )
  {
    DoThing();
  }
#+END_SRC

GNU styling:

#+BEGIN_SRC cpp :class cpp
  if ( a == b )
    {
      DoThing();
    }
#+END_SRC

K&R Styling ("JavaScript" style):

#+BEGIN_SRC cpp :class cpp
  if ( a == b ) {
    DoThing();
  }
#+END_SRC

- One-line contents ::
  Always surround your if statement and while loop contents within curly braces { } even though it's not required
  for one-line internals.


#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML

No:

#+BEGIN_SRC cpp :class cpp
  if ( CONDITION )
    DoThingA();
  else if ( CONDITION2 )
    DoThingB();
  else
    DoThingC();
#+END_SRC

#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

Yes:

#+BEGIN_SRC cpp :class cpp
  if ( CONDITION )
  {
    DoThingA();
  }
  else if ( CONDITION2 )
  {
    DoThingB();
  }
  else
  {
    DoThingC();
  }
#+END_SRC

Or:

#+BEGIN_SRC cpp :class cpp
  if      ( CONDITION )  { DoThingA(); }
  else if ( CONDITION2 ) { DoThingB(); }
  else                   { DoThingC(); }
#+END_SRC

#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML

