# -*- mode: org -*-

*Quick reference - C++ basics*

| Code                                   | Description                                         |
|----------------------------------------+-----------------------------------------------------|
| =#include <iostream>=                  | Use =cout= (console out) and =cin= (console in).    |
| =#include <string>=                    | Create variables with =string= data types.          |
| =#include <iomanip>=                   | Formatting like =setw=, =setprecision=.             |
|----------------------------------------+-----------------------------------------------------|
| =int num;=                             | Basic declaration                                   |
| =int num = 5;=                         | Initialized declaration                             |
| =num = 10;=                            | Assignment statement                                |
| =num = other;=                         | Copying data from "other" variable to num           |
| =cout << num;=                         | Display variable value                              |
| =cin >> num;=                          | Get new value from keyboard                         |
| =getline( cin, my_string );=           | Get a whole line of text from keyboard              |
| =string strC = strA + strB;=           | Combine "strA" and "strB" together, store in =strC= |
|----------------------------------------+-----------------------------------------------------|
| =const string CITY = "Overland Park";= | Creating const string                               |
| =const int MAX_STUDENTS = 20;=         | Creating const int                                  |
