# -*- mode: org -*-

Make sure to also view the "*C++ Quick Reference*" part of this textbook
for a reference guide on basic C++ syntax.

* Includes

| Include               | Features                                                                       |
|-----------------------+--------------------------------------------------------------------------------|
| =#include <iostream>= | Use of =cout= (console output), =cin= (console input), =getline=               |
| =#include <string>=   | Use of the =string= data type and its functions                                |
| =#include <fstream>=  | Use of =ofstream= (output file stream), =ifstream= (input file stream), and related functions |
| =#include <array>=    | Use of =array= from the Standard Template Library                              |
| =#include <vector>=   | Use of =vector= from the Standard Template Library                             |
| =#include <cstdlib>=  | C standard libraries, usually used for =rand()=.                               |
| =#include <ctime>=    | C time libraries, usually used for =srand( time( NULL ) );= to seed random # generator |
| =#include <cmath>=    | C math libraries, such as =sqrt=, trig functions, etc.                         |
| =#include "file.h=    | Use "" to include .h files within your own project. DON'T USE INCLUDE ON .cpp FILES!! |

- About the =using= command ::
  The =using namespace std;= command states that we're using the =std= namespace.
  If this is left off, then we need to prefix any C++ types and functions with =std::=,
  such as =std::cout << "Hello!" << std::endl;=

  *Best practices:*
  - If your program ONLY uses C++ standard library includes, use =using namespace std;= for brevity.
  - If your program uses MULTIPLE LIBRARIES, avoid =using namespace std;= and prefix each type/function with the library it belongs to (e.g., =std::string= from the STD library, =sf::vector2f= from the SFML library)

-----------------------------------
* Bare minimum C++ programs

*Without arguments:*

#+BEGIN_SRC cpp :class cpp
  #include <iostream>
  using namespace std;

  int main()
  {
    cout << "Hello, world!" << endl;
    return 0;
  }
#+END_SRC

*With arguments:*

#+BEGIN_SRC cpp :class cpp
  #include <string>
  #include <iostream>
  using namespace std;

  int main( int argCount, char* args[] )
  {
    if ( argCount < 4 ) { cout << "Not enough arguments!" << endl; return 1; }

    int my_int = stoi( args[1] );
    float my_float = stof( args[2] );
    string my_string = string( args[3] );

    return 0;
  }
#+END_SRC

-----------------------------------
* Building and running from the command line

*Building a single source file:*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
g++ SOURCEFILE.cpp -o PROGRAMNAME.out
#+END_SRC

*Building multiple source files:*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
g++ *.cpp *.h -o PROGRAMNAME.out
#+END_SRC

(There's a different command to use the Visual Studio compiler to build from command line.)

*Running a program without arguments:*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
./PROGRAMNAME.out
#+END_SRC

*Running a program with arguments:*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
./PROGRAMNAME.out arg1 arg2 arg3
#+END_SRC

-----------------------------------
* Variable declaration

1. Declaring a variable: =DATATYPE VARIABLENAME;=
2. Declaring a variable and assigning a value: =DATATYPE VARIABLENAME = VALUE;=
3. Declaring and assigning a named constant: =const DATATYPE NAME = VALUE;=
4. Assigning a new value to an existing variable: =VARIABLENAME = VALUE;=
5. Copying a value from one variable to another: =UPDATEDVAR = COPYME;=

| Data type | Value examples         |
|-----------+------------------------|
| int       | =-5=, =0=, =100=       |
| float     | =0.99=, =-3.25=, =1.0= |
| string    | ="Hello world!"=       |
| char      | ='a'=, ='$'=           |
| bool      | =true=, =false=        |

*Increment/Decrement statements:*
- =a++=, =a--=
- =++a=, =--a=
- =a+=5;=, =a-=5;=
- =a = a + 5;=, =a = a - 5;=

*Notes:*

- =LHS = RHS;= copies FROM =RHS= TO =LHS=; make sure you have the right order!
- A hard-coded value, like ="Hello"=, is known as a *literal*.

-----------------------------------
* Console input and output

1. Output a variable's value: =cout << VARIABLENAME;=
2. Output a string literal: =cout << "Hello";=
3. Chain together multiple items: =cout << "Label: " << VARIABLENAME << endl;=
4. Input to a variable: =cin >> VARIABLENAME;=
5. Input a whole line to a string variable: =getline( cin, STRINGVARIABLE );=

*Notes:*
- The =<<= operator is called the *output stream operator* and is used on =cout= statements.
- The =>>= operator is called the *input stream operator* and is used on =cin= statements.
- =endl= is only to be used with =cout= statements, not =cin=!
- =getline= can only be used with *strings*!! Use =cin >>= for other data types.
- You need a =cin.ignore()= ONLY in between =cin >> ...= and =getline( cin, ... )=.
  - If your program is *skipping an input* then you're missing a =cin.ignore();=.
  - If your program is getting input but *not storing the first letter* then you have too many =cin.ignore();= statements / they're in the wrong place.

-----------------------------------
* Boolean expressions

- AND (=&&=)
  - Expression is TRUE if all sub-expressions are TRUE
  - Expression is FALSE if at least one sub-expression is FALSE
- OR (=||=)
  - Expression is TRUE if at least one sub-expression is TRUE
  - Expression is FALSE if all sub-expressions are FALSE
- NOT (=!=)
  - Expression is TRUE if sub-expression is FALSE
  - Expression is FALSE if sub-expression is TRUE

| =a= | =b= | =a AND b= | =a OR b= |
|-----+-----+-----------+----------|
| T   | T   | T         | T        |
| T   | F   | F         | T        |
| F   | T   | F         | T        |
| F   | F   | F         | F        |

| =a= | =NOT a= |
|-----+---------|
| T   | F       |
| F   | T       |

-----------------------------------
* If statements

- if statement:
#+BEGIN_SRC cpp :class cpp
  if ( CONDITION_A )
  {
    // Action A
  }
#+END_SRC
- if/else statement:
#+BEGIN_SRC cpp :class cpp
  if ( CONDITION_A )
  {
    // Action A
  }
  else
  {
    // Action Z
  }
#+END_SRC
- if/else if statement:
#+BEGIN_SRC cpp :class cpp
  if ( CONDITION_A )
  {
    // Action A
  }
  else if ( CONDITION_B )
  {
    // Action B
  }
  else if ( CONDITION_C )
  {
    // Action C
  }
#+END_SRC
- if/else if/else statement:
#+BEGIN_SRC cpp :class cpp
  if ( CONDITION_A )
  {
    // Action A
  }
  else if ( CONDITION_B )
  {
    // Action B
  }
  else if ( CONDITION_C )
  {
    // Action C
  }
  else
  {
    // Action Z
  }
#+END_SRC

*Condition types* can be a boolean variable:
#+BEGIN_SRC cpp :class cpp
  bool done = true;
  if ( done )
  {
    cout << "Goodbye!" << endl;
  }
#+END_SRC

Or a boolean expression:
#+BEGIN_SRC cpp :class cpp
  int a = 10, b = 5;
  if ( a > b )
  {
    cout << "a is bigger!" << endl;
  }
#+END_SRC

*Notes:*
- =else= statements NEVER TAKE A CONDITION
- Whichever *condition* evaluates to =true=, all future else if and else statements are skipped.

-----------------------------------
* Switch statements

#+BEGIN_SRC cpp :class cpp
  switch( myNumber )
  {
    case 1:
      cout << "It's one!" << endl;
      break;

    case 2:
      cout << "It's two!" << endl;
      break;

    default:
      cout << "I don't know what it is!" << endl;
  }
#+END_SRC

*Notes:*
- You can leave off =break;= from a case statement. In this case, *fallthrough* occurs, where the following case's code will be executed, up until it hits a =break;= statement.
- Switch statements like the one above can be replaced with =if (myNumber == 1)=, =else if (myNumber == 2)=.
- In C++, =switch= doesn't work with =string=, only primitive data types like =int=, =float=, =char=.

*Fallthrough example:*

#+BEGIN_SRC cpp :class cpp
  char choice;
  cout << "Enter a choice: (y/n): ";
  cin >> choice;

  switch( choice )
  {
    case 'y':
    case 'Y':
      cout << "YES" << endl;
      break;

    case 'n':
    case 'N':
      cout << "NO" << endl;
      break;

    default:
      cout << "INVALID INPUT!" << endl;
  }
#+END_SRC


-----------------------------------
* While loops

=while ( CONDITION ) { }=

#+BEGIN_SRC cpp :class cpp
  while ( a < b )
  {
    cout << a << endl;
    a++;
  }
#+END_SRC

*Notes:*
- While loops use CONDITIONS like if statements do.
- While loops are susceptible to *infinite loop* errors if nothing within
  the loop causes the CONDITION to evaluate to false eventually. Be careful!

-----------------------------------
* For loops

*Normal for loop:*

=for ( INIT; CONDITION; UPDATE ) { }=

#+BEGIN_SRC cpp :class cpp
  for ( int i = 0; i < 10; i++ )
  {
    cout << i << endl;
  }
#+END_SRC

*Notes:*
- When using STL arrays or vectors, =size_t= or =unsigned int= should be used instead of =int= for =i=.
  This will remove the warning relating to testing =.size()= (which returns =size_t=) against a signed integer.

*Range-based for loop:*

In versions of C++ past C++98 (from 1998) you can use range-based for loops to iterate over a range of items:

=for ( INIT : RANGE ) { }=

#+BEGIN_SRC cpp :class cpp
  vector<int> myVec = { 1, 2, 3, 4 };
  for ( int element : myVec )
  {
    cout << element << endl;
  }
#+END_SRC

-----------------------------------
* Arrays and vectors

1. Declare a C-style array: =DATATYPE ARRAYNAME[ SIZE ];=
2. Declare a C++ STL array: =array<DATATYPE, SIZE> ARRAYNAME;=
3. Declare a C++ STL vector: =vector<DATATYPE> VECTORNAME;=
4. Declare a dynamic array via pointer: =DATATYPE * PTR = NEW DATATYPE[ SIZE ];=
5. Destroy a dynamic array: =delete [] PTR;=
6. Initialize an array with an initializer list: =DATATYPE ARRAY[] = { VAL1, VAL2, VAL3 }= (also works for =array= and =vector=).

*Notes:*
- The *position* of an item within an array is called its *index*.
- The variable within the array at some position is called its *element*.
- A =vector= is a dynamic array, but you don't have to worry about the memory management. :)
  This means that it can be resized.
- A =array= is a fixed size, just like the C-style array.

*Iterate over an array/vector*

#+BEGIN_SRC cpp :class cpp
  // C-style array:
  for ( int i = 0; i < TOTAL_STUDENTS; i++ )
  {
    cout << "index: " << i << ", value: " << students[i] << endl;
  }

  // STL Array and STL Vector:
  for ( size_t i = 0; i < bankBalances.size(); i++ )
  {
    cout << "index: " << i << ", value: " << students[i] << endl;
  }
#+END_SRC

=size_t= is another name for an =unsigned int=, which allows values of 0 and above - no negatives.

-----------------------------------
* File I/O

*Create an output file and write*

#+BEGIN_SRC cpp :class cpp
  ofstream output;
  output.open( "file.txt" );

  // Write to text file
  output << "Hello, world!" << endl;
#+END_SRC

*Create an input file and read*

#+BEGIN_SRC cpp :class cpp
  ifstream input;
  input.open( "file.txt" );
  if ( input.fail() )
  {
    cout << "ERROR: could not load file.txt!" << endl;
  }
  string buffer;

  // read a word
  input >> buffer;

  // read a line
  getline( input, buffer );
#+END_SRC

*Notes:*
- An =ifstream= open will fail if the file requested does not exist or cannot be found.
  Use =if ( input.fail() )= to check for a failure scenario.
- An =ofstream= open will create a file if it doesn't already exist.
- The default path where files are written to or read from is *wherever your project file is* (.vcxproj for Visual Studio, .cbp for Code::Blocks).
  You can also set a *default working directory* in your project settings.
  - Mac users may need to specify an absolute path, like "=~/myfile.txt"=, which
    will put the file in your home directory. I don't know how to use XCode but we can work through it
    together if you need help.

---------------------------------------------
* *Review questions:*

Answer these questions in your notes, then check your work by completing the
related Concept Intro assignment on Canvas.

#+ATTR_HTML: :class action-item
#+NAME: content
#+BEGIN_HTML
1. The starting point of all C++ programs is...
2. Given two variables, =vip= and =player3=, how do you copy the name from the =player3= variable into the =vip= variable?
3. =<<= is known as ..., =>>= is known as...
4. getline( cin, ... ) can only be used with...
5. An if or else if statement's contents are executed when...
6. The three parts of a for loop's header is...
7. ... can be resized, but ... cannot.
#+END_HTML

