# -*- mode: org -*-

The assignments the instructor writes for this course are meant to help the student learn new topics,
starting easy and increasing the challenge over time. If a student does not do their own work then they
miss out on the lessons and strategy learned from going from step A to step B to step C. The instructor
is always willing to help you work through assignments, so ideally the student shouldn't feel the need
to turn to third party sources for help.

Generally, for R.W. Singh's courses:

- OK things:
  - Asking the instructor for help, hints, or clarification, on any assignment.
  - Posting to the discussion board with questions (except with tests - please email me for those). (If you're unsure if you can post a question to the discussion board, you can go ahead and post it. If there's a problem I'll remove/edit the message and just let you know.)
  - Searching online for general knowledge questions (e.g. "C++ if statements", error messages).
  - Working with a tutor through the assignments, as long as they're not doing the work for you.
  - Use your IDE (replit, visual studio, code::blocks) to test out things before answering questions.
  - Brainstorming with classmates, sharing general information ("This is how I do input validation").

- Not OK Things:
  - Sharing your code files with other students, or asking other students for their code files.
  - Asking a tutor, peer, family member, friend, AI, etc. to do your assignments for you.
  - Searching for specific solutions to assignments online/elseware.
  - Basically, any work/research you aren't doing on your own, that means you're not learning the topics.
  - Don't give your code files to other students, even if it is "to verify my work!"
  - Don't copy solutions off other parts of the internet; assignments get modified a little bit each semester.



If you have any further questions, please contact the instructor.

Each instructor is different, so make sure you don't assume that what is OK with one instructor is OK with another.
