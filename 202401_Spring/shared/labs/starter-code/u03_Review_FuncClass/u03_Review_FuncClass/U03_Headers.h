#ifndef _U03_HEADERS
#define _U03_HEADERS

namespace U03
{

void U03_Program1_Program();
void U03_Program1_Tester();

void U03_Program2_Program();
void U03_Program2_Tester();

void U03_Program3_Program();
void U03_Program3_Tester();

}

void U03_Tester();

void U03_Program();

#endif
