#include <iostream>   // Library that contains `cout` commands
#include <iomanip>    // Library for formatting; `setprecision`
#include <string>     // Library that contains `string` types
#include <vector>     // Library that contains `vector`

#include "../../Utilities/Helper.hpp"

namespace U03
{

class Employee
{
  private:
  int employee_id;
  std::string name;
  float hourly_wage;

  public:
  Employee( int new_id, std::string new_name, float new_wage )
  {
    // TODO: Initialize the private member variables
  }

  int GetId() const
  {
     return -1; // TODO: REMOVE THIS PLACEHOLDER
    // TODO: Return the corresponding private member variable
  }

  std::string GetName() const
  {
     return ""; // TODO: REMOVE THIS PLACEHOLDER
    // TODO: Return the corresponding private member variable
  }

  float GetWage() const
  {
     return -1; // TODO: REMOVE THIS PLACEHOLDER
    // TODO: Return the corresponding private member variable
  }
};

int GetEmployeeIndex( std::vector<Employee> employee_list, int lookup_id )
{
  // TODO: Iterate through all the employees in `employee_list`.
  // Check each one's ID, if it matches `lookup_id` then return the index it is at in the vector.
  // If the for loop ends and nothing has been returned, then return -1 for "not found".

  return -1;
}

//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! DON'T MODIFY ANYTHING BELOW THIS POINT! !!
//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// - PROGRAM STARTER --------------------------------------------------------//

void DisplayTable( std::vector<Employee> employees )
{
  std::cout << std::left
    << std::setw( 10 ) << "INDEX"
    << std::setw( 10 ) << "ID"
    << std::setw( 30 ) << "NAME"
    << std::setw( 10 ) << "WAGE"
    << std::endl << std::string( 80, '-' ) << std::endl;
  for ( size_t i = 0; i < employees.size(); i++ )
  {
  std::cout
    << std::setw( 10 ) << i
    << std::setw( 10 ) << employees[i].GetId()
    << std::setw( 30 ) << employees[i].GetName()
    << std::setw( 10 ) << employees[i].GetWage()
    << std::endl;
  }
  std::cout << std::endl;
}

void U03_Program3_Program()
{
  std::cout << "EMPLOYEE LOOKUP" << std::endl;

  std::vector<Employee> employees = {
    Employee( 123, "Ochoa",   16.65 ),
    Employee( 234, "Bakalar", 16.65 ),
    Employee( 353, "Grubb",   16.65 ),
  };

  DisplayTable( employees );

  std::cout << "Enter Employee ID to find index of: ";
  int eeid;
  std::cin >> eeid;

  int result = GetEmployeeIndex( employees, eeid );
  std::cout << "RESULT: Index = " << result << std::endl;
}

// - AUTOMATED TESTER -------------------------------------------------------//
#include "../../INFO.h"
std::string EmployeeToString( Employee employee )
{
  return Utilities::Helper::ToString( employee.GetId() ) + "/" + Utilities::Helper::ToString( employee.GetWage() ) + "/" + employee.GetName() + "]";
}

std::string VectorToString( std::vector<Employee> employees )
{
  bool first = true;
  std::string blah = "[";
  for ( auto& ee : employees )
  {
    if ( !first ) { blah += ","; }
    blah += EmployeeToString( ee );
    first = false;
  }
  blah += "]";
  return blah;
}

void U03_Program3_Tester()
{
  const std::string GRN = "\033[0;32m"; const std::string RED = "\033[0;31m"; const std::string BOLD = "\033[0;35m"; const std::string CLR = "\033[0m";
  std::cout << std::endl << std::string( 80, '-' ) << std::endl;
  std::cout << "2024-01-U03-P3-TEST; STUDENT: " << STUDENT_NAME << std::endl;

  // (Automated test):

  const int TOTAL_TESTS = 3;
  std::vector<Employee>   in1[TOTAL_TESTS]; // inputs 1
  int                     in2[TOTAL_TESTS]; // inputs 2
  int                     exo[TOTAL_TESTS]; // expected output
  int                     aco[TOTAL_TESTS]; // actual output

  // Setup test 1
  in1[0] = { Employee( 11, "AAA", 100 ), Employee( 22, "BBB", 200 ), Employee( 33, "CCC", 300 ), Employee( 44, "DDD", 400 )  };
  in2[0] = 44;
  exo[0] = 3;

  // Setup test 2
  in1[1] = { Employee( 100, "aardvark", 11 ), Employee( 200, "bear", 12 )  };
  in2[1] = 200;
  exo[1] = 1;

  // Setup test 2
  in1[2] = { Employee( 100, "aardvark", 11 ), Employee( 200, "bear", 12 )  };
  in2[2] = 444;
  exo[2] = -1;

  // Run tests
  for ( int i = 0; i < TOTAL_TESTS; i++ )
  {
    aco[i] = GetEmployeeIndex( in1[i], in2[i] );

    if ( aco[i] == exo[i] )
    {
      // PASS
      std::cout << GRN << "[PASS] ";
      std::cout << " TEST " << i+1 << ", StudentCode(" << VectorToString( in1[i] ) << ", " << in2[i] << ") = " << aco[i] << std::endl;
    }
    else
    {
      // FAIL
      std::cout << RED << "[FAIL] ";
      std::cout << " TEST " << i+1 << ", StudentCode(" << VectorToString( in1[i] ) << ", " << in2[i] << ")" << std::endl;
      std::cout << "   EXPECTED OUTPUT: [" << exo[i] << "]" << std::endl;
      std::cout << "   ACTUAL OUTPUT:   [" << aco[i] << "]" << std::endl;
    }
  }
  std::cout << CLR;
}

}

