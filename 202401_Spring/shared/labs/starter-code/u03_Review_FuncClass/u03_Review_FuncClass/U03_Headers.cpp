#include "U03_Headers.h"

#include <string>
#include <iostream>

void U03_Tester()
{
  U03::U03_Program1_Tester();
  U03::U03_Program2_Tester();
  U03::U03_Program3_Tester();
}

void U03_Program()
{
  std::string title = "UNIT 03: Functions/Classes Review ";
  std::cout << title << std::string( 80-title.size(), '-' ) << std::endl << std::endl;
  std::cout << "Program 1: Sales" << std::endl;
  std::cout << "Program 2: Room builder" << std::endl;
  std::cout << "Program 3: Employees" << std::endl;
  int choice;
  std::cout << std::endl << "Run program #: ";
  std::cin >> choice;

  switch( choice )
  {
    case 1: U03::U03_Program1_Program(); break;
    case 2: U03::U03_Program2_Program(); break;
    case 3: U03::U03_Program3_Program(); break;
  }
}
