#include "GroceryStoreProgram.h"
#include <iostream>
#include <iomanip>
using namespace std;

/**
@param  product          The name of the product to be stocked on the shelves
@param  price            The price of the product to be stocked on the shelves
Related member variable: map<string, float> product_prices

Use the `product` as the key and the `price` as the value, adding the data to the
`product_prices` map.
*/
void GroceryStoreProgram::Stock( string product, float price )
{
  // TODO: Implement me
}

/**
@param  customer          The name of the customer who has entered the store
Related member variable:  map<string, Customer> customers_in_store

Use the `customer` as the key for a new item in the `customers_in_store` map.

struct Customer
{
    string name;
    stack<string> products_in_cart;
};

Since this is a `Customer` object, make sure to set its `name` member variable.
*/
void GroceryStoreProgram::CustomerEnterStore( string customer )
{
  // TODO: Implement me
}

/**
@param  customer          The name of the customer who is adding an item to their cart
@param  product           The name of the product that the customer is adding to their cart
Related member variable:  GroceryStoreProgram - map<string, Customer> customers_in_store
                          Customer - stack<string> products_in_cart;

Access the customer at the `customer` key. The Customer object contains
a `products_in_cart` stack, push the new `product` onto that stack.
*/
void GroceryStoreProgram::CustomerCartAdd( string customer, string product )
{
  // TODO: Implement me
}

/**
@param  customer          The name of the customer who is putting an item back/removing from cart
Related member variable:  GroceryStoreProgram - map<string, Customer> customers_in_store
                          Customer - stack<string> products_in_cart

Access the customer at the `customer` key. The Customer object contains
a `products_in_cart` stack, use the pop function to remove the last-added item from their cart.
*/
void GroceryStoreProgram::CustomerOops( string customer )
{
  // TODO: Implement me
}

/**
@param  customer          The name of the customer who is lining up in the checkout queue
Related member variable:  queue<Customer> checkout_queue
                          map<string, Customer> customers_in_store

Access the customer at the `customer` key in the `customers_in_store` map.
Push this customer into the `checkout_queue`.
*/
void GroceryStoreProgram::CustomerLineup( string customer )
{
  // TODO: Implement me
}

/**
Related member variables:   GroceryStoreProgram - queue<Customer> checkout_queue
                            GroceryStoreProgram - map<string, float> product_prices
                            Customer - stack<string> products_in_cart

This function will process everybody who is currently in the `checkout_queue`.

1. While the checkout queue is not empty:
  a. Display the name of the customer at the front of the checkout queue.
  b. Create a float variable to store the total cost of the transaction, initialize to 0.
  c. For the front customer, while their `products_in_cart` stack is not empty:
    I. Get the price of the next item - use the `product_prices` and the key of the product name.
    II. Add the product price to your total variable.
    III. Display the front item in the customer's cart and the price.
    IV. Pop the top item from the `products_in_cart`.
  d. After the while loop, display that checkout is done and the total amount of money.
  e. Pop the customer from the checkout queue.

Hints:
* You can get the next customer in line with: `checkout_queue.front()`.
* You can access Customer member variables further: `checkout_queue.front().name`
* You can access the next customer's next item in cart with: `checkout_queue.front().products_in_cart.top()`
* You can get the price of an item with `product_prices[ KEY ]`.
    The key will be the product name, which corresponds to `checkout_queue.front().products_in_cart.top()`.
*/
void GroceryStoreProgram::Process()
{
  // TODO: Implement me
}



//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! DON'T MODIFY ANYTHING BELOW THIS POINT! !!
//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

GroceryStoreProgram::GroceryStoreProgram()
{
    cout << left << fixed << setprecision( 2 );
    minutes = 0;
    hours = 8;
}

void GroceryStoreProgram::PrintTimestamp()
{
  cout << "(" << hours << ":";
  if ( minutes < 10 ) { cout << "0"; }
  cout << minutes << ") ";

  minutes += 5;
  if ( minutes == 60 )
  {
    minutes = 0;
    hours++;
  }
}
