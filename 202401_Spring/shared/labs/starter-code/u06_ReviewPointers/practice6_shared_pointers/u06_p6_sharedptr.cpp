#include <memory>
#include <iostream>
#include <string>
using namespace std;

int main()
{
    cout << endl << "SHARED POINTERS" << endl;

    shared_ptr<string> option1 = shared_ptr<string>( new string( "Lemon" ) );
    shared_ptr<string> option2 = shared_ptr<string>( new string( "Lime" ) );

    shared_ptr<string> vote1;
    shared_ptr<string> vote2;
    shared_ptr<string> vote3;
    
    int choice;

    // TODO: Display 1 and option1 (dereferenced), 2 and option2 (dereferenced)
    
    cout << "Person 1 enter your vote: ";
    cin >> choice;
    switch ( choice )
    {
        case 1: break; // TODO: Point vote1 to option1
        case 2: break; // TODO: Point vote1 to option2
    }
    
    cout << "Person 2 enter your vote: ";
    cin >> choice;
    switch ( choice )
    {
        case 1: break; // TODO: Point vote2 to option1
        case 2: break; // TODO: Point vote2 to option2
    }
    
    cout << "Person 3 enter your vote: ";
    cin >> choice;
    switch ( choice )
    {
        case 1: break; // TODO: Point vote3 to option1
        case 2: break; // TODO: Point vote3 to option2
    }
    
    cout << endl << "RESULTS:" << endl;
    // TODO: Display option 1 and its use_count().
    // TODO: Display option 2 and its use_count().


    cout << endl << "THE END" << endl;
    return 0;
}
