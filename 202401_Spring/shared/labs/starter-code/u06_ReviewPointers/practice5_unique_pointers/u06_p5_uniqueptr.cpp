#include <memory>
#include <iostream>
#include <string>
using namespace std;

int main()
{
    cout << endl << "UNIQUE POINTERS" << endl;

    int arraySize = 3;
    int itemCount = 0;
    // TODO: Use a unique_ptr to create a new array
    
    bool done = false;
    while ( !done )
    {
        cout << endl << string( 40, '-' ) << endl;

        // Display array information
        cout << "ARRAY SIZE: " << arraySize << ", ITEM COUNT: " << itemCount << endl;
        cout << "ARRAY CONTENTS:" << endl;
        for ( int i = 0; i < itemCount; i++ )
        {
            // TODO: Display items in array
        }
        cout << endl << endl;

        cout << "Enter a new item to add, or QUIT to quit: ";
        string text;
        getline( cin, text );

        if ( text == "QUIT" )
        {
            done = true;
        }
        else
        {
            // TODO: Add a new item to the array, increment itemCount
        }

        // Check if full, resize if so
        if ( itemCount == arraySize )
        {
            cout << "* NEED TO RESIZE" << endl;
            int newSize = arraySize + 3;
            // TODO: Create bigger array (also a unique_ptr).

            cout << "* COPY DATA FROM OLD ARRAY TO NEW ARRAY" << endl;
            for ( int i = 0; i < arraySize; i++ )
            {
                // TODO: Copy data from old array to biggerArray.
            }

            cout << "* MOVE BIG ARRAY OWNERSHIP TO ORIGINAL ARRAY" << endl;
            // TODO: Use the move command to move ownership of biggerArray to arr.

            cout << "* UPDATE ARRAYSIZE" << endl;
            // TODO: Update the array size.
        }
    }


    cout << endl << "THE END" << endl;
    return 0;
}
