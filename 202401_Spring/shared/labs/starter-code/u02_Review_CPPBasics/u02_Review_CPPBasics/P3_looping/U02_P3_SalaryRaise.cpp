
// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
#include <iomanip>    // Library for formatting; `setprecision`


float U02_Program3_Function( float starting_salary, float raise_per_year, int years )
{
  // TODO: Create float called `updated_salary`. Initialize it to the `starting_salary` amount.
  

  // TODO: Display "Starting salary:" and the `starting_salary`.
  

  // TODO: Use a loop, you need to create a counter variable that goes from 1 to `years`.
  // Within the loop, calculate the updated salary with
  // `updated_salary = updated_salary + ( updated_salary * raise_per_year );`
  // Use `cout` to display the year (counter) and the `updated_salary` each iteration.
  

  // TODO: Return the `updated_salary` as the result.
  
  
  return -1; // TEMP: erase this line of code!!
}



//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! DON'T MODIFY ANYTHING BELOW THIS POINT! !!
//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// - PROGRAM STARTER --------------------------------------------------------//
void U02_Program3_Program()
{
  float starting, percent_raise, decimal_raise;
  int years;
  std::cout << std::fixed << std::setprecision( 2 );
  std::cout << "SALARY RAISE PROGRAM v2" << std::endl;

  std::cout << "What is your starting salary? $";
  std::cin >> starting;

  std::cout << "What is the raise you get per year? %";
  std::cin >> percent_raise;
  decimal_raise = percent_raise / 100;

  std::cout << "How many years to calculate? ";
  std::cin >> years;

  float new_salary = U02_Program3_Function( starting, decimal_raise, years );
  std::cout << "Salary after " << years << " years: $" << new_salary << std::endl;
}

// - AUTOMATED TESTER -------------------------------------------------------//
#include "../../INFO.h"
void U02_Program3_Tester()
{
  const std::string GRN = "\033[0;32m"; const std::string RED = "\033[0;31m"; const std::string BOLD = "\033[0;35m"; const std::string CLR = "\033[0m";
  std::cout << std::endl << std::string( 80, '-' ) << std::endl;
  std::cout << "2024-01-U02-P3-TEST; STUDENT: " << STUDENT_NAME << std::endl;

  // (Automated test):

  const int TOTAL_TESTS = 2;
  float  in1[TOTAL_TESTS]; // inputs 1
  float  in2[TOTAL_TESTS]; // inputs 2
  int    in3[TOTAL_TESTS]; // inputs 3
  float  exo[TOTAL_TESTS]; // expected output
  float  aco[TOTAL_TESTS]; // actual output

  // Setup test 1
  in1[0] = 60000;
  in2[0] = 0.05;
  in3[0] = 5;
  exo[0] = 76576.89;

  // Setup test 2
  in1[1] = 100000;
  in2[1] = 0.1;
  in3[1] = 3;
  exo[1] = 133100;

  // Run tests
  for ( int i = 0; i < TOTAL_TESTS; i++ )
  {
    std::cout << CLR;
    aco[i] = U02_Program3_Function( in1[i], in2[i], in3[i] );

    // Range-based check because of floats, woo.
    if ( aco[i] >= exo[i] - 1 && aco[i] <= exo[i] + 1 )
    {
      // PASS
      std::cout << GRN << "[PASS] ";
      std::cout << " TEST " << i+1 << ", StudentCode(" << in1[i] << ", " << in2[i] << ", " << in3[i] << ") = " << aco[i] << std::endl;
    }
    else
    {
      // FAIL
      std::cout << RED << "[FAIL] ";
      std::cout << " TEST " << i+1 << ", StudentCode(" << in1[i] << ", " << in2[i] << ", " << in3[i] << ")" << std::endl;
      std::cout << "   EXPECTED OUTPUT: [" << exo[i] << "]" << std::endl;
      std::cout << "   ACTUAL OUTPUT:   [" << aco[i] << "]" << std::endl;
    }
  }
  std::cout << CLR;
}
