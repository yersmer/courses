// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
#include <iomanip>    // Library for formatting; `setprecision`


char U02_Program2_Function( float points_possible, float my_score )
{
  // TODO: Create a float for `grade_percent`. Calculate the percent with `my_score` divided by `points_possible` times 100.


  // TODO: Create a char for `letter_grade`. Initialize it to 'F'.


  // TODO: Use if/else if/else statements to determine value for `letter_grade` based on the `grade_percent`;
  // 89.5 and above = A, 79.5 and above = B, 69.5 and above = C, 59.5 and above = D, below that is F.


  // TODO: Display grade information, including `my_score`, `points_possible`, `grade_percent`, and `letter_grade`.


  // TODO: Return the `letter_grade` as the result.
  
  
  return 'x'; // TEMP: erase this line of code!!
}



//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! DON'T MODIFY ANYTHING BELOW THIS POINT! !!
//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// - PROGRAM STARTER --------------------------------------------------------//
void U02_Program2_Program()
{
  std::cout << "LETTER GRADE PROGRAM" << std::endl;
  float points, score;

  std::cout << "How many points was the assignment worth? ";
  std::cin >> points;

  std::cout << "How many points did you score? ";
  std::cin >> score;

  char letter_grade = U02_Program2_Function( score, points );
  std::cout << "Letter grade result: " << letter_grade << std::endl;
}

// - AUTOMATED TESTER -------------------------------------------------------//
#include "../../INFO.h"
void U02_Program2_Tester()
{
  const std::string GRN = "\033[0;32m"; const std::string RED = "\033[0;31m"; const std::string BOLD = "\033[0;35m"; const std::string CLR = "\033[0m";
  std::cout << std::endl << std::string( 80, '-' ) << std::endl;
  std::cout << "2024-01-U02-P2-TEST; STUDENT: " << STUDENT_NAME << std::endl;

  // (Automated test):

  const int TOTAL_TESTS = 5;
  float in1[TOTAL_TESTS]; // inputs 1
  float in2[TOTAL_TESTS]; // inputs 2
  char  exo[TOTAL_TESTS]; // expected output
  char  aco[TOTAL_TESTS]; // actual output

  // Setup test 1
  in1[0] = 89;
  in2[0] = 90;
  exo[0] = 'A';

  // Setup test 2
  in1[1] = 80;
  in2[1] = 64;
  exo[1] = 'B';

  // Setup test 3
  in1[2] = 70;
  in2[2] = 49;
  exo[2] = 'C';

  // Setup test 4
  in1[3] = 60;
  in2[3] = 36;
  exo[3] = 'D';

  // Setup test 5
  in1[4] = 59;
  in2[4] = 12;
  exo[4] = 'F';

  // Run tests
  for ( int i = 0; i < TOTAL_TESTS; i++ )
  {
    aco[i] = U02_Program2_Function( in1[i], in2[i] );

    if ( aco[i] == exo[i] )
    {
      // PASS
      std::cout << GRN << "[PASS] ";
      std::cout << " TEST " << i+1 << ", StudentCode(" << in1[i] << ", " << in2[i] << ") = " << aco[i] << std::endl;
    }
    else
    {
      // FAIL
      std::cout << RED << "[FAIL] ";
      std::cout << " TEST " << i+1 << ", StudentCode(" << in1[i] << ", " << in2[i] << ")" << std::endl;
      std::cout << "   EXPECTED OUTPUT: [" << exo[i] << "]" << std::endl;
      std::cout << "   ACTUAL OUTPUT:   [" << aco[i] << "]" << std::endl;
    }
  }
  std::cout << CLR;
}
