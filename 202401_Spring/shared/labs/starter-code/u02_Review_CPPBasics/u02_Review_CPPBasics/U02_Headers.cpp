#include "U02_Headers.h"

#include <string>
#include <iostream>

void U02_Tester()
{
  U02_Program1_Tester();
  U02_Program2_Tester();
  U02_Program3_Tester();
  U02_Program4_Tester();
  U02_Program5_Tester();
  U02_Program6_Tester();
}

void U02_Program()
{
  std::string title = "UNIT 02: C++ Basics Review ";
  std::cout << title << std::string( 80-title.size(), '-' ) << std::endl << std::endl;
  std::cout << "Program 1: Pizza slices" << std::endl;
  std::cout << "Program 2: Letter grade" << std::endl;
  std::cout << "Program 3: Salary raise v1" << std::endl;
  std::cout << "Program 4: GPA" << std::endl;
  std::cout << "Program 5: Salary raise v2" << std::endl;
  std::cout << "Program 6: Lyrics" << std::endl;
  int choice;
  std::cout << std::endl << "Run program #: ";
  std::cin >> choice;

  switch( choice )
  {
    case 1: U02_Program1_Program(); break;
    case 2: U02_Program2_Program(); break;
    case 3: U02_Program3_Program(); break;
    case 4: U02_Program4_Program(); break;
    case 5: U02_Program5_Program(); break;
    case 6: U02_Program6_Program(); break;
  }
}
