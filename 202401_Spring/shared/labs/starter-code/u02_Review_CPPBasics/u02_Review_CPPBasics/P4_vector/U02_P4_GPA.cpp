// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
#include <vector>     // Library that contains `vector`
#include <iomanip>    // Library for formatting; `setprecision`


float U02_Program4_Function( std::vector<char> course_grades )
{
  float total = 0;

  // TODO: Use a for loop to iterate over all the grades in the `course_grades` vector.
  // For each one, check if that item is an 'A', 'B', 'C', 'D', or 'F'.
  // For 'A', add 4.0 to the `total`.     For 'B', add 3.0 to the `total`.
  // For 'C', add 2.0 to the `total`.     For 'D', add 1.0 to the `total`.
  // Otherwise (for 'F'), add 0.0 or don't add anything to the `total`.

  

  // Returns the average
  return total / course_grades.size();
}



//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   !! DON'T MODIFY ANYTHING BELOW THIS POINT! !!
//   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// - PROGRAM STARTER --------------------------------------------------------//
void U02_Program4_Program()
{
  std::vector<char> grades;
  char input = ' ';
  while ( true )
  {
    std::cout << "Enter another letter grade (A, B, C, D, F) or X to stop: ";
    std::cin >> input;
    if ( toupper( input ) == 'X' )
    {
      break;
    }
    grades.push_back( toupper( input ) );
  }

  float gpa = U02_Program4_Function( grades );
  std::cout << "Your GPA is: " << gpa << std::endl;
}

// - AUTOMATED TESTER -------------------------------------------------------//
#include "../../INFO.h"
void U02_Program4_Tester()
{
  const std::string GRN = "\033[0;32m"; const std::string RED = "\033[0;31m"; const std::string BOLD = "\033[0;35m"; const std::string CLR = "\033[0m";
  std::cout << std::endl << std::string( 80, '-' ) << std::endl;
  std::cout << "2024-01-U02-P4-TEST; STUDENT: " << STUDENT_NAME << std::endl;

  // (Automated test):

  const int TOTAL_TESTS = 2;
  std::string in1[TOTAL_TESTS]; // inputs 1
  float  exo[TOTAL_TESTS]; // expected output
  float  aco[TOTAL_TESTS]; // actual output

  // Setup test 1
  in1[0] = "AABC";
  exo[0] = 3.25;

  // Setup test 2
  in1[1] = "ABCCCDF";
  exo[1] = 2.0;

  // Run tests
  for ( int i = 0; i < TOTAL_TESTS; i++ )
  {
    std::vector<char> grades;
    for ( size_t grade = 0; grade < in1[i].size(); grade++ )
    {
      grades.push_back( in1[i][grade] );
    }

    aco[i] = U02_Program4_Function( grades );

    if ( aco[i] >= exo[i] - 0.1 && aco[i] <= exo[i] + 0.1 )
    {
      // PASS
      std::cout << GRN << "[PASS] ";
      std::cout << " TEST " << i+1 << ", StudentCode(" << in1[i] << ") = " << aco[i] << std::endl;
    }
    else
    {
      // FAIL
      std::cout << RED << "[FAIL] ";
      std::cout << " TEST " << i+1 << ", StudentCode(" << in1[i] << ")" << std::endl;
      std::cout << "   EXPECTED OUTPUT: [" << exo[i] << "]" << std::endl;
      std::cout << "   ACTUAL OUTPUT:   [" << aco[i] << "]" << std::endl;
    }
  }
  std::cout << CLR;
}
