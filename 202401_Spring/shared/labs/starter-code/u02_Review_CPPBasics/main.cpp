#include <iostream>
#include "u02_Review_CPPBasics/U02_Headers.h"

int main()
{
  std::cout << "1. Run AUTOMATED TESTS" << std::endl;
  std::cout << "2. Run PROGRAMS" << std::endl;
  int choice;
  std::cout << ">> ";
  std::cin >> choice;

  switch( choice )
  {
    case 1:
    U02_Tester();
    break;

    case 2:
    U02_Program();
    break;
  }

  return 0;
}
