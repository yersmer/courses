# -*- mode: org -*-

#+ATTR_HTML: :class action-item
#+NAME: content
#+BEGIN_HTML

[[file:../../images/comics/pixel-goals.png]] *Goals:*
- Practice with getting addresses
- Practice with pointers
- Practice with allocating memory for variables via pointer
- Practice with allocating memory for array via pointer
- Practice using unique pointers
- Practice using shared pointers


[[file:../../images/comics/pixel-turnin.png]] *Turn-in:*
- You'll commit your code to your repository, create a *merge request*, and submit the merge request URL in Canvas. (Instructions in document.)


[[file:../../images/comics/pixel-practice.png]] *Practice programs and graded program:*
- This assignment contains several "practice" program folders,
  and a "graded" program folder. Only the "graded" program is required.
- The practice programs are there to help you practice with the topics before
  tackling the larger graded program. If you feel confident in the topic already,
  you may go ahead and work on the graded program. If you feel like you need
  additional practice first, that's what the practice assignments are for.
- The *graded program* assignment contains unit tests to help verify your code.
- You can score up to 100% turning in just the *graded program* assignment.
- You can turn in the *practice* assignments for *extra credit points*.



[[file:../../images/comics/pixel-fight.png]] *Stuck on the assignment? Not sure what to do next?*
- Continue scrolling down in the documentation and see if you're missing anything.
- Try skimming through the entire assignment before getting started,
  to get a high-level overview of what we're going to be doing.
- If you're stuck on a practice program, try working on a different one and come back to it later.
- If you're stuck on the graded program, try the practice programs first.


[[file:../../images/comics/pixel-dual.png]] *Dual enrollment:*
If you are in both my CS 235 and CS 250 this semester, these instructions are the same.
You only need to implement it once and upload it to _one_ repository, then turn in the link for the one merge request.
Please turn in on both Canvas assignments so they're marked done.


#+END_HTML

------------------------------------------------
* *Setup: Starter code and new branch*

For this assignment, I've already added the code to your repository.

1. Pull the starter code:
   1. Check out the =main= branch: =git checkout main=
   2. Pull latest: =git pull=
2. Create a new branch to start working from: =git checkout -b BRANCHNAME=

The =u06_ReviewPointers= folder contains the following items:

#+begin_src artist
.
├── graded_program
│   ├── Array.cpp
│   ├── Array.h
│   ├── INFO.h
│   ├── main.cpp
│   ├── Project_CodeBlocks
│   │   ├── DynamicArray.cbp
│   │   ├── DynamicArray.depend
│   │   └── DynamicArray.layout
│   ├── Project_Makefile
│   │   └── Makefile
│   └── Project_VisualStudio2022
│       ├── DynamicArray
│       │   ├── DynamicArray.vcxproj
│       │   └── DynamicArray.vcxproj.filters
│       └── DynamicArray.sln
├── practice1_addresses
│   └── u06_p1_addresses.cpp
├── practice2_pointers
│   └── u06_p2_pointers.cpp
├── practice3_dynamic_variables
│   └── u06_p3_dynvars.cpp
├── practice4_dynamic_arrays
│   └── u06_p4_dynarr.cpp
├── practice5_unique_pointers
│   └── u06_p5_uniqueptr.cpp
└── practice6_shared_pointers
    └── u06_p6_sharedptr.cpp
#+end_src

** *Practice program projects:*
I have only provided a Visual Studio and Code::Blocks project for the graded assignment.
For the practice assignments you will need to create a project and add the file to that project.
You can follow the steps in this video:

- Visual Studio project creation: https://youtu.be/vMKYByf9-nA
- Code::Blocks project creation: https://youtu.be/bE1ciTVcOZA
- Building from the terminal with g++: https://youtu.be/ogcY5jnv1Lk

------------------------------------------------
* *=practice1_addresses=*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
EXPLORING ADDRESSES

Original Variables!
VARIABLE            ADDRESS             VALUE
--------------------------------------------------------------------------------
num1                0x7ffd286acffc      10
num2                0x7ffd286ad000      15
num3                0x7ffd286ad004      -5

Pointer!
Address: 0
Don't dereference the nullptr!!

ptr is pointing to address: 0x7ffd286acffc
The value at that address is: 10
Enter a new value: 500

ptr is pointing to address: 0x7ffd286ad000
The value at that address is: 15
Enter a new value: 200

ptr is pointing to address: 0x7ffd286ad004
The value at that address is: -5
Enter a new value: 300

Changed Variables!
VARIABLE            ADDRESS             VALUE
--------------------------------------------------------------------------------
num1                0x7ffd286acffc      500
num2                0x7ffd286ad000      200
num3                0x7ffd286ad004      300

THE END
#+END_SRC

Look out for the comment lines in the code that give instructions on what to do.

You will need to create an integer pointer and assign it to various addresses throughout the program.
Once finished, the output should look similar to the above one.

------------------------------------------------
* *=practice2_pointers=*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
POINTERS AND OBJECTS

PRODUCTS:
0    Pencil              1.59
1    Markers             2.39
2    Eraser              0.59
Enter # of product to modify: 0
Enter new price: $9.99
Enter new name: Super Pencil

UPDATED PRODUCTS:
0    Super Pencil        9.99
1    Markers             2.39
2    Eraser              0.59

THE END
#+END_SRC

Look out for the comment lines in the code that give instructions on what to do.

This program will use pointers to objects, so you will need to also access
member variables of the given class. Use the =->= operator to access
members of an object, when it's being pointed to via a pointer.

------------------------------------------------
* *=practice3_dynamic_variables=*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
LINKED NODES

DYNAMICALLY ALLOCATING MEMORY FOR VARIABLES
--------------------------------------------------------------------------------
 CREATE NODES AND LINK THEM:
 * Create firstNode (A)
 * Create firstNode->ptrNext (B)
 * Create firstNode->ptrNext->ptrNext (C)

 ITERATE THROUGH NODES:

 FREE MEMORY:
 * Delete firstNode->ptrNext->ptrNext
 * Delete firstNode->ptrNext
 * Delete firstNode
#+END_SRC

Look out for the comment lines in the code that give instructions on what to do.

Follow the steps of the program to create a basic "linked" structure.

------------------------------------------------
* *=practice4_dynamic_arrays=*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
DYNAMICALLY ALLOCATING MEMORY FOR ARRAYS

----------------------------------------
ARRAY SIZE: 3, ITEM COUNT: 0
ARRAY CONTENTS:

Enter a new item to add, or QUIT to quit: cat

----------------------------------------
ARRAY SIZE: 3, ITEM COUNT: 1
ARRAY CONTENTS:
0. cat

Enter a new item to add, or QUIT to quit: dog

----------------------------------------
ARRAY SIZE: 3, ITEM COUNT: 2
ARRAY CONTENTS:
0. cat
1. dog

Enter a new item to add, or QUIT to quit: rat
 * NEED TO RESIZE
 * COPY DATA FROM OLD ARRAY TO NEW ARRAY
 * FREE THE OLD MEMORY
 * UPDATE POINTER TO NEW MEMORY
 * UPDATE ARRAYSIZE

----------------------------------------
ARRAY SIZE: 6, ITEM COUNT: 3
ARRAY CONTENTS:
0. cat
1. dog
2. rat

Enter a new item to add, or QUIT to quit: panda

----------------------------------------
ARRAY SIZE: 6, ITEM COUNT: 4
ARRAY CONTENTS:
0. cat
1. dog
2. rat
3. panda

Enter a new item to add, or QUIT to quit: QUIT

FREE REMAINING MEMORY BEFORE LEAVING!!

THE END
#+END_SRC

Look out for the comment lines in the code that give instructions on what to do.

Follow along with the instructions to implement the core functionality of a
*dynamic array*, which will also be part of the graded program.

------------------------------------------------
* *=practice5_unique_pointers=*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
UNIQUE POINTERS

----------------------------------------
ARRAY SIZE: 3, ITEM COUNT: 0
ARRAY CONTENTS:

Enter a new item to add, or QUIT to quit: eggs

----------------------------------------
ARRAY SIZE: 3, ITEM COUNT: 1
ARRAY CONTENTS:
0: eggs

Enter a new item to add, or QUIT to quit: bagels

----------------------------------------
ARRAY SIZE: 3, ITEM COUNT: 2
ARRAY CONTENTS:
0: eggs
1: bagels

Enter a new item to add, or QUIT to quit: cereal
 * NEED TO RESIZE
 * COPY DATA FROM OLD ARRAY TO NEW ARRAY
 * MOVE BIG ARRAY OWNERSHIP TO ORIGINAL ARRAY
 * UPDATE ARRAYSIZE

----------------------------------------
ARRAY SIZE: 6, ITEM COUNT: 3
ARRAY CONTENTS:
0: eggs
1: bagels
2: cereal

Enter a new item to add, or QUIT to quit: QUIT

THE END
#+END_SRC

Look out for the comment lines in the code that give instructions on what to do.

Follow along to practice with unique pointers. This program is similar to the
dynamic array program except without the memory management.

------------------------------------------------
* *=practice6_shared_pointers=*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
SHARED POINTERS
1. Lemon	 2. Lime

Person 1 enter your vote: 1
Person 2 enter your vote: 2
Person 3 enter your vote: 2

RESULTS:
OPTION 1: Lemon, USE COUNT: 2
OPTION 2: Lime, USE COUNT: 3

THE END
#+END_SRC

Look out for the comment lines in the code that give instructions on what to do.

The shared pointers =vote1=, =vote2=, and =vote3= will point to either
=option1= or =option2=. At the end of the program, we check their
=use_count()= s to see the total amount of references to each address.

------------------------------------------------
* *=graded_program=*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
DYNAMIC ARRAY
1. Create array
2. Allocate space
   AllocateSpace
3. Add items to array, trigger resize
   AddItem
   AddItem
   AddItem
   AddItem
   ResizeArray
   AddItem
4. Display array
0: BASIC
1: COBOL
2: Java
3: C#
4: C++
5. Deallocate space
   DeallocateSpace
   DeallocateSpace
#+END_SRC

For this program you will be implementing a dynamic array inside the =Array.cpp= file.

The class has already been declared as:

#+BEGIN_SRC cpp :class cpp
class Array
{
public:
    Array();
    Array( int arraySize );
    ~Array();

    void AllocateSpace( int arraySize );
    void DeallocateSpace();

    void ResizeArray();
    void AddItem( string value );
    void Display();

private:
    int m_arraySize = 3;
    int m_itemCount = 0;
    string * m_ptr = nullptr;


    friend void Test_Array_Constructor();
    friend void Test_Array_AllocateSpace();
    friend void Test_Array_DeallocateSpace();
    friend void Test_Array_ResizeArray();
    friend void Test_Array_AddItem();
};
#+END_SRC

You can ignore the =friend= functions - these are used with the unit tests.
Basically, marking a function as a =friend= gives that function access
to the private member variables of a class, which helps me with implementing
the unit tests.

The functions of the Array will need to be implemented.
You will only need to update *Array.cpp*.

#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *Array::Array()*

This is the *default constructor* function.

Initialize the =m_ptr= pointer to =nullptr= for safety.

#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *Array::Array( int arraySize )*

This is the *parameterized constructor* function.

Initialize the =m_ptr= pointer to =nullptr= for safety
and call the =AllocateSpace= function, passing in the =arraySize=.

#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *Array::~Array()*

This is the *destructor* function.

Call the =DeallocateSpace= function.

#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *void Array::AllocateSpace( int arraySize )*

If =m_ptr= is NOT pointing to =nullptr= this means that it is currently in use.
Display an error message and return early (you can leave void functions with =return;=.)

Otherwise, allocate space for a new array via the =m_ptr= pointer -
we are creating a dynamic array. Use the =arraySize= given.

Make sure to also initialize =m_arraySize= to the =arraySize= and
initialize =m_itemCount= to 0.

#+ATTR_HTML: :class hidden-hint
#+NAME: content
#+BEGIN_HTML
QUESTION: How do I create a new dynamic array with =m_ptr=?

#+ATTR_HTML: :class hidden-hint
=m_ptr = new string[ arraySize ];=

#+END_HTML

#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *void Array::DeallocateSpace()*

If =m_ptr= is NOT pointing to =nullptr= then we need to free the memory.
=delete= the array at the =m_ptr= address, then assign =nullptr= to
the =m_ptr= for safety.

#+ATTR_HTML: :class hidden-hint
#+NAME: content
#+BEGIN_HTML
QUESTION: How do I free the memory that =m_ptr= is pointing to?

#+ATTR_HTML: :class hidden-hint
=delete [] m_ptr;=

QUESTION: How do I point =m_ptr= to nullptr?

#+ATTR_HTML: :class hidden-hint
=m_ptr = nullptr;=
#+END_HTML

#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *void Array::ResizeArray()*

1. Create a new dynamic array with a larger array size. Let's call this =newArray=.
2. Copy the data from the /old/ array (=m_ptr=) to the new array (=newArray=).
   You'll use a for loop, and iterate from =i=0= to the =m_itemCount= (not-inclusive).
3. After copying the data over, free the memory at the =m_ptr= location using the =delete= command.
4. Update the =m_ptr= pointer to point to the same address as =newArray=.
5. Update the value of the =m_arraySize= to the new array size.

#+ATTR_HTML: :class hidden-hint
#+NAME: content
#+BEGIN_HTML
QUESTION: How do I create a new dynamic array with a larger size?

#+ATTR_HTML: :class hidden-hint
=string * newArray = new string[ m_arraySize * 2 ];= or some other larger size.

QUESTION: How do I copy the data from one array to another?

#+ATTR_HTML: :class hidden-hint
=for ( int i = 0; i < m_itemCount; i++ ) { newArray[i] = m_ptr[i]; }=

QUESTION: How do I free the memory that =m_ptr= is pointing to?

#+ATTR_HTML: :class hidden-hint
=delete [] m_ptr;=

QUESTION: How do I point =m_ptr= to the same address as =newArray=?

#+ATTR_HTML: :class hidden-hint
=m_ptr = newArray;=
#+END_HTML

#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *void Array::AddItem( string value )*

If the array is full (=m_itemCount == m_arraySize=)
then call the =ResizeArray= function and then continue on.

Assign the element of =m_ptr= at the index =m_itemCount= to the =value= given.

Increment =m_itemCount= afterward.

#+ATTR_HTML: :class hidden-hint
#+NAME: content
#+BEGIN_HTML
QUESTION: How do I assign an item to the =m_ptr= array?

#+ATTR_HTML: :class hidden-hint
=m_ptr[INDEX] = value;=


QUESTION: What is the index going to be?

#+ATTR_HTML: :class hidden-hint
=m_ptr[m_itemCount] = value;=
#+END_HTML


#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *void Array::Display()*

*Note: This function does not have a unit test.*

Use a for loop to iterate over all the elements of the =m_ptr= array.
Display each item's *index* and *element value*.


#+ATTR_HTML: :class hidden-hint
#+NAME: content
#+BEGIN_HTML
QUESTION: What is an *index*?

#+ATTR_HTML: :class hidden-hint
The index is the *position* of an element in the array.
The lowest valid index is 0, and for an array of size /size/,
the highest valid index is /size-1/.

QUESTION: How do I iterate over all the elements of the array?

#+ATTR_HTML: :class hidden-hint
=for ( int i = 0; i < m_itemCount; i++ ) { cout << i << ". " << m_ptr[i] << endl; }=
#+END_HTML



#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML

Once everything is implemented, all the tests should pass.
You can also run the program itself to see your dynamic array
in use.

------------------------------------------------
* *Turning in the assignment*


*Screenshot:* Before finishing up, run the automated tests and take a screenshot
of all of your tests passing. Save the file somewhere as a .png.

*Back up your code:* Open Git Bash in the base directory of your repository folder.

1. Use =git status= to view all the changed items (in red).
2. Use =git add .= to add all current changed items.
3. Use =git commit -m "INFO"= to commit your changes, change "INFO" to a more descriptive message.
4. Use =git push -u origin BRANCHNAME= to push your changes to the server.
5. On GitLab.com, go to your repository webpage. There should be a button to
   *Create a new Merge Request*. You can leave the default settings and create the merge request.
6. Copy the URL of your merge request.

*Turn in on Canvas:*
1. Find the assignment on Canvas and click *"Start assignment"*.
2. Select *Upload file* and upload your screenshot.
3. *Paste your GitLab URL* into the Comments box.
4. Click *"Submit assignment"* to finish.
