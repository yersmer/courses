# -*- mode: org -*-

#+ATTR_HTML: :class action-item
#+NAME: content
#+BEGIN_HTML
- About: :: Source control is an important tool in software development. GitLab is a service that allows us to host git repositories. Our own computer will use the git software to interact with the GitLab server.
- Goals: ::
  - Create a *GitLab* account
  - Set up *git* software on your computer
  - Clone a repository
  - Make changes to a repository
  - Deal with merge conflicts
  - Create branches and merge requests
- Turn-in: ::
  - 1. Tell me what your GitLab username is using the "*❓ Unit 00 Setup - GitLab Username*" assignment on Canvas. Then I can give you access to your repository for the semester.
  - 2. Turn in a *URL to a merge request* in the "*🧑‍🔬 Unit 00 Lab - Set up (U00.LAB.202401CS250)*" assignment on Canvas.
    - Should contain an example program and your project files.
- Links (CS 235) ::
  - ❓ Unit 00 Setup - GitLab Username - https://canvas.jccc.edu/courses/68320/modules/items/3792510
  - 🧑‍🔬 Unit 00 Lab - Set up (U00.LAB.202401CS35) - https://canvas.jccc.edu/courses/68320/modules/items/3742299
- Links (CS 250) ::
  - ❓ Unit 00 Setup - GitLab Username - https://canvas.jccc.edu/courses/68318/modules/items/3792509
  - 🧑‍🔬 Unit 00 Lab - Set up (U00.LAB.202401CS250) - https://canvas.jccc.edu/courses/68318/assignments/1589239
#+END_HTML

------------------------------------------------

* Installing an IDE

Download and install an IDE (Integrated Development Environment) on your system.
Here are some suggested ones, but you can use whatever tools you'd like.

| IDE                     | Link                                             | Platforms                  | Info                                                                               |
|-------------------------+--------------------------------------------------+----------------------------+------------------------------------------------------------------------------------|
| Visual Studio Community | https://visualstudio.microsoft.com/vs/community/ | Windows only (C++ version) | During install, make sure to select *Desktop development with C++*!!               |
| Code::Blocks            | https://www.codeblocks.org/downloads/binaries/   | Windows/Linux              | Lightweight, better for older machines. (WINDOWS: Download the *mingw-setup* version!) |
| XCode                   | https://developer.apple.com/xcode/               | Mac                        |                                                                                    |

We'll use our IDE more once we set up *git* and clone our repository...

------------------------------------------------

* Installing git

Starting off, you will need to install the git program to your computer.
Go to https://git-scm.com/ and select Download for your OS.
Make sure to read through the setup settings here to prevent headaches in the future.
(In particular, not setting Vim as the default text editor... unless that's something you want.)

Most of the default settings are fine, but take note:

- *Select Components:* Make sure that "Git Bash Here" is checked.
- *Choosing the default editor used by Git:* Choose something like Notepad or if you have a preferred text editor, use that.

------------------------------------------------

* Configuring git

After git is installed, we will need to do a one-time configuration. Open up *Git Bash* (in Windows) or use
git from your *terminal* (Linux/Mac).

You will need to configure your name:

#+ATTR_HTML: :class console
#+BEGIN_SRC bash
git config --global user.email "you@example.com"
#+END_SRC

Replace "you@example.com" with your email (such as your student email).

And your email address:

#+ATTR_HTML: :class console
#+BEGIN_SRC bash
git config --global user.name "Your Name"
#+END_SRC

Replace "Your Name" with your name, which is what will show up when you submit code file edits.

Finally, we're setting up a default merge resolution scheme, which you don't really
need to worry about the details right now:

#+ATTR_HTML: :class console
#+BEGIN_SRC bash
git config --global pull.rebase false
#+END_SRC

------------------------------------------------

* Registering a GitLab account

#+INCLUDE: "../../reference/creating_a_gitlab_account.org"

------------------------------------------------

* Make sure the instructor knows your GitLab username

Post your GitLab username in the "*❓ Unit 00 Setup - GitLab Username*" assignment.
Once you have access to your repository, you can continue.

------------------------------------------------

* Cloning the repository

*Your operating system:*  Navigate to the folder where you're storing your class projects. From here,
right-click in the empty space and select *Git Bash Here* to open Git Bash.

*Repository webpage:* On the repository webpage (gitlab.com)
click on the blue *Clone* button and copy the URL listed under the *Clone with HTTPS* header. (You can use SSH if you
know how to set it up, but we aren't going to cover that here.)

[[file:images/c3_u00_clone.png]]

*Git Bash:* Use the =git clone URL= command in Git Bash. This will clone the repository
within the folder you opened Git Bash in. (Windows: To use PASTE, right-click in Git Bash...
CTRL+V will not work here.)

#+ATTR_HTML: :class console
#+BEGIN_SRC bash
$ git clone https://gitlab.com/rsingh13-student-repos/2024-01_cs250/YOURSSO-cs250.git
Cloning into 'YOURSSO-cs250'...
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (3/3), done.
#+END_SRC

/(This will be "cs235" or "cs250" based on the course you're in.)/

Now that the folder is on your computer, you will need to *change directory* into the folder. Use =cd FOLDERNAME=
(in the example above, the folder name would be =YOURSSO-cs235= or =YOURSSO-cs250=) to enter the repository folder in Git Bash.

Changing directory in Git Bash:

[[file:images/c3_u00_clonedrepo_bash.png]]

Viewing the folder in Windows Explorer:

[[file:images/c3_u00_clonedrepo.png]]

------------------------------------------------

* Creating a branch

| Command                      | Description                                                   |
|------------------------------+---------------------------------------------------------------|
| =git branch=                 | View available branches                                       |
| =git checkout -b BRANCHNAME= | Create a new branch with the given BRANCHNAME                 |
| =git checkout BRANCHNAME=    | Checkout an existing branch whose name matches the BRANCHNAME |

When working on an assignment, you will need to create a new *branch* each time.

To view available branches, use the =git branch= command:

#+ATTR_HTML: :class console
#+BEGIN_SRC bash
 $ git branch
 * main
#+END_SRC

To create a new branch, use the =git checkout -b BRANCHNAME= command.
The branch name needs to be unique. I suggest putting the Unit # in the branch name at least.

#+ATTR_HTML: :class console
#+BEGIN_SRC bash
$ git checkout -b u00_setup
Switched to a new branch 'u00_setup'
#+END_SRC

Now if you type in =git branch= again you will see two branches with the asterisk marking which
branch you're currently on:

#+ATTR_HTML: :class console
#+BEGIN_SRC bash
 $ git branch
   main
 * u00_setup
#+END_SRC

Each branch basically has its own *code space*. If you have work-in-progress code happening on separate branches,
they will only be available on that specific branch. If you switch to another branch you'll only see code for
that branch, but you can always keep switching between to get the code back. (When starting a new assignment,
make sure to go back to the =main= branch and use =git pull= to grab latest code before then creating a new branch.
I'll try to put a reminder in future assignments.)

If you want to switch between existing branches, use =git checkout BRANCHNAME=:

#+ATTR_HTML: :class console
#+BEGIN_SRC bash
  $ git checkout main
  Switched to branch 'main'
  Your branch is up to date with 'origin/main'.

  $ git checkout u00_setup
  Switched to branch 'u00_setup'.
#+END_SRC

(Note: The main branch might be named "master" so if your =git branch= shows "master"
instead of "main", use that.)

Stay on your new branch for now.

------------------------------------------------

| Command                         | Description                                                                           |
|---------------------------------+---------------------------------------------------------------------------------------|
| =ls=                            | List out the files and folders in the current directory                               |
| =touch FILENAME=                | Create a new file                                                                     |
| =echo "text" >> FILENAME=       | Append text to a file                                                                 |
| =git status=                    | View files that have changed                                                          |
| =git add FILENAME=              | Add a specific file to the changeset                                                  |
| =git add *.cpp=                 | Add all files that end with ".cpp" to the changeset                                   |
| =git add .=                     | Add all changed files to the changeset                                                |
| =git commit=                    | Create a snapshot of currently added changes                                          |
| =git commit -m "text"=          | Create a snapshot of currently added changes, setting commit message at the same time |
| =git push -u origin BRANCHNAME= | Push the changes in the branch to the server                                          |

Starting off, your repository may only have a README file in the folder:

#+ATTR_HTML: :class console
#+BEGIN_SRC bash
$ ls
README.md
#+END_SRC

In Windows you can create a new text file in here by opening notepad
and saving a new file in this directory.

In Git Bash you can create a file by using the =touch= command:
Use the =touch FILENAME= command to create a new file. Name your file YOURNAME.txt .
You can open the file from your OS and edit the text, or you can
use the following command to put the text "Hello, I'm NAME!" in place:

#+ATTR_HTML: :class console
#+BEGIN_SRC bash
$ echo "Hello, I'm Rachel!" >> u00_hello.txt
#+END_SRC

Next, use the =git status= command to view the changed file:

#+ATTR_HTML: :class console
#+BEGIN_SRC bash
$ git status
On branch u00_setup
Untracked files:
  (use "git add <file>..." to include in what will be committed)
	u00_hello.txt

nothing added to commit but untracked files present (use "git add" to track)
#+END_SRC

We need to use the =git add FILENAME= command to mark the file for the changeset.

#+ATTR_HTML: :class console
#+BEGIN_SRC bash
$ git add u00_hello.txt
#+END_SRC

It won't show any confirmation message if there is no error.

Next, we use the =git commit -m "COMMIT MESSAGE"= command to make a snapshot of the file's changes at this point:

#+ATTR_HTML: :class console
#+BEGIN_SRC bash
$ git commit -m "Created my text file"
[rsingh13_u04ex 9eb8e5c] Created my text file
 1 file changed, 1 insertion(+)
 create mode 100644 u00_hello.txt
#+END_SRC

Finally, use the =git push -u origin BRANCHNAME= command to push your changes to the GitLab server:

#+ATTR_HTML: :class console
#+BEGIN_SRC bash
$ git push -u origin u00_setup
Enumerating objects: 4, done.
Counting objects: 100% (4/4), done.
Delta compression using up to 12 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 303 bytes | 303.00 KiB/s, done.
Total 3 (delta 1), reused 0 (delta 0)
remote:
remote: To create a merge request for u00_setup, visit:
remote:   (A URL)
remote:
To gitlab.com:(REPOSITORY URL).git
 * [new branch]      u00_setup -> u00_setup
Branch 'u00_setup' set up to track remote branch 'u00_setup' from 'origin'.
#+END_SRC

------------------------------------------------

* Pulling remote changes

| Command    | Description                         |
|------------+-------------------------------------|
| =git pull= | Pull latest changes from the server |

*GitLab webpage:* On the webpage your branch will now show up in the dropdown menu.

[[file:images/c3_u00_gitlab_branch.png]]

Select your branch and you'll be able to see the last commit and your file here:

[[file:images/c3_u00_gitlab_files.png]]

Open up your text file here and click the blue *Edit* button and select an editor.
Add another line to the text file, then click the *Commit changes* button at the bottom of the page.

[[file:images/c3_u00_gitlab_commit.png]]

*Git Bash:* Use the =git pull= command here to pull all changes, which will grab the changes
that you just made from the server.

#+ATTR_HTML: :class console
#+BEGIN_SRC bash
$ git pull
remote: Enumerating objects: 5, done.
remote: Counting objects: 100% (5/5), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 1), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (3/3), 517 bytes | 517.00 KiB/s, done.
From gitlab.com:(URL)
   9eb8e5c..90394af  u00_setup -> origin/u00_setup
Updating 9eb8e5c..90394af
Fast-forward
 u00_hello.txt | 2 ++
 1 file changed, 2 insertions(+)
#+END_SRC

When you open the text file in Notepad or a text editor, you will now see the second line of text.

------------------------------------------------

* Dealing with merge conflicts

This time we're going to purposefully create a *merge conflict*. This happens when changes are made to the same file from different locations
(such as on your computer and the web, or between different developers) and git can't figure out how to automatically merge the changes together.
It can be intimidating to deal with, but it isn't too bad.

*Computer:* Edit your text file to add some text in the middle. Then use =git add FILENAME= , =git commit -m "Edited file"=, but don't push yet.

*GitLab:* Edit your text file to add a different line of text in the middle.

[[file:images/c3_u00_gitlab_twochanges.png]]

*Git Bash:* Now that you have two sets of changes, try to push your changes to the server. It will be rejected:

#+ATTR_HTML: :class console
#+BEGIN_SRC bash
$ git push
To gitlab.com:(URL).git
 ! [rejected]        u00_welcome -> u00_welcome (fetch first)
error: failed to push some refs to 'git@gitlab.com:(URL).git'
hint: Updates were rejected because the remote contains work that you do
hint: not have locally. This is usually caused by another repository pushing
hint: to the same ref. You may want to first integrate the remote changes
hint: (e.g., 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
#+END_SRC

It gives you a hint - you need to use =git pull= to grab any latest changes from the server:

#+ATTR_HTML: :class console
#+BEGIN_SRC bash
$ git push
To gitlab.com:(URL).git
 ! [rejected]        u00_welcome -> u00_welcome (non-fast-forward)
error: failed to push some refs to 'git@gitlab.com:(URL).git'
hint: Updates were rejected because the tip of your current branch is behind
hint: its remote counterpart. Integrate the remote changes (e.g.
hint: 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
#+END_SRC

Use =git pull=, which will work, but result in a merge conflict:

#+ATTR_HTML: :class console
#+BEGIN_SRC bash
$ git pull
Auto-merging u00_welcome.txt
CONFLICT (content): Merge conflict in u00_welcome.txt
Automatic merge failed; fix conflicts and then commit the result.
#+END_SRC

Open the text file in your editor and you'll see some additions to the file:

#+ATTR_HTML: :class textfile
#+BEGIN_SRC
Hello, I'm Rachel!
<<<<<<< HEAD
This is my third edit.
=======
Fourth update!!!
>>>>>>> a0c7bd54e2c5c19d1ebd41a84e7a6b8820573cf9
This is a second update!
#+END_SRC

There are two regions, and we need to manually decide how we want to merge the text.
You could just remove the markers
if you wanted both changes. Make the update and save the file:

#+ATTR_HTML: :class textfile
#+BEGIN_SRC
Hello, I'm Rachel!
This is my third edit.
Fourth update!!!
This is a second update!
#+END_SRC

Then do your add, commit, and push to push the merged updates to the server.

Protip: You can combine multiple commands together with =&&=:

=$ git add . && git commit -m "manual merge" && git push=

*GitLab:* On the website, you can select the "Commits" link under the "Code" section to
view all the changes you've made to the code:

[[file:images/c3_u00_gitlab_commitlist.png]]

------------------------------------------------

* Creating an example program to test our IDE

Lastly we're going to create a basic C++ project and make sure everything builds correctly.
The project and code you create here should be stored *within your repository*.

#+ATTR_HTML: :class hint
#+NAME: content
#+BEGIN_HTML
- If you're using VS Code, please check the reference portion of the book
  for the section labeled *VS Code -  Open projects, building, and running*.
- If you're using another code editor and wish to build from the terminal using the Makefile,
  look in the reference portion of the book for the section labeled
  *Terminal - Building and running*
#+END_HTML

*Visual Studio*

#+INCLUDE: "../../reference/visualstudio_testproject.org"

*Code::Blocks*

#+INCLUDE: "../../reference/codeblocks_testproject.org"

*Afterwards, add, commit, and push your changes:*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
git add .
git commit -m "Example project"
git push -u origin u00_setup
#+END_SRC


------------------------------------------------

* Creating a merge request

Once changes are made, you'll create a merge request. This is how you can have your code reviewed,
and the instructor will sign off
on it and merge it to the main branch, where all the code will live together. This is similar to in
software development, where
everybody will work on their own features on their own branches, create a merge request, get their
code reviewed, then a senior
developer will merge the code into the main project.

*GitLab:* On the main repository page you will see a message saying that there are changes in your
branch, with a button
"Create merge request":

[[file:images/c3_u00_gitlab_mergenotif.png]]

You can mostly leave the defaults unless you want to add notes. Scroll down and click the blue *"Create merge request"* button.

There will be a merge request page generated:

[[file:images/c3_u00_gitlab_mergerequest2.png]]

*The URL of this page is what you will usually turn in as your Lab assignments*

#+INCLUDE: "../../reference/canvas_turnin_mergerequest.org"
