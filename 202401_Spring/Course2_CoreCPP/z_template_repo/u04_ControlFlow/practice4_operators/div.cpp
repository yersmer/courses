// PROGRAM: Practice using logic operators

// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
using namespace std;  // Using the C++ STanDard libraries

// - PROGRAM CODE -----------------------------------------------------------//
int main()
{
  // TODO: Create two boolean variables, `has_paper` and `printer_on`.


  // TODO: Display the message "Is the printer on? (0) off, (1) on: ";


  // TODO: Use `cin` to get keyboard input and store it in `printer_on`.


  // TODO: Display the message "Does the printer have paper? (0) no paper, (1) has paper: ";


  // TODO: Use `cin` to get keyboard input and store it in `has_paper`.


  // TODO: If has_paper is true and printer_on is true then display "CAN PRINT!"
  // Otherwise, display "CANNOT PRINT!".


  // Return 0 means quit program with no errors, in this context.
  return 0;
}
