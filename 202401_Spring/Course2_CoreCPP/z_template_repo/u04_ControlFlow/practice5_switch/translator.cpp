// PROGRAM: Practice using switch statements

// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
using namespace std;  // Using the C++ STanDard libraries

// - PROGRAM CODE -----------------------------------------------------------//
int main()
{
  // TODO: Create two string variables, `english` and `translated`. Initialize `english` to "cat".


  // TODO: Create a character variable named `language`. Ask the user to enter a language letter,
  // and store their response in this variable.


  cout << "(e)speranto, (s)panish, (m)andarin, (h)indi"


  // TODO: Use a switch statement to look at the `language` chosen.
  // In case of 'e', set `translated` to "kato".
  // In case of 's', set `translated` to "gato".
  // In case of 'm', set `translated` to "mao".
  // In case of 'h', set `translated` to "billee".
  // For the default case, set `translated` to "?"


  // TODO: Display the `english` word and then `translated` word.`



  // Return 0 means quit program with no errors, in this context.
  return 0;
}
