# -*- mode: org -*-

For each of the problems below, answer the following questions:

- How does the *expected result* differ from the *actual result*?
- Which line(s) of code are causing the issue?
- How can the issue be fixed?
- Write out the fixed code.

* Problem 1

*Original code:*

#+BEGIN_SRC cpp :class cpp
  int number;
  cout << "Enter number: ";
  cin >> number;

  if ( number < 0 )
    {
      cout << "Invalid selection!" << endl;

  cout << "You chose: " << number << endl;
#+END_SRC

*Expected result:*
Program asks user to enter a number. If they select a number less than 0
then it displays "Invalid selection!".
At the end, it shows "You chose:" and then the number they typed in.

*Actual result:*
The program doesn't build.

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
temp.cpp: In function ‘int main()’:
temp.cpp:18:1: error: expected ‘}’ at end of input
   18 | }
      | ^
temp.cpp:5:1: note: to match this ‘{’
    5 | {
      | ^
#+END_SRC

* Problem 2

*Original code:*

#+BEGIN_SRC cpp :class cpp
  int num1, num2;
  cout << "Enter num1: ";
  cin >> num1;

  cout << "Enter num2: ";
  cin >> num2;

  if ( num1 < num2 )
  {
    cout << "num1 is smaller" << endl;
  }
  else if ( num1 > num2 )
  {
    cout << "num1 is larger" << endl;
  }
  else ( num1 == num2 )
  {
    cout << "num1 and num2 are equal" << endl;
  }
#+END_SRC

*Expected result:*
User enters two numbers. Program displays "num1 is smaller", "num1 is larger", or "num1 and num2 are equal" based on their values.

*Actual result:*
The program doesn't build.

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
program.cpp:21:24: error: expected ‘;’ before ‘{’ token
   21 |   else ( num1 == num2 )
      |                        ^
      |                        ;
   22 |   {
      |   ~
#+END_SRC


