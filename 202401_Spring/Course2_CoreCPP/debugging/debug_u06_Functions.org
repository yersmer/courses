# -*- mode: org -*-

For each of the problems below, answer the following questions:

- How does the *expected result* differ from the *actual result*?
- Which line(s) of code are causing the issue?
- How can the issue be fixed?
- Write out the fixed code.

* Problem 1

*Original code:*

#+BEGIN_SRC cpp :class cpp
  int Sum( int a, int b )
  {
    return a + b;
  }

  int main()
  {
    int num1, num2;

    cout << "Enter num1: ";
    cin >> num1;

    cout << "Enter num2: ";
    cin >> num2;

    int sum = Sum( int num1, int num2 );

    cout << "Sum: " << sum << endl;

    return 0;
  }
#+END_SRC

*Expected result:*


*Actual result:*
The program doesn't build.

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
temp.cpp: In function ‘int main()’:
temp.cpp:19:18: error: expected primary-expression before ‘int’
   19 |   int sum = Sum( int num1, int num2 );
      |                  ^~~
temp.cpp:19:28: error: expected primary-expression before ‘int’
   19 |   int sum = Sum( int num1, int num2 );
      |                            ^~~
#+END_SRC

* Problem 2

*Original code:*

#+BEGIN_SRC cpp :class cpp
#+END_SRC

*Expected result:*


*Actual result:*
