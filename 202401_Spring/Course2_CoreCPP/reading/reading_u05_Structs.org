# -*- mode: org -*-

* *Introduction to objects*

[[file:images/reading_u05_Structs_menuitems.png]]

Programming paradigms (pronounced "pair-uh-dimes")
are ways we can classify different programming styles.
Some programming languages support multiple paradigm
styles and some are restricted to one style of coding.
Over the decades different paradigms have been
developed and evolved over time.
*Object Oriented Programming* is one of the most common
styles of programming these days, and is a big part of how
C++, C#, and Java-based programs are designed.

#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *What are "Objects"?*

Defining *classes* and *structs* in our programs
are a way that we can create our own *data types*
for our variables. When we define our own structures,
we can create them with *internal variables and functions*
available to them. The variables we create, whose data types
come from a defined *class or struct*, is known as an *object*.

Design-wise, the idea is to take real-world objects and
find a way to /represent them/ in a computer program
as an *object* that has *attributes* (variables)
and *functionality* (functions).




#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *OOP design ideals*

The concept of *Object Oriented Programming* is
creating a design that is easy to maintain over time.
In particular, there are some core design goals
behind OOP, including:

- *Encapsulation:* Giving the user / other programmers an “interface”
to interact with the objects, hiding the inner-workings within the class.
  - Certain functions are made *public*, which other programmers can use to interface with the object.
  - The other programmers don't need to worry about the inner-workings of the object in order to use it.
  - The developer of the class can modify /how/ the internals work without breaking the public interface.
  - Helps protect the data within the class from being accessed or modified
    by external things that shouldn't have access to it.
- *Loose coupling:*
  Ideally, different objects in a program shouldn't have
  their functionality tied to other objects too closely;
  we wan tto reduce inter-dependence between objects.
  When objects are more /independent/ from each other,
  we say they are /loosely coupled/.
- *High cohesion:*
  When we design our objects, we shouldn't just
  throw everything and the kitchen sink into one object.
  To design an object with /high cohesion/ means that everything
  inside the object /belongs/ to that object - reduce the clutter.




------------------------------------------------------------------------
* *Separate variables to one struct*

*Structs* are a way we can group related information together into one data type.

For example, let's say we were writing a program for a restaurant, and an item on the menu would have a name, a price, a calorie count, and a "is it vegetarian?" signifier. We could declare these all as separate variables:

#+BEGIN_SRC cpp :class cpp
string food1_name;
float food1_price;
int food1_calories;
bool food1_is_veggie;
#+END_SRC

But there isn't really anything in the program that says these variables are related, except that we as humans have given these variables similar prefixes in their names.

A better design would be to create a new datatype called MenuItem (or something similar), and the MenuItem will contain these four variables within it. We're basically making a variable data type that can contain multiple variables!

First we create the struct, which should go in its own .h file:

*MenuItem.h:*
#+BEGIN_SRC cpp :class cpp
struct MenuItem
{
  string name;
  float price;
  int calories;
  bool isVeggie;
};
#+END_SRC

And then we can declare variables of this type in our program:

*main.cpp:*
#+BEGIN_SRC cpp :class cpp
  MenuItem food1;
  food1.name = "Bean Burrito";
  food1.price = 1.99;
  food1.calories = 350;
  food1.isVeggie = true;

  MenuItem food2;
  food2.name = "Crunchy Taco";
  food2.price = 1.99;
  food2.calories = 170;
  food2.isVeggie = false;
#+END_SRC


------------------------------------------------------------------------
* *Multiple files in C++ programs*

[[file:images/reading_u05_Structs_structfiles.png]]

When creating a struct, we should create a new source code file in our project.
Usually the name of the file will be the same name as the struct, with a .h at the end.
This is a header file and declarations for structs (and functions and classes later) will
go in these types of files.

Something you need to require in your .h files that isn't needed for your .cpp
files are file guards. These prevent the compiler from reading the file more than once.
If it reads it multiple times, it will think you're redeclaring things over and over again.

Your .h files should always look like this:

#+BEGIN_SRC cpp :class cpp
#ifndef _MYFILE_H
#define _MYFILE_H

// put code here

#endif
#+END_SRC

Where the "=_MYFILE_H=" should be changed to something unique in each file -
usually, the name of your file.




------------------------------------------------------------------------
* *Creating our own structs*

A basic struct declaration is of the form...

#+BEGIN_SRC cpp :class cpp
  #ifndef _MYFILE_H
  #define _MYFILE_H

  struct STRUCTNAME
  {
    // member variables
    int MEMBER1;
    string MEMBER2;
    float MEMBER3;
  };

  #endif
#+END_SRC

Note that the closing curly-brace =}= must end with a =;= otherwise the compiler will give you errors.

You can name your struct anything you'd please but it has to abide by the C++ naming rules
(no spaces, no keywords, can contain letters, numbers, and underscores).

Any variables we declare within the struct are known as *member variables* - they are members of that struct.

Any new variables you declare whose data type is this struct will have its own copy of each of these variables.



#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *Declaring object variables*

A object is a type of variable whose data type is a struct (or class). To declare a variable whose
data type is a struct, it looks like any normal variable declaration:

#+BEGIN_SRC cpp :class cpp
DATATYPE VARIABLE;
#+END_SRC


#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *Accessing member variables*

Our *struct object variables* each have their own *member variables*, which we access via the dot operator =.=.
We can assign values to it, our read its value, similar to any normal variable.

#+BEGIN_SRC cpp :class cpp
  VARIABLE.MEMBER1 = 10;
  VARIABLE.MEMBER2 = "ASDF";
  VARIABLE.MEMBER3 = 2.99;

  cout << VARIABLE.MEMBER1 << endl;
#+END_SRC

#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** *Example: Fraction struct*

First we would declare our Fraction struct within *Fraction.h:*
#+BEGIN_SRC cpp :class cpp
  #ifndef _FRACTION_H
  #define _FRACTION_H

  struct Fraction
  {
    int num;
    int denom;
  };

  #endif
#+END_SRC

Within =main()=, we could then create Fraction variables and work with them:

*main.cpp:*
#+BEGIN_SRC cpp :class cpp
  #include "Fraction.h"

  int main()
  {
    Fraction frac1, frac2, frac3;

    cout << "FIRST FRACTION" << endl;
    cout << "* Enter numerator: ";
    cin >> frac1.num;
    cout << "* Enter denominator: ";
    cin >> frac1.denom;

    cout << endl;
    cout << "SECOND FRACTION" << endl;
    cout << "* Enter numerator: ";
    cin >> frac2.num;
    cout << "* Enter denominator: ";
    cin >> frac2.denom;

    frac3.num = frac1.num * frac2.num;
    frac3.denom = frac1.denom * frac2.denom;
    cout << endl;
    cout << "PRODUCT: " << frac3.num << "/" << frac3.denom << endl;

    return 0;
  }
#+END_SRC



---------------------------------------
* *Review questions:*

#+ATTR_HTML: :class action-item
#+NAME: content
#+BEGIN_HTML
1. File guards are needed because...
2. What kind of data would be good to represent with a struct?
   - A character in a video game including the amount of experience points they have, their current hit points, and current level
   - The tax rate of a city
   - The URL of a website you're visiting
   - An address, including recipient name, street address, city, state, and zip code
3. If we declared an object variable like =Course cs200;= , How would we assign "cs" to the =department= member variable
   and 200 to the =code= member variable?
#+END_HTML
