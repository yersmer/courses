# -*- mode: org -*-

#+ATTR_HTML: :class action-item
#+NAME: content
#+BEGIN_HTML
*What we're gonna do:*
- Look at the cplusplus.com documentation for strings
- Work with some common string operations

- *IDE help:* Look at the "C++ projects - Integrated Development Environments" section under the Reference part of this textbook.
#+END_HTML


----------------------------------------------------------------
* *Graded program:*

#+INCLUDE: "starter_code/u07_Strings/graded_program/requirements.org"

-----------------------------------------------------------------------------------------
* *Turning in your work*

Go to the *GitLab* webpage, locate the file you want to update, and click the blue "*Edit*" button,
then click "*Edit single file*". Paste in your changes and click the "*Commit changes*" at the bottom of the page.
Copy the URL to the file or unit folder and paste it in as your submission on the Canvas assignment.

