#ifndef _FUNCTIONS
#define _FUNCTIONS

#include <string>
using namespace std;

// Function declarations:
void Tester();
void DisplayMenu();
void DisplayFormattedUSD( float );
float GetTaxPercent();
float GetPricePlusTax( float, float );
float GetNewPrice();
int GetChoice( int min, int max );
float StudentCode();
void Tester();

const string YOURNAME = "Your Name, Spring 2024"; // TODO: Update this to your name!

#endif
