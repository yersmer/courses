// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
#include <iomanip>    // Library for formatting; `setprecision`
using namespace std;  // Using the C++ STanDard libraries

#include "Functions.h"

// -- FUNCTION DEFINITION GOES HERE -------------------------------------------

/**
   Displays the numbered menu for the program using cout statements.
*/
void DisplayMenu()
{
  // TODO: Add DisplayMenu here
}

/**
   @param price            The price to display in USD format.
   Turn on USD formatting with the command:
   `cout << fixed << setprecision(2);`
   Afterwards, use a cout statement to display the `price`.
*/
void DisplayFormattedUSD( float price )
{
  // TODO: Add FormatUSD here
}

/**
   @return float            Returns the tax rate, as a percent (don't use %). (output)
   Returns the value 9.61.
*/
float GetTaxPercent()
{
  // TODO: Add GetTaxPercent here
  return -1; // TODO: REMOVE THIS AND REPLACE WITH PROPER DATA
}

/**
   @return float            Returns the price with tax that was calculated. (output)
   @param  original_price   The original price, without tax. (input)
   @param  tax_percent      The tax percent applied to this price. (input)
   Calculate the price plus tax and return the result.
   The total tax added on is going to be the original price times the tax percent.
   Add this amount onto the original price to get the updated price value.
   Return the updated price value.
*/
float GetPricePlusTax( float original_price, float tax_percent )
{
  // TODO: Add GetPricePlusTax here
  return -1; // TODO: REMOVE THIS AND REPLACE WITH PROPER DATA
}

/**
   @return float   Returns the new price that the user entered. (output)
   Display "Please enter a new price: ". Get the user's input and store it in a float variable.
   Return your variable at the end of this function.
*/
float GetNewPrice()
{
  // TODO: Add GetNewPrice function. Inputs: None, Output: float
}

/**
   @return int     The validated user input
   @param  min     The minimum valid value
   @param  max     The maximum valid value

   Create an int to store the user's input. Get their input and store it here.
   Use a while loop to keep looping while the input is invalid. In this case,
     ask the user to re-enter their selection.
   After the while loop, return the user's input.
*/
int GetChoice( int min, int max )
{
  // TODO: Add GetChoice function.
}

/**
   @return float    The final price once the user checks out.
   This function contains the main program loop and brings everything together.
*/
float StudentCode()
{
  float final_price = 0;
  float transaction_total = 0;
  bool running = true;
  float new_price;
  int choice;

  // TODO: Add program code

  return final_price;
}
