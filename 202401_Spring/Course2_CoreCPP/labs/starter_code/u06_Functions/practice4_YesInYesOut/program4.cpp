// PROGRAM: Practice with function - YES input, YES output
#include <iostream>
#include <string>
using namespace std;

/*
 *Example program output:*
 ------------------------------------------
 Enter price: $3.5
 Enter tax: %9.58
 Updated price: $3.8353
 ------------------------------------------

 *Function:*
 Implement the CalculatePricePlusTax function below.
 This function HAS input parameters so its parentheses ( ) will contain the following:
 `float original_price`, and `float tax_percent`.
 and it has a float output RETURN so its return type is `float`.

 Within this function we need to calculate the following:
 1. Tax as a decimal: take `tax_percent / 100` and store it in a float variable.
 2. Add on price: take `original_price` and multiply it by the "tax as a decimal" amount and store it in a float variable.
 3. New price: take `original_place` and add the "add on price".
 Return the new price as the function result.

 *main:*
 Within main, set the `updated_price` variable to the result from calling the `GetPricePlusTax` function.
 Afterwards the updated price will be displayed to the screen.
*/

// -- FUNCTION DEFINITION GOES HERE -------------------------------------------
/**
   CalculatePricePlusTax function
   NO input parameters / YES output return
   @param   original_price    The original price of an item (before tax)
   @param   tax_percent       The tax rate, in % form
   @return  float             The new price after tax is applied
   Calculates the price plus the additional price to due tax and returns the value.
*/
// TODO: Define the CalculatePricePlusTax function here


// -- MAIN PROGRAM FUNCTION ---------------------------------------------------
int main()
{
  float price, tax, updated_price;

  cout << "Enter price: $";
  cin >> price;
  cout << "Enter tax: %";
  cin >> tax;

  // TODO: Call the GetPricePlusTax function,

  cout << "Updated price: $" << updated_price << endl;

  return 0;
}
