// PROGRAM: Practice with function - NO input, NO output
#include <iostream>
#include <iomanip>
using namespace std;

/*
 *Example program output:*
 ------------------------------------------
 1. Add item          2. Checkout
 ------------------------------------------

 *Function:*
 Implement the DisplayMenu function definition below.
 This function has NO input parameters so its parentheses ( ) will be empty,
 and it has NO output return so its return type will be `void`.

 Within this function, display the numbered menu like in the example output above.

 *main:*
 Within main, call the DisplayMenu function.
 The function call will just be the function name and empty parentheses,
 with a semicolon ; at the end.
*/

// -- FUNCTION DEFINITION GOES HERE -------------------------------------------
/**
   DisplayMenu function
   NO input parameters / NO output return
   This function displays a numbered menu to the screen.
*/

// TODO: Add DisplayMenu function

// -- MAIN PROGRAM FUNCTION ---------------------------------------------------
/**
   main function
   Call the DisplayMenu function inside.
*/
int main()
{
  // TODO: Call DisplayMenu function

  return 0;
}
