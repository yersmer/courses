// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
#include <iomanip>    // Library for formatting; `setprecision`
using namespace std;  // Using the C++ STanDard libraries

/**
   @return int    The length of the string passed in.
   @param  text   The string we're investigating.
   Use the string library's size function to get and return the amount of characters in the `text` string.
   Docs: https://cplusplus.com/reference/string/string/size/
*/
unsigned int GetStringLength( string text )
{
  return 0; // PLACEHOLDER -- TODO: REMOVE ME!
}

/**
   @return char      The character at the given position.
   @param  text      The string we're investigating.
   @param  position  The # position of the character we wish to return from the string.
   Use the string subscript operator [ ] to return the character at the `position` given.
   Docs: https://cplusplus.com/reference/string/string/operator[]/
*/
char GetLetter( string text, unsigned int position )
{
  return 'x'; // PLACEHOLDER -- TODO: REMOVE ME!
}

/**
   @return int        The position where the `needle` is found, or string::npos if not found.
   @param  haystack   The big string to search within.
   @param  needle     The substring we're trying to find within the `haystack`.
   Use the find function on haystack to try to find the needle. Return the result.
   Docs: https://cplusplus.com/reference/string/string/find/
*/
int GetSubstringPosition( string haystack, string needle )
{
  return -1; // PLACEHOLDER -- TODO: REMOVE ME!
}

/**
   @return string    The updated string, once str1 and str2 have been combined.
   @param  str1      The base string.
   @param  str2      The string to add onto the end of str1.
   Use the concatenation operator + to combine the strings and return the resulting string.
   Docs: https://cplusplus.com/reference/string/string/operator+/
*/
string CombineStrings( string str1, string str2 )
{
  return "x"; // PLACEHOLDER -- TODO: REMOVE ME!
}

/**
   @return int       The comparison result after comparing str1 and str2.
   @param  str1      The first string to compare.
   @param  str2      The second string to compare.
   Return the result of taking str1 and comparing str2 to it.
   Docs: https://cplusplus.com/reference/string/string/compare/
*/
int CompareStrings( string str1, string str2 )
{
  return 0; // PLACEHOLDER -- TODO: REMOVE ME!
}

/**
   @return string    Text that describes str1's relationship to str2 ( <, >, or ==. )
   @param  str1      The first string to compare.
   @param  str2      The second string to compare.
   If str1 is greater than str2, then return the text "[str1] > [str2]".
   Otherwise if str1 is less than str2, then return the text "[str1] < [str2]".
   Otherwise, return the text "[str1] == [str2]".
   Docs: https://cplusplus.com/reference/string/string/operators/
*/
string StringRelations( string str1, string str2 )
{
  return "x"; // PLACEHOLDER -- TODO: REMOVE ME!
}

/**
   @return string     An updated string after the `insert_me` text has been inserted to the original at the position given.
   @param  original   The original string to work with.
   @param  insert_me  The new string to insert into the original string.
   @param  position   The position at which to insert `insert_me` into the new string.
   Docs: https://cplusplus.com/reference/string/string/insert/
*/
string InsertString( string original, string insert_me, int position )
{
  return "x"; // PLACEHOLDER -- TODO: REMOVE ME!
}

/**
   @return string    An updated string after the characters at position+length have been removed.
   @param  original  The original string to work with.
   @param  position  The position to begin removing characters at.
   @param  length    The amount of characters to remove.
   Docs: https://cplusplus.com/reference/string/string/erase/
*/
string EraseFromString( string original, int position, int length )
{
  return "x"; // PLACEHOLDER -- TODO: REMOVE ME!
}

/**
   @return string     An updated string after the item at position+length is replaced with `insert_me`.
   @param  original   The original string to work with.
   @param  insert_me  The new string to insert into the original string.
   @param  position   The position (in the original string) to begin the replacement in.
   @param  length     The amount of characters (in the original string) to replace.
   Docs: https://cplusplus.com/reference/string/string/replace/
*/
string ReplaceInString( string original, string insert_me, int position, int length )
{
  return "x"; // PLACEHOLDER -- TODO: REMOVE ME!
}
