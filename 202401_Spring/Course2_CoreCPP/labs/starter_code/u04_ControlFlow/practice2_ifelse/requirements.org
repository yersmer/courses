# -*- mode: org -*-

*=practice2_ifelse=: If/else statement*

#+BEGIN_SRC cpp :class cpp
// PROGRAM: Practice if/else statements

// - INCLUDES ---------------------------------------------------------------//
#include <iostream>   // Library that contains `cout` commands
#include <string>     // Library that contains `string` types
using namespace std;  // Using the C++ STanDard libraries

// - PROGRAM CODE -----------------------------------------------------------//
int main()
{
  // TODO: Create an integer `number` variable. Ask the user to enter a value for the number.

  // TODO: If number is greater than 0, then display "Positive number". Otherwise, display "Negative or zero".

  // Return 0 means quit program with no errors, in this context.
  return 0;
}
#+END_SRC

*Program inputs:*

| Variable | Description        |
|----------+--------------------|
| =number= | Some integer value |

*Program outputs:*

| Scenario | Output             | Criteria                               |
|----------+--------------------+----------------------------------------|
|       1. | =positive=         | The number passed in is greater than 0 |
|       2. | =zero or negative= | The number passed in is 0 or below     |

*Program logic:*

Given the =number= passed into the program, use an if/else statement to determine
whether it is either (A) POSITIVE or (B) ZERO OR NEGATIVE. Return either ="positive"=
or ="zero or negative"=.

Tips:

- You don't need an "else if" statement here; the =else= executes if the =if= statement is false...
  The opposite of $x > y$ is $x \leq y$.

*Example output:*

Passing in a 6:
#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
Enter a number: 6
Positive number
#+END_SRC

Passing in a 0:
#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
Enter a number: 0
Negative or zero
#+END_SRC

Passing in a -5:
#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
Enter a number: -5
Negative or zero
#+END_SRC


