# -*- mode: org -*-

*Example program output:*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
Enter a low number: 2
Enter a high number: 8

Low to high:
2 3 4 5 6 7 8

High to low:
8 6 4 2

Multiply up:
2 4 8

GOODBYE
#+END_SRC

*Create a project:* Create a new project in this assignment's folder and use the *add existing item* feature
of your IDE to add =forloops.cpp= to your project.

At the beginning of the program the user is asked for a =low= number and a =high= number. We will be using these in our for loops.

*Generic for loop:* A for loop comes in this form:

=for ( START; CONDITION; UPDATE ) { /* loop body */ }=


--------------------------------------------

*Counting up with a for loop:*

First create a for loop with the following properties:
- The start action is declaring an integer =i= and initializing it to the =low= value.
- The condition is to keep looping while =i= is less than or equal to the =high= value.
- The update action is to incremenet =i= each time: =i++=.

Within the function body write a cout statement that displays =i= and then a space...

#+BEGIN_SRC cpp :class cpp
  for ( ???; ???; ??? )
  {
    cout << i << " ";
  }
  cout << endl << endl; // Added for spacing :)
#+END_SRC

--------------------------------------------

*Counting down with a for loop:*

Next create a for loop with the following properties:
- The start action is declaring =int i= and initializing it to the =high= value.
- The condition is to keep looping while =i= is greater than or equal to the =low= value.
- The update action is to decrement =i= each time: =i--=.

The inside of your for loop should be the same as above: display =i= and then a space.

--------------------------------------------

*Multiply up with a for loop:*

Finally, now we're going to multiply our counter variable by 2 each time in a loop.

Create a loop with the following properties:
- The start action is declaring an integer =i= and initializing it to the =low= value.
- The condition is to keep looping while =i= is less than or equal to the =high= value.
- The update action is to multiple =i= by 2 each time and assign it back to the =i= variable: =i*=2;=
