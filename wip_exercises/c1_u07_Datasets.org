# -*- mode: org -*-

#+TITLE: CS134 Unit 07 Exercise: Lists/Vectors of data
#+AUTHOR: Rachel Wil Sha Singh

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/rworgmode.css" />
#+HTML_HEAD: <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
#+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">
#+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script> <script>hljs.initHighlightingOnLoad();</script>

-----
* Introduction

** Creating a list/vector

#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML

Python:
#+BEGIN_SRC python :class python
  // General: LISTNAME = []

  string_list = []
  int_list = []
#+END_SRC

#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

C++:
#+BEGIN_SRC cpp :class cpp
  // General: vector<DATATYPE> VECTORNAME;

  vector<string> string_list;
  vector<int> int_list;
#+END_SRC

In C++, you'll need to have =#include <vector>= at the top of the file.

#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML


** Adding items to the list/vector

#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML

Python:
#+BEGIN_SRC python :class python
  // General: LISTNAME.append( DATA );

  studentList.append( "Kabe" )

  name = "Luna"
  studentList.append( name )
#+END_SRC

#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

C++:
#+BEGIN_SRC cpp :class cpp
  // General: LISTNAME.push_back( DATA );

  studentList.push_back( "Kabe" );

  string name = "Luna";
  studentList.push_back( name );
#+END_SRC

#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML



** Accessing items at some index

#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML

Python:
#+BEGIN_SRC python :class python
  // General: LISTNAME[ INDEX ]

  print( "first:", studentList[0] )
  print( "second:", studentList[1] )
  print( "third:", studentList[2] )
  print( "fourth:", studentList[3] )
#+END_SRC

#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

C++:
#+BEGIN_SRC cpp :class cpp
  // General: LISTNAME[ INDEX ]

  cout << "first: " << studentList[0] << endl;
  cout << "second: " << studentList[1] << endl;
  cout << "third: " << studentList[2] << endl;
  cout << "fourth: " << studentList[3] << endl;
#+END_SRC

#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML

Using a variable to access an element:

#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML

Python:
#+BEGIN_SRC python :class python
  index = int( input( "Enter index:" ) )
  print( "That is:", studentList[index] )
#+END_SRC

#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

C++:
#+BEGIN_SRC cpp :class cpp
  int index;
  cout << "Enter index: ";
  cin >> index;
  cout << "That is: " << studentList[index] << endl;
#+END_SRC

#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML





** Looping through all the elements

*** For loop 1: getting an index and element

Here we use a *counter variable*, which we usually name =i=. =i= will begin at 0, go up to 1, then 2, and so on until it hits the last index of the list of items.
=i= represents the *index* and =studentList[i]= represents the *element*.


#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML

Python:

#+BEGIN_SRC python :class python
  for i in range( len( LISTNAME ) ):
     # Display index (i) and
     # then the element (LISTNAME[i]):
     print( i, LISTNAME[i] )
#+END_SRC

#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

C++:

#+BEGIN_SRC cpp :class cpp
    for ( int i = 0; i < LISTNAME.size(); i++ )
    {
      // Display index (i) and
      // then the element (LISTNAME[i]):
      cout << i << ". " << LISTNAME[i] << endl;
    }
#+END_SRC

#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML



*** For loop 2: getting just the element

For these loops, they use an *alias variable* - each time through the loop, the =student= placeholder is filled with the next item in the list.

#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML

Python:

#+BEGIN_SRC python :class python
  for item in LISTNAME:
      print( item )
#+END_SRC

#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

C++:

#+BEGIN_SRC cpp :class cpp
  for ( auto& item : LISTNAME )
  {
    cout << item << endl;
  }
#+END_SRC

#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML


-----

* Program part 1: Recipe names

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
Recipes:
0. Gulab Jamun
1. Aloo Paratha
2. Mango Lassi
#+END_SRC

Create a list/vector of =strings= named =recipes=. Each element should be the name of a different recipe.

*Without using a loop*, display each element's index (e.g., "0") and then the element's value (e.g., "Chocolate Chip Cookies").
The index number you print out, like 0, 1, 2, will just be hard coded, like =print( "0. ")=. The element value will be
accessed by using the list name (=recipes=), the subscript operator (=[ ]=), and the index number (=0=)... so... =recipes[0]=.

-----
* Program part 2: Adding onto the list

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
Recipes:
0. Gulab Jamun
1. Aloo Paratha
2. Mango Lassi

Enter a new recipe name: Rasgulla

3. Rasgulla

There are now 4 recipe(s).
#+END_SRC

Continue editing the same program file. After the previous items, ask the user to *etner a new recipe name*. This will be string input.
After getting their input and storing it in a temporary variable, append/push their selection to your =recipes= list.

At the end, do another print/cout to display the new recipe item's idnex and value.

You can also display how many items are in the =recipes= list with:

Python:

#+BEGIN_SRC python :class python
  print( len( recipes ) )
#+END_SRC

C++:
#+BEGIN_SRC cpp :class cpp
  cout << recipes.size() << endl;
#+END_SRC

-----
* Program part 3: Looping through the list

Adding onto the previous part, create a loop that displays all the recipes at the end. Use the examples below to
write your for loop to display the index (=i=) and the value (replace "LISTNAME" with =recipes=.)

#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML

Python:

#+BEGIN_SRC python :class python
  for i in range( len( LISTNAME ) ):
     # Display index (i) and
     # then the element (LISTNAME[i]):
     print( i, LISTNAME[i] )
#+END_SRC

#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

C++:

#+BEGIN_SRC cpp :class cpp
    for ( int i = 0; i < LISTNAME.size(); i++ )
    {
      // Display index (i) and
      // then the element (LISTNAME[i]):
      cout << i << ". " << LISTNAME[i] << endl;
    }
#+END_SRC

#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML



#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
Recipes:
0. Gulab Jamun
1. Aloo Paratha
2. Mango Lassi

Enter a new recipe name: Rasgulla

3. Rasgulla

There are now 4 recipe(s).

0. Gulab Jamun
1. Aloo Paratha
2. Mango Lassi
3. Rasgulla
#+END_SRC


-----
