# -*- mode: org -*-

#+TITLE: CS235 Unit 11: Static
#+AUTHOR: Rachel Wil Sha Singh

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/rworgmode.css" />
#+HTML_HEAD: <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
#+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">
#+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script> <script>hljs.initHighlightingOnLoad();</script>

-----

* Assignment information

** Grabbing a ticket:
In your team repository assign yourself one of the tickets that are labeled "U11EX":

- [[https://gitlab.com/rsingh13-student-repos/2023-08_cs235/202308_cs235_team-guava/-/issues/?sort=created_date&state=opened&milestone_title=U11EX&first_page_size=20][Team Guava issues]]
- [[https://gitlab.com/rsingh13-student-repos/2023-08_cs235/202308_cs235_team-lychee/-/issues/?sort=created_date&state=opened&milestone_title=U11EX&first_page_size=20][Team Lychee issues]]
- [[https://gitlab.com/rsingh13-student-repos/2023-08_cs235/202308_cs235_team-mango/-/issues?sort=created_date&state=opened&milestone_title=U11EX&first_page_size=20][Team Mango issues]]
- [[https://gitlab.com/rsingh13-student-repos/2023-08_cs235/202308_cs235_team-papaya/-/issues?sort=created_date&state=opened&milestone_title=U11EX&first_page_size=20][Team Papaya issues]]

*If someone else had a certain area for the U05EX assignment, please let them have dibs on the U11EX assignment
for the same area.*

All of the assignment details are in the ticket description. I've tried to make it more descriptive than last time.

** Working with the code
Reminder that you need to be working *INSIDE YOUR TEAM REPOSITORY*. There is already files for the entire program
and project files set up! These files should STAY IN THE ORIGINAL PATHS. ALL FILES ARE REQUIRED, this is an example
of a "large" codebase, like what you would work with in the real world.

DIRECTORY OF THE MUSIC APP PROGRAM:

#+begin_src artist
202308_cs235_team-YOURTEAM
├── diagram.dia
├── diagram.png
├── readme.org
└── source
    ├── data
    │   └── users
    │       ├── playlists.csv
    │       └── users.csv
    ├── main.cpp
    ├── Namespace_Application
    │   ├── MenuApp.cpp
    │   ├── MenuApp.h
    │   ├── ProgramManager.cpp
    │   └── ProgramManager.h
    ├── Namespace_Audiobook
    │   ├── Audiobook.cpp
    │   ├── Audiobook.h
    │   ├── AudiobookManager.cpp
    │   ├── AudiobookManager.h
    │   ├── AuthorManager.cpp
    │   ├── AuthorManager.h
    │   ├── BookArtist.cpp
    │   ├── BookArtist.h
    │   ├── Chapter.cpp
    │   ├── Chapter.h
    │   ├── ChapterManager.cpp
    │   └── ChapterManager.h
    ├── Namespace_Metadata
    │   ├── Genre.cpp
    │   ├── Genre.h
    │   ├── Language.cpp
    │   ├── Language.h
    │   ├── Role.cpp
    │   ├── Role.h
    │   ├── StatusCode.cpp
    │   ├── StatusCode.h
    │   ├── StatusCodeManager.cpp
    │   └── StatusCodeManager.h
    ├── Namespace_Music
    │   ├── Album.cpp
    │   ├── Album.h
    │   ├── AlbumManager.cpp
    │   ├── AlbumManager.h
    │   ├── ArtistManager.cpp
    │   ├── ArtistManager.h
    │   ├── MusicArtist.cpp
    │   ├── MusicArtist.h
    │   ├── Track.cpp
    │   ├── Track.h
    │   ├── TrackManager.cpp
    │   └── TrackManager.h
    ├── Namespace_Podcast
    │   ├── CreatorManager.cpp
    │   ├── CreatorManager.h
    │   ├── Episode.cpp
    │   ├── Episode.h
    │   ├── EpisodeManager.cpp
    │   ├── EpisodeManager.h
    │   ├── ShowArtist.cpp
    │   ├── ShowArtist.h
    │   ├── Show.cpp
    │   ├── Show.h
    │   ├── ShowManager.cpp
    │   └── ShowManager.h
    ├── Namespace_Testing
    │   ├── TesterBase.cpp
    │   ├── TesterBase.h
    │   ├── TesterManager.cpp
    │   └── TesterManager.h
    ├── Namespace_User
    │   ├── Playlist.cpp
    │   ├── Playlist.h
    │   ├── PlaylistManager.cpp
    │   ├── PlaylistManager.h
    │   ├── PlaylistTester.h
    │   ├── User.cpp
    │   ├── User.h
    │   ├── UserManager.cpp
    │   ├── UserManager.h
    │   ├── UserManagerTester.h
    │   └── UserTester.h
    ├── Namespace_Utilities
    │   ├── CsvParser.cpp
    │   ├── CsvParser.h
    │   ├── Helper.cpp
    │   ├── Helper.h
    │   ├── Logger.cpp
    │   ├── Logger.h
    │   ├── Messager.cpp
    │   ├── Messager.h
    │   ├── NotImplementedException.h
    │   ├── Platform.cpp
    │   ├── Platform.h
    │   ├── Preprocessor.h
    │   ├── ScreenDrawer.cpp
    │   └── ScreenDrawer.h
    ├── Project_CodeBlocks
    │   ├── BandroidProject.cbp
    │   ├── BandroidProject.depend
    │   └── BandroidProject.layout
    ├── Project_Makefile
    │   └── Makefile
    └── Project_VisualStudio2022
        ├── MusicApp.sln
        ├── MusicApp.vcxproj
        └── MusicApp.vcxproj.filters
#+end_src


*** Fresh clone:
1. Copy the HTTPS link from the repository webpage.
2. On your harddrive, open up Git Bash and use the clone command: =git clone PROJECTURL=
3. Now the =202308_cs235_team-YOURTEAM= folder will be on your harddrive.
   Work with the files in this directory. DO NOT MOVE OR REARRANGE THE FILES. Work with the Visual Studio or Code::Blocks projects provided,
   or use the Makefile to build the project from the command line.

*** Getting latest:

*I have spent time cleaning up the codebase after U05EX. PLEASE MAKE SURE TO DO THE FOLLOWING BEFORE GETTING STARTED:*

1. Check out the master branch: =git checkout master=
2. Pull latest changes: =git pull origin master=
3. If you experience merge conflicts, you may just want to create a fresh clone (I changed a lot of files,
   but preserved everyone's work... just with new function names.)

*** Creating a new branch for this assignment

Once you've pulled all the latest changes, you will need to create a new branch to work on.

1. Create a new branch: =git checkout -b NAME_U11EX=





** For reference: User and Playlist
I have the User, Playlist, and UserManager and PlaylistManager classes implemneted
and you can use these for reference for your own work.


** Reviewing someone else's code
While your code review doesn't have to be perfect, *don't just approve others' work if there is something glaringly wrong*.

For example, with U05EX, tickets were approved even if they were missing the *Exceptions*, or implemented Exceptions incorrectly.
In each ticket I've given kind of a rundown on what I expect from code reviews.

You will now have a few points assigned to *how well you review someone's code*, so make sure to do a code review
for somebody else and to get a code review for your code as well.

Again, the review doesn't have to be perfect; you don't have to /pull their code and test it/, but obvious issues should be pointed out.


-----
