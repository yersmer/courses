# -*- mode: org -*-

#+TITLE: CS134 Unit 03 Exercise: Designing software
#+AUTHOR: Rachel Wil Sha Singh

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="https://rachels-courses.gitlab.io/webpage/web-assets/style/rworgmode.css" />
#+HTML_HEAD: <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
#+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">
#+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script> <script>hljs.initHighlightingOnLoad();</script>

-----

* Student A, B, and C Instructions

** Creating a code file
You will first need to create a code file within your "studentA" folder. 

You can add a file by hovering over the "studentA" folder, clicking the "..." menu, then selecting "Add file".

You can name your file anything, just don't have spaces. The file extension should reflect which language you want to use:

| C++  | Lua  | Python |
|------+------+--------|
| .cpp | .lua | .py    |

-----

** Starter code

Use one of the following items as starter code:

*C++*
#+BEGIN_SRC c++ :class cpp
#include <iostream>
using namespace std;

int main()
{
  cout << "Hello, world!" << endl;
  return 0;
}
#+END_SRC

*Lua*
#+BEGIN_SRC c++ :class lua
print( "Hello, world! (Lua edition :)" )
#+END_SRC

*Python*
#+BEGIN_SRC c++ :class python
print( "Hello, world! (Python edition :)" )
#+END_SRC

-----

** Running the starter code

Before getting started on the assignment, make sure that the starter code runs alright. Use the =cd= command to go into your folder:

#+ATTR_HTML: :class console
#+BEGIN_SRC bash
cd studentA
#+END_SRC

And from here, run your program:

*C++*
#+ATTR_HTML: :class console
#+BEGIN_SRC bash
g++ FILENAME.cpp && ./a.out
#+END_SRC

*Lua*
#+ATTR_HTML: :class console
#+BEGIN_SRC bash
lua FILENAME.lua
#+END_SRC

*Python*
#+ATTR_HTML: :class console
#+BEGIN_SRC bash
python3 FILENAME.py
#+END_SRC

-----

** Program instructions

1. Find a (short) recipe off a website. (Example: https://www.allrecipes.com/recipe/246889/super-easy-peanut-butter-cookies/)
2. Using =print= statements (Lua/Python) or =cout= statements (C++), write a program that displays that recipe to the screen.
   Make sure that the output is readable, such as having sections clearly marked.
3. Afterwards, have a teammate run your recipe program, and run one of your teammates' programs. Each of you give constructive feedback:
   a. Is it easy to read the program?
   b. Is it clear what this propgram does?
   c. Any ideas to make the design better?

-----

** Example output

#+ATTR_HTML: :class console
#+BEGIN_SRC bash
  ------------------------------------------------------------------------------------------
  Super Easy Peanut Butter Cookies
  From https://www.allrecipes.com/recipe/246889/super-easy-peanut-butter-cookies/
  ------------------------------------------------------------------------------------------

  INGREDIENTS
  - 1 cup smooth peanut butter
  - 1/2 cup white sugar
  - 1 large egg

  DIRECTIONS

    STEP 1: Preheat the oven to 325 degrees F (165 degrees C).

    STEP 2: Mix together peanut butter, sugar, and egg in a large bowl until well blended.

    STEP 3: Roll dough into 24 balls; place 4 inches apart on ungreased baking sheets. Flatten each ball with the back of a fork.

    STEP 4: Bake in the preheated oven until lightly browned, about 20 minutes; do not overbake. Cool on the baking sheets for 5 minutes, then transfer to wire racks to cool completely. 
#+END_SRC

