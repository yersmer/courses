#include "Programs.h"
#include "Functions.h"

void Program1()
{
  cout << endl << "------------------------------------------" << endl;
  cout << "DIVISION EXAMPLE" << endl;

  float numerator, denominator;
  cout << "Enter a numerator: ";
  cin >> numerator;

  cout << "Enter a denominator: ";
  cin >> denominator;

  float quotient;

  cout << endl;

  quotient = Divide(numerator, denominator);
  cout << "Quotient: " << quotient << endl;
}

void Program2()
{
  cout << endl << "------------------------------------------" << endl;
  cout << "ARRAY EXAMPLE" << endl;

  array<string, 5> myArray = { "cat", "bat", "rat", "gnat", "goat" };

  cout << "Display item at which index? ";
  int index;
  cin >> index;

  cout << endl;

  Display(myArray, index);
}

void Program3()
{
  cout << endl << "------------------------------------------" << endl;
  cout << "MULTIPLE EXCEPTIONS EXAMPLE" << endl;

  int* dynamicArray = new int[4];
  dynamicArray[0] = rand() % 100;
  dynamicArray[1] = rand() % 100;
  dynamicArray[2] = rand() % 100;
  dynamicArray[3] = rand() % 100;

  int* badPointer = nullptr;

  cout << "Display item at which index? ";
  int index;
  cin >> index;

  cout << endl;

  cout << "dynamicArray ";
  PtrDisplay(dynamicArray, 4, index);

  cout << endl << "badPointer ";
  PtrDisplay(badPointer, 4, index);

  delete[] dynamicArray;
}

void Program4()
{
  cout << endl << "------------------------------------------" << endl;
  cout << "CUSTOM EXAMPLE" << endl;

  int friendCount, pizzaSliceCount;
  cout << "How many pizza slices at pizza party? ";
  cin >> pizzaSliceCount;

  cout << "How many friends at party? ";
  cin >> friendCount;

  cout << endl;

  int slices = SlicesPerPerson(friendCount, pizzaSliceCount);
  cout << "Give each friend " << slices << " slices of pizza" << endl;
}


