#include <iostream>
using namespace std;

int main()
{
  cout << "Alice’s Adventures in Wonderland" << endl;
  cout << "by Lewis Carroll" << endl;
  cout << endl;
  cout << "CHAPTER I. Down the Rabbit-Hole" << endl;
  cout << endl;
  cout << "Alice was beginning to get very tired of sitting by her sister on the   " << endl;
  cout << "bank, and of having nothing to do: once or twice she had peeped into    " << endl;
  cout << "the book her sister was reading, but it had no pictures or              " << endl;
  cout << "conversations in it, “and what is the use of a book,” thought Alice     " << endl;
  cout << "“without pictures or conversations?”                                    " << endl;
  cout << "                                                                        " << endl;
  cout << "So she was considering in her own mind (as well as she could, for the   " << endl;
  cout << "hot day made her feel very sleepy and stupid), whether the pleasure of  " << endl;
  cout << "making a daisy-chain would be worth the trouble of getting up and       " << endl;
  cout << "picking the daisies, when suddenly a White Rabbit with pink eyes ran    " << endl;
  cout << "close by her.                                                           " << endl; 
  
  return 0;
}
