#include <iostream>
#include <string>
using namespace std;

// DEFINE CLASS:
// * Name: Ingredient
// * Purpose: Represents a single ingredient of a recipe
//
// * Member variables:
//   * m_name: string
//   * m_amount: float
//   * m_unit: string    Such as "cups", "tsps", etc.
//
// * Member functions (aka methods):
//   * Setup
//      * INPUTS: new_name (string), new_amount (float), new_unit (string)
//      * RETURNS: NONE
//      * FUNCTIONALITY: Set m_name to new_name, m_amount to new_amount, and m_unit to new_unit.
//   * Display
//      * INPUTS: NONE
//      * RETURNS: NONE
//      * Display all the ingredient info in one line (e.g. "1.5 cup(s) butter")



int main() {
  // PROGRAM CODE
      
  // Create 1 Ingredient variable: `ingredient`
  
  // Call Setup on the ingredient, setting its name, amount, and unit of measurement.
  
  // Call Display on the ingredient.
}