#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

// DEFINE CLASS:
// * Name: Dice
// * Purpose: Represents a dice.
//
// * Member variables:
//   * m_sides: int
//
// * Member functions (aka methods):
//   * SetSides
//      * INPUTS: new_sides (int)
//      * RETURNS: none
//   * Roll
//      * INPUTS: none
//      * RETURNS: a number between 1 and the number of sides
//      * Use the `random.randint( 1, new_sides )` function call to get a random value between 1 and the amount of sides. Return this as the result.

int main() {
  // PROGRAM CODE
    
  // Ask the user how many sides to give the dice. Store this in an integer variable `sides`.
  
  // Create a Dice variable `my_dice`.
  
  // Call the SetSides function of `my_dice`, passing in the `sides` variable as input.
  
  // Ask the user how many times to roll the dice. Store this in an integer variable named `roll_count`.
  
  // Use a for loop to loop `roll_count` amount of times. Within the loop...
  
  //    - Call the Roll function of `my_dice` and display the result
}