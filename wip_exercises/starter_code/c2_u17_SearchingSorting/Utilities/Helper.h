#ifndef _HELPER
#define _HELPER

#include <string>
bool StringContains( std::string haystack, std::string needle, bool caseSensitive = true );

#endif