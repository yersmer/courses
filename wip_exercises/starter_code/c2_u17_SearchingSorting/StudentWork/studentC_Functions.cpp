/* STUDENT NAME: 
 * SEMESTER/YEAR: 
*/

#include "../Courses/CourseManager.h"
#include "../Utilities/Helper.h"
#include <utility>
using namespace std;

vector<Course> CourseManager::SearchCoursesByTitle(const vector<Course> &original, string searchTerm)
{
  vector<Course> matches;

  // TODO: IMPLEMENT LINEAR SEARCH, SEARCH BY TITLE
  // 1. Use a for loop to iterate over all the items in the "original" vector
  // 2. Inside the for loop, use an if statement to check if the title of the current item is equal to the search term
  // 2a. Within the if statement, if the text is found, then push the current element onto the "matches" vector.
  // 3. Before the function ends, return the "matches" vector.

  return matches;
}

vector<Course> CourseManager::BubbleSortCoursesByTitle(const vector<Course> &original)
{
  vector<Course> sortCourses = original;

  // TODO: IMPLEMENT BUBBLE SORT HERE, SORT BY COURSE TITLES
  // See reading material for the algorithm!

  return sortCourses;
}
