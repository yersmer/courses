--------------------------------------------------------
INTRODUCTION (Scroll down further for program instructions)
--------------------------------------------------------

-- FUNCTIONS: NO INPUTS, YES OUTPUTS -- 
Functions like these are used to retrieve data. Sometimes it will be from other places in the program, or from a data file or database. In this case we're just returning some hard-coded data.

These functions take in no inputs so what they output is not affected by any input; their output is the same every time.


-- BUILDING YOUR PROGRAM --
You will need to make sure to build ALL header and source files now.

cd studentC/p3_NoInYesOut
g++ *.h *.cpp -o program.out


-- EXAMPLE PROGRAM OUTPUT --
./program.out 
Error code is: 404

--------------------------------------------------------
PROGRAM INSTRUCTIONS
--------------------------------------------------------

Within the Functions.h file, create a function DECLARATION that meets the following criteria:
* Name: GetErrorCode
* Input Parameters: NONE
* Output Return: int

Within the Functions.cpp file, you will add your function DEFINITION. The function header needs to match the header from the .h file exactly, except with no `;` at the end. Instead, you'll have a code block { } and put program logic within.
Within the function, just return the city name: Overladn Park


Within main() do the following:
1. Create an int variable to store the `error`.
2. Call the `GetErrorCode` function. It will have no input arguments in () but it returns an int, so assign its return value to your `error` variable.
3. Display the `error` to the screen.






