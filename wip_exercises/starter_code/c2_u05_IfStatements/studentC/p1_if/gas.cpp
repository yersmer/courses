/* STUDENT NAME: 
 * SEMESTER/YEAR: 
*/

#include <iostream>
using namespace std;

int main( int argCount, char* args[] )
{
  if ( argCount < 3 ) { cout << "NOT ENOUGH ARGUMENTS! Expected: gas capacity" << endl; return 1; }
  
  float gas = stof( args[1] );
  float capacity = stof( args[2] );
  
  // -- PROGRAM CONTENTS GO HERE --------------------------------------------------

  
  
  return 0;
}
