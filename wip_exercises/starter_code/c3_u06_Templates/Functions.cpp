#include "Functions.h"

void Test_Display()
{
    cout << endl << string( 20 , '-' ) << " Test_Display (Manual tests) " << string( 20, '-') << endl;
    { // test begin
        cout << "Test 1 - Classes" << endl;
        vector<string> header = { "Class 1", "Class 2", "Class 3", "Class 4" };
        vector<vector<string>> data = {
            { "CS 134", "CS 200", "CS 235", "CS 250" },
            { "ASL 120", "ASL 122", "ASL 135", "ASL 145" }
        };

        Display( header, data );
    } // test end

    cout << endl << endl;

    { // test begin
        cout << "Test 2 - Minimum wage vs. Median monthly rent" << endl;
        vector<string> header = { "2010", "2000", "1990", "1980", "1970", "1960", "1950" };
        vector<vector<float>> data = {
            { 7.25, 5.15, 3.80, 3.10, 1.60, 1.00, 0.75 },
            { 841,  602,  447,  243,  108,  71,   42  },
        };

        Display( header, data );
    } // test end
}

