# PROGRAM INSTRUCTIONS

## Program 1: size
Use the string library's `size` function to display the length
of the string passed in as an argument (`str`)

https://cplusplus.com/reference/string/string/size/

### Building the program:
```
cd studentA/p1_strings
g++ size.cpp -o size.out
```

### Example output:
```
./size.out bob
Length is: 3
```


--------------------------------------------------------------------------------
## Program 2: concat
Use the string library's concatenation operator `+` to combine
the two strings (`string1`, `string2`).

https://cplusplus.com/reference/string/string/operator+/

### Building the program:
```
cd studentA/p2_strings
g++ concat.cpp -o concat.out
```

### Example output:
```
./concat.out one two
string3 is onetwo
```


--------------------------------------------------------------------------------
## Program 3: insert

Use the string library's `insert` function to insert `string2`
into `string1` at the `index` specified.

https://cplusplus.com/reference/string/string/insert/


### Building the program:
```
cd studentA/p3_strings
g++ insert.cpp -o insert.out
```

### Example output:
```
./insert wolf erew 1
string1 is now werewolf
```


