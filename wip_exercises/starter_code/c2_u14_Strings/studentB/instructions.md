# PROGRAM INSTRUCTIONS

## Program 1: letter
Use the string library's subscript operator `[]` to get the letter at the `index` given.

https://cplusplus.com/reference/string/string/operator[]/

### Building the program:
```
cd studentB/p1_strings
g++ letter.cpp -o letter.out
```

### Example output:
```
./letter.out meow 2
Letter #2 is: o
```

--------------------------------------------------------------------------------
## Program 2: order
Use the string library's `compare` function to compare the two strings.

https://cplusplus.com/reference/string/string/compare/

### Building the program:
```
cd studentB/p2_strings
g++ order.cpp -o order.out
```

### Example output:
```
./order.out a b
Result is -1
```


--------------------------------------------------------------------------------
## Program 3: remove

Use the string library's `erase` function to remove a chunk of text, starting at the `index` and running for the `length`.

https://cplusplus.com/reference/string/string/erase/


### Building the program:
```
cd studentB/p3_strings
g++ remove.cpp -o remove.out
```

### Example output:
```
./remove homestarrun 4 4
string1 is now homerun
```


