# PROGRAM INSTRUCTIONS

## Program 1: find
Use the string library's `find` function to check if `string2` is in `string1`.

https://cplusplus.com/reference/string/string/find/

### Building the program:
```
cd studentC/p1_strings
g++ find.cpp -o find.out
```

### Example output:
```
./find.out racecar ace
Position of string2 is 1
```

--------------------------------------------------------------------------------
## Program 2: order
Use the string library's relational operators to compare if `string1` is <, >, or == `string2`.

https://cplusplus.com/reference/string/string/operators/

### Building the program:
```
cd studentC/p2_strings
g++ compare.cpp -o compare.out
```

### Example output:
```
./compare.out cat dog
string1 < string2
```


--------------------------------------------------------------------------------
## Program 3: remove

Use the string library's `replace` function to replace part of `string1` with `string2`.

https://cplusplus.com/reference/string/string/replace/


### Building the program:
```
cd studentC/p3_strings
g++ replace.cpp -o replace.out
```

### Example output:
```
./replace.out marioworld land 5 5
string1 is now marioland
```


