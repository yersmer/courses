/*
YOU DO NOT NEED TO MODIFY THIS FILE.
*/

#include <iostream>
#include <iomanip>
#include <string>
#include <cstdlib>
#include <ctime>
using namespace std;

#include "CoinTests.h"
#include "Coin.h"

void CoinProgram()
{
  string a, b;
  cout << "What's on side A? ";
  cin >> a;
  cout << "What's on side B? ";
  cin >> b;
  
  Coin coin( a, b );
    
  int times;
  cout << "Flip how many times? ";
  cin >> times;
  
  for ( int i = 0; i < times; i++ )
  {
    cout << "Flip #" << i << ": " << coin.Flip() << endl;
  }
}

// You don't need to modify main()
int main( int argCount, char* args[] )
{
    srand( time( NULL ) );
    
    if ( argCount < 2 )
    {
      cout << "EXPECTED FORM: " << args[0] << " run/test" << endl;
      return 1;
    }
    
    string action = string( args[1] );
    if ( action == "run" )
    {
      CoinProgram();
    }
    else if ( action == "test" )
    {
      CoinTest();
    }

    return 0;
}
