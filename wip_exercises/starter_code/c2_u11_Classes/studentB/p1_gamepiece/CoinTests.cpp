/* STUDENT NAME: 
 * SEMESTER/YEAR: 
*/
/*
YOU DO NOT NEED TO MODIFY THIS FILE.
*/

#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

#include "CoinTests.h"
#include "Coin.h"

void CoinTest()
{
    cout << endl << "COIN TESTS" << endl;

    int testsPass = 0;
    int totalTests = 0;

    {
        Coin coin( "heads", "tails" );
        if      ( coin.m_side1 != "heads" )   { cout << "\033[0;31m [FAIL] "; }
        else if ( coin.m_side2 != "tails" )   { cout << "\033[0;31m [FAIL] "; }
        else                                  { testsPass++; cout << "\033[0;32m [PASS] "; }
        totalTests++;
        cout << "TEST 1: Use the Coin constructor to set the sides' names." << endl;
        cout << "\033[0m";
        cout << "\t Coin coin( \"heads\", \"tails\" );" << endl;
        cout << "\t EXPECTATION:    coin.m_side1 to be \"heads\"" << endl;
        cout << "\t ACTUAL:         coin.m_side1 is    \"" << coin.m_side1 << "\"" << endl;
        cout << "\t EXPECTATION:    coin.m_side2 to be \"tails\"" << endl;
        cout << "\t ACTUAL:         coin.m_side2 is    \"" << coin.m_side2 << "\"" << endl;
        cout << endl;
    }

    {
        Coin coin( "cats", "dogs" );
        if      ( coin.m_side1 != "cats" )   { cout << "\033[0;31m [FAIL] "; }
        else if ( coin.m_side2 != "dogs" )   { cout << "\033[0;31m [FAIL] "; }
        else                                  { testsPass++; cout << "\033[0;32m [PASS] "; }
        totalTests++;
        cout << "TEST 2: Use the Coin constructor to set the sides' names." << endl;
        cout << "\033[0m";
        cout << "\t Coin coin( \"cats\", \"dogs\" );" << endl;
        cout << "\t EXPECTATION:    coin.m_side1 to be \"cats\"" << endl;
        cout << "\t ACTUAL:         coin.m_side1 is    \"" << coin.m_side1 << "\"" << endl;
        cout << "\t EXPECTATION:    coin.m_side2 to be \"dogs\"" << endl;
        cout << "\t ACTUAL:         coin.m_side2 is    \"" << coin.m_side2 << "\"" << endl;
        cout << endl;
    }

    {
        Coin coin( "heads", "tails" );
        string result = coin.Flip();
        if      ( result != "heads" && result != "tails" )  { cout << "\033[0;31m [FAIL] "; }
        else                                                { testsPass++; cout << "\033[0;32m [PASS] "; }
        totalTests++;
        cout << "TEST 3: Coin flip will be one of the two sides." << endl;
        cout << "\033[0m";
        cout << "\t Coin coin( \"heads\", \"tails\" );" << endl;
        cout << "\t coin.Flip();" << endl;
        cout << "\t EXPECTATION:    Result is \"heads\" or \"tails\"" << endl;
        cout << "\t ACTUAL:         Result is \"" << result << "\"" << endl;
        cout << endl;
    }

    {
        Coin coin( "cats", "dogs" );
        string result = coin.Flip();
        if      ( result != "cats" && result != "dogs" )  { cout << "\033[0;31m [FAIL] "; }
        else                                              { testsPass++; cout << "\033[0;32m [PASS] "; }
        totalTests++;
        cout << "TEST 3: Coin flip will be one of the two sides." << endl;
        cout << "\033[0m";
        cout << "\t Coin coin( \"cats\", \"dogs\" );" << endl;
        cout << "\t coin.Flip();" << endl;
        cout << "\t EXPECTATION:    Result is \"cats\" or \"dogs\"" << endl;
        cout << "\t ACTUAL:         Result is \"" << result << "\"" << endl;
        cout << endl;
    }
}
