--------------------------------------------------------

# INTRODUCTION

(Scroll down further for program instructions)

## CLASSES

Like structs, classes are a way we can create our own data type and group related variables together under one name. With classes, we can also add MEMBER FUNCTIONS (aka METHODS) to the class declaration. These functions belong to the class, and are there to operate on the MEMBER VARIABLES within it. (Structs also support functions, but the best practice is to only use structs for very small structures).

Class declarations look like this:

```
class CLASSNAME
{
  public:
    // PUBLIC MEMBERS
  private:
    // PRIVATE MEMBERS
}; // << DON'T FORGET THE ;
```

## UML DIAGRAMS FOR CLASSES

| Day                               | << Class name                            |
| --------------------------------- | ---------------------------------------- |
| `- m_num : int`                   | << Member variables go here              |
| `-m_month : string`               | << "`-`" denotes "private" accessibility |
| --------------------------------- |                                          |
| `+ SetNum( newNum : int ) : void` | << Member functions (aka methods)        |
| `+GetNum() : int`                 |                                          |
| `+Display() : void`               |                                          |

**Parts of a UML diagram:** With UML diagrams, the top section is for the CLASS NAME. The middle contains the MEMBER VARIABLES. And the bottom contains the MEMBER FUNCTIONS (aka METHODS).

**Accessibility levels:** The "-" sign denotes that the member variable or function should be `private`, and the "+" sign denotes `public`.

**NAME : TYPE**  UML items are written in this format:  `VARNAME : TYPE` , this is to be
programming language agnostic, so it can be translated into other languages. You will
have to "translate" the UML to C++ code, this is an important part of software development.

| UML                               | C++ translation            |
| --------------------------------- | -------------------------- |
| `- total_cats : int`              | `int total_cats;`          |
| `+ Display() : void`              | `void Display();`          |
| `+ GetCatCount() : int`           | `int GetCatCount();`       |
| `+ Add( a : int, b : int ) : int` | `int Add( int a, int b );` |

## CLASS MEMBER VARIABLE NAMES

While not required, I prefix my CLASS MEMBER VARIABLES with the `m_` prefix (which stands for "member"). There are a couple of benefits to this:

* Easily tell what variables are MEMBER VARIABLES vs. function parameters or local variables.
* If a function contains a parameter with the same name ("month") you don't have a naming conflict with the member variable ("m_month").

## CLASS MEMBER FUNCTIONS (METHODS) DEFINITIONS

The function definitions for class' member functions looks a little different from functions we've used so far. In particular, EVERY FUNCTION NAME needs to be prefixed with the CLASS NAME and then `::`, the SCOPE RESOLUTION OPERATOR.
For example:

```
void Day::Display()
{
  cout << m_num << " of " << m_month << endl;
}
```

## BUILDING YOUR PROGRAM

use `cd` to navigate to the folder
use `g++` to build the code:

`g++ *.cpp *.h -o PROGRAMNAME.out`

## EXAMPLE PROGRAM OUTPUT

Runner:

```
./program.out run
What's on side A? heads
What's on side B? tails
Flip how many times? 3
Flip #0: heads
Flip #1: tails
Flip #2: heads
```

Tester:

```
./program.out test

COIN TESTS
 [PASS] TEST 1: Use the Coin constructor to set the sides' names.
	 Coin coin( "heads", "tails" );
	 EXPECTATION:    coin.m_side1 to be "heads"
	 ACTUAL:         coin.m_side1 is    "heads"
	 EXPECTATION:    coin.m_side2 to be "tails"
	 ACTUAL:         coin.m_side2 is    "tails"

 [PASS] TEST 2: Use the Coin constructor to set the sides' names.
	 Coin coin( "cats", "dogs" );
	 EXPECTATION:    coin.m_side1 to be "cats"
	 ACTUAL:         coin.m_side1 is    "cats"
	 EXPECTATION:    coin.m_side2 to be "dogs"
	 ACTUAL:         coin.m_side2 is    "dogs"

 [PASS] TEST 3: Coin flip will be one of the two sides.
	 Coin coin( "heads", "tails" );
	 coin.Flip();
	 EXPECTATION:    Result is "heads" or "tails"
	 ACTUAL:         Result is "tails"

 [PASS] TEST 3: Coin flip will be one of the two sides.
	 Coin coin( "cats", "dogs" );
	 coin.Flip();
	 EXPECTATION:    Result is "cats" or "dogs"
	 ACTUAL:         Result is "cats"

```

--------------------------------------------------------

# PROGRAM INSTRUCTIONS

## CLASS DECLARATION

Within **Card.h** a class declaration has already been written:

```
class Coin
{
    public:
    Coin( string newSide1, string newSide2 ); // The text on each side of the coin
    string Flip();                      // Gets the result of flipping the coin

    private:
    string m_side1, m_side2; // What's on side 1 and side 2

    friend void CoinTest();   // This helps the tester (ignore)
};
```

You will be implementing the CLASS METHODS, which are in the corresponding SOURCE (.cpp) FILE.

## CLASS MEMBER FUNCTION DEFINITIONS

1. `Coin::Coin( string newSide1, string newSide2 ) constructor`
    **Input parameters:** `side1` - a string, `side2` - a string
    **Functionality:** The Card class has the member variables `m_side1` and `m_side2`.
      This function takes in parameters `newSide1` and `newSide2`, which are meant to give new values to these variables.
      Copy the values from the parameters to the member variables with a simple assignment statement:
      `m_side1 = newSide1;`

2. `string Coin::Flip()`
    **Input parameters:** none
    **Output return:** void
    **Functionality:** Create a random number between 0 and 1 like this:
    `int random = rand() % 2;`
    Then use an if statement - if the random number is 0, return `m_side1`, otherwise
    return `m_side2`.

## MAIN PROGRAM

The contents of `main()` have also already been written. Running the program requires a second argument, either "run" to run a simple program or "test" to run the unit tests.
You do not need to make any modifications to main.cpp, but look through the file and look at how the class variable object is created and used.
