/* STUDENT NAME:
 * SEMESTER/YEAR:
 * */
#include "Location.h"
#include <iostream>
#include <iomanip>
using namespace std;

// -- CLASS FUNCTION DEFINITIONS -----------------------------------------------------

void Location::Setup( int newLocId, string newAddress, float newTax )
{
  // TODO: Implement me!
}

void Location::PrintTableRow() const
{
  // TODO: Implement me!
}

// -- ADD FUNCTION DEFINITIONS HERE --------------------------------------------------
void SetupLocations( Location& l1, Location& l2 )
{
  // TODO: Implement me!
}

void PrintLocationTable( const Location& l1, const Location& l2 )
{
  cout << left;
  cout << "LOCATIONS" << endl;
  cout
    << setw( 5 ) << "ID"
    << setw( 15 ) << "SALES TAX"
    << setw( 60 ) << "ADDRESS"
    << endl << string( 80, '-' ) << endl;
  
  // TODO: Implement me!
}
