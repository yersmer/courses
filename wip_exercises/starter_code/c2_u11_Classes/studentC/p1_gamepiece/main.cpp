/*
YOU DO NOT NEED TO MODIFY THIS FILE.
*/

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <cstdlib>
#include <ctime>
using namespace std;

#include "CardTests.h"
#include "Card.h"

void CardProgram()
{
  vector<string> ranks = { "A", "2", "3", "4", "5", "6", "7", "8", "9", "J", "Q", "K" };
  vector<char> suits = { 'H', 'D', 'S', 'C' };
  
  int times;
  cout << "Pull how many cards? ";
  cin >> times;
  
  for ( int i = 0; i < times; i++ )
  {
    string rank = ranks[ rand() % ranks.size() ];
    char suit = suits[ rand() % suits.size() ];
    cout << "Card #" << i << ": ";
    Card card;
    card.Setup( rank, suit );
    card.Display();
  }
}

// You don't need to modify main()
int main( int argCount, char* args[] )
{
    srand( time( NULL ) );
    
    if ( argCount < 2 )
    {
      cout << "EXPECTED FORM: " << args[0] << " run/test" << endl;
      return 1;
    }
    
    string action = string( args[1] );
    if ( action == "run" )
    {
      CardProgram();
    }
    else if ( action == "test" )
    {
      CardTest();
    }

    return 0;
}
