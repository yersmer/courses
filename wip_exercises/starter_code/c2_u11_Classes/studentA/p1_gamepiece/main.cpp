/*
YOU DO NOT NEED TO MODIFY THIS FILE.
*/

#include <iostream>
#include <iomanip>
#include <string>
#include <cstdlib>
#include <ctime>
using namespace std;

#include "DieTests.h"
#include "Die.h"

void DiceProgram()
{
  int sides;
  cout << "How many sides? ";
  cin >> sides;
  
  Die die( sides );
  
  int times;
  cout << "Roll how many times? ";
  cin >> times;
  
  for ( int i = 0; i < times; i++ )
  {
    cout << "Roll #" << i << ": " << die.Roll() << endl;
  }
}

// You don't need to modify main()
int main( int argCount, char* args[] )
{
    srand( time( NULL ) );
    
    if ( argCount < 2 )
    {
      cout << "EXPECTED FORM: " << args[0] << " run/test" << endl;
      return 1;
    }
    
    string action = string( args[1] );
    if ( action == "run" )
    {
      DiceProgram();
    }
    else if ( action == "test" )
    {
      DiceTest();
    }

    return 0;
}
