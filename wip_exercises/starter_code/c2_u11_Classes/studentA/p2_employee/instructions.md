--------------------------------------------------------

# INTRODUCTION

(Scroll down further for program instructions)

## BUILDING YOUR PROGRAM

use `cd` to navigate to the folder
use `g++` to build the code:

`g++ *.cpp *.h -o PROGRAMNAME.out`

## EXAMPLE PROGRAM OUTPUT

Runner:

```
./program.out
EMPLOYEES
ID   LOCATION  WAGE      NAME                                              
--------------------------------------------------------------------------------
1    2         15.57     Rhonda W.                                         
2    1         16.97     Fady A.                                           
3    1         15.09     Neha G.   
```

--------------------------------------------------------

# PROGRAM INSTRUCTIONS

## Grab your work from the struct lab

Copy your `main.cpp`, `Employee.h`, and `Employee.cpp` files from your struct lab.
We're going to reuse a lot of this code, but we're turning the `struct` into a `class`.

## CONVERTING Employee FROM STRUCT TO CLASS

Our struct looked like this:

```
struct Employee
{
  int employeeId;
  int locationId;
  string name;
  float hourlyWage;
};
```

But changing it to a class, we need to mark those member variables as `private`,
and create some `public` functions to interface with them. This helps us protect our data.

Update the class to look like this:

```
class Employee
{
  public:
  void Setup( int newEmpId, int newLocId, string newName, float newWage );  
  void PrintTableRow() const;
  
  private:
  int employeeId;
  int locationId;
  string name;
  float hourlyWage;
};
```

We have two functions now: A `Setup` function that takes in values for all the class' information,
and a `PrintTableRow` function that will handle displaying the variables to the screen.

## DEFINING OUR MEMBER FUNCTIONS (METHODS)

### Setup

Within the *.cpp* file, we're going to define our member functions.

Defining a function takes this form:

```
RETURNTYPE CLASSNAME::FUNCTIONNAME( PARAMETERS )
{
}
```

So, for the Setup function, it will start like this:

```
void Employee::Setup( int newEmpId, int newLocId, string newName, float newWage )
{
}
```

Notice that we've put `Employee::` before the function name `Setup`.

Within this function, we want to give new values to our *private member variables*:
`employeeId`, `locationId`, `name`, and `hourlyWage`.

The function takes in several *input parameters*, carrying new values:
`newEmpId`, `newName`, and `newWage`.

With each *private member variable*, write an assignment statement to set its new value
to the corresponding *input parameter variable*.


### PrintTableRow

For this function, cut part of the `PrintEmployeesTable` function where it's didsplaying
one of the items, like:

```
  cout
    << setw( 5 ) << e1.employeeId
    << setw( 10 ) << e1.locationId
    << setw( 10 ) << e1.hourlyWage
    << setw( 50 ) << e1.name
    << endl;
```

Paste it into the `PrintTableRow` function. We don't need the "`e1.`" anymore, so remove that
part, but the rest of it should be good for displaying the variables.


```
void Employee::PrintTableRow() const
{
  cout
    << setw( 5 ) << employeeId
    << setw( 10 ) << locationId
    << setw( 10 ) << hourlyWage
    << setw( 50 ) << name
    << endl;
}
```

## UPDATING THE FUNCTIONS FROM BEFORE

### SetupEmployees

Before, we were setting the member variables directly, like this:

```
  e1.employeeId = 1;
  e1.locationId = 2;
  e1.name = "Rhonda W.";
  e1.hourlyWage = 15.57;
```

Now those variables are private, so they must be set by calling the `Setup` function:

```
e1.Setup( VALUE1, VALUE2, VALUE3, VALUE4 );`
```

Change all of the variables being set up (`e1`, `e2`, `e3`) to use the Setup function instead,
using the same data as before.

### PrintEmployeeTable

For this function we will keep the header part of the table display, but we don't need
to have cout statements for `e1`, `e2`, and `e3` anymore. Instead, we call each of their
`PrintTableRow` functions:

```
e1.PrintTableRow();
```

Do this for all variables.


