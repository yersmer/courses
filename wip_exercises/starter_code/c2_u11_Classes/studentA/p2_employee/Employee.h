/* STUDENT NAME:
 * SEMESTER/YEAR:
 * */
#ifndef _EMPLOYEE
#define _EMPLOYEE

#include <string>
using namespace std;

// -- ADD STRUCT DECLARATION HERE --------------------------------------------------

class Employee
{
  public:
  void Setup( int newEmpId, int newLocId, string newName, float newWage );  
  void PrintTableRow() const;
  
  private:
  int employeeId;
  int locationId;
  string name;
  float hourlyWage;
};

// -- ADD FUNCTION DECLARATIONS HERE --------------------------------------------------

void SetupEmployees( Employee& e1, Employee& e2, Employee& e3 );
void PrintEmployeeTable( const Employee& e1, const Employee& e2, const Employee& e3 );

#endif
