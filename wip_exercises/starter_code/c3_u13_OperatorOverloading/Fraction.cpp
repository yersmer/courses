#include "Fraction.h"

#include <istream>
#include <ostream>
using namespace std;

Fraction::Fraction()
{
  m_num = 1;
  m_denom = 1;
}

float Fraction::GetDecimal() const
{
  return -1; // TEMPORARY PLACEHOLDER - REMOVE ME ONCE YOU IMPLEMENT THIS FUNCTION!
}

Fraction Fraction::CommonDenominatorize( const Fraction& other ) const
{
  return Fraction(); // TEMPORARY PLACEHOLDER - REMOVE ME ONCE YOU IMPLEMENT THIS FUNCTION!
}

Fraction& Fraction::operator=( const Fraction& other )
{
  return *this;
}

Fraction operator+( const Fraction& left, const Fraction& right )
{
  return Fraction(); // TEMPORARY PLACEHOLDER - REMOVE ME ONCE YOU IMPLEMENT THIS FUNCTION!
}

Fraction operator-( const Fraction& left, const Fraction& right )
{
  return Fraction(); // TEMPORARY PLACEHOLDER - REMOVE ME ONCE YOU IMPLEMENT THIS FUNCTION!
}

Fraction operator*( const Fraction& left, const Fraction& right )
{
  return Fraction(); // TEMPORARY PLACEHOLDER - REMOVE ME ONCE YOU IMPLEMENT THIS FUNCTION!
}

Fraction operator/( const Fraction& left, const Fraction& right )
{
  return Fraction(); // TEMPORARY PLACEHOLDER - REMOVE ME ONCE YOU IMPLEMENT THIS FUNCTION!
}

bool operator==( const Fraction& left, const Fraction& right )
{
  return false; // TEMPORARY PLACEHOLDER - REMOVE ME ONCE YOU IMPLEMENT THIS FUNCTION!
}

bool operator!=( const Fraction& left, const Fraction& right )
{
  return false; // TEMPORARY PLACEHOLDER - REMOVE ME ONCE YOU IMPLEMENT THIS FUNCTION!
}

bool operator<=( const Fraction& left, const Fraction& right )
{
  return false; // TEMPORARY PLACEHOLDER - REMOVE ME ONCE YOU IMPLEMENT THIS FUNCTION!
}

bool operator>=( const Fraction& left, const Fraction& right )
{
  return false; // TEMPORARY PLACEHOLDER - REMOVE ME ONCE YOU IMPLEMENT THIS FUNCTION!
}

bool operator<( const Fraction& left, const Fraction& right )
{
  return false; // TEMPORARY PLACEHOLDER - REMOVE ME ONCE YOU IMPLEMENT THIS FUNCTION!
}

bool operator>( const Fraction& left, const Fraction& right )
{
  return false; // TEMPORARY PLACEHOLDER - REMOVE ME ONCE YOU IMPLEMENT THIS FUNCTION!
}

ostream& operator<<( ostream& out, const Fraction& fraction )
{
  return out;
}

istream& operator>>( istream& in, Fraction& fraction )
{
  return in;
}

