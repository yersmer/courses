#ifndef _PROGRAM_H
#define _PROGRAM_H

#include "Fraction.h"

class Program
{
  public:
  Program();
  void Run();
  void Menu_Initialization();
  void Menu_Arithmetic();
  void Menu_Relational();

  private:
  Fraction m_fractions[2];
  const int FRACTION_COUNT;

  void DisplayFractions();
};

#endif
