#include "Product.h"

Product::Product()
{
	Setup("UNSET", 0, 0);
}

Product::Product(std::string newName, int newQuantity, float newPrice)
{
	Setup(newName, newQuantity, newPrice);
}

Product::Product(const Product& copyMe)
{
	Setup(copyMe.GetName(), copyMe.GetQuantityInStock(), copyMe.GetPrice());
}

//void Product::Setup(std::string newName)
//{
//	Setup(newName, 0, 0);
//}

void Product::Setup(std::string newName /*= "unset"*/, int newQuantity /*= 0*/, float newPrice /*= 0*/)
{
	m_name = newName;
	m_quantityInStock = newQuantity;
	m_price = newPrice;
}

std::string Product::GetName() const
{
	return m_name;
}

int Product::GetQuantityInStock() const
{
	return m_quantityInStock;
}

float Product::GetPrice() const
{
	return m_price;
}