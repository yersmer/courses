#ifndef _INGREDIENT
#define _INGREDIENT

#include <string>
#include <iostream>
#include <iomanip>
using namespace std;

struct Ingredient
{
  string name;
  float amount;
  string unit;
};

#endif
