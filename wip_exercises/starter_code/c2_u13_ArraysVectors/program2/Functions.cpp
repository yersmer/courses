#include "Functions.h"

#include <string>
#include <iostream>
#include <iomanip>
using namespace std;

void DisplayAllIngredients( const vector<Ingredient>& ingredients )
{
  cout << "INGREDIENTS" << endl;
  cout << left << setw( 5 ) << "ID" << setw( 30 ) << "INGREDIENT" << setw( 5 ) << "#" << setw( 10 ) << "UNIT" << endl;
  cout << string( 80, '-' ) << endl;

  // TODO: Display ingredients
}

void AddIngredient( vector<Ingredient>& ingredients )
{
  cout << endl << string( 10, '-' ) << " ADD INGREDIENT " << string( 20, '-' ) << endl;
  string newName, newUnit;
  float newAmount;
  cin.ignore();

  // TODO: Set up and add new ingredient

  PressEnterToContinue();
}

void EditIngredient( vector<Ingredient>& ingredients )
{
  cout << endl << string( 10, '-' ) << " EDIT INGREDIENT " << string( 20, '-' ) << endl;

  // TODO: Ask user which ingredient and what field to edit, let them make updates.

  PressEnterToContinue();
}

void DisplayMainMenu()
{
  cout << endl << "OPTIONS" << endl;
  cout << "1. Add ingredient" << endl;
  cout << "2. Edit ingredient" << endl;
  cout << "3. Exit" << endl;
}

int GetChoice( int min, int max )
{
  int choice;
  cout << ">> ";
  cin >> choice;

  while ( choice < min || choice > max )
    {
      cout << "Invalid selection! Try again!" << endl;
      cout << ">> ";
      cin >> choice;
    }
  return choice;
}

void PressEnterToContinue()
{
  string a;
  cout << "PRESS ENTER TO CONTINUE" << endl;
  cin.ignore();
  getline( cin, a );
}
