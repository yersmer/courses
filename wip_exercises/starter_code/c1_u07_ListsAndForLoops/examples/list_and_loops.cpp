#include <iostream>
#include <vector>
using namespace std;

int main()
{
  vector<string> classes = { "CS134", "CS200", "CS210" };

  cout << "The size of the classes vector is: " << classes.size() << endl;

  // LOOP STYLE 1:
  cout << endl << "LOOP STYLE 1:" << endl;
  for ( int i = 0; i < classes.size(); i++ )
  {
    cout << "Item #" << i << " is: " << classes[i] << endl;    
  }

  // LOOP STYLE 2:
  cout << endl << "LOOP STYLE 2:" << endl;
  for (auto& item : classes)
  {
      cout << "Item: " << item << endl;
  }
  
  return 0;
}