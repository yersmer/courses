# 1. Create a variable named `running` and assign it to True (a boolean value).


# 2. Create a while loop that continues running while `running == True`. Within the while loop, do the following:


#  2a. Display a main menu of options. Option 1 is show favorite movie, option 2 is show favorite book, option 3 is quit.


#  2b. Ask the user for their selection. Store their integer input in a variable `choice`.


#  2c. If `choice` is 1, display your favorite movie.


#  2d. Else, if `choice` is 2, display your favorite book.


#  2e. Else, if `choice` is 3, set `running = False`.


# Say Goodbye at the end of the program!
print( "\n GOODBYE!" )