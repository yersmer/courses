var indexSectionsWithContent =
{
  0: "_abcdefghiklmnoprstuw~",
  1: "bcdilmnpst",
  2: "cdeu",
  3: "bcdilmnprst",
  4: "abcdefghimnoprstw~",
  5: "_cdkmnprst",
  6: "b",
  7: "c"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "related",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Friends",
  7: "Pages"
};

