var indexSectionsWithContent =
{
  0: "abcdefghiklmnopqrstuw~",
  1: "dehilmnpst",
  2: "cdeu",
  3: "dehilmnprst",
  4: "abcdefghilmnopqrstw~",
  5: "cdkmnstu",
  6: "c",
  7: "dlq",
  8: "h",
  9: "c"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "related",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Friends",
  9: "Pages"
};

