/*
* BinarySearchTreeTester.h
* Using cutest v2023-04
* RW Singh
*
*
* ADD TESTS FOR POP, POP ROOT, ADDING PARENT IN PUSH FUNCTION, ETC.
*/

#ifndef _BINARY_SEARCH_TREE_TESTER_HPP
#define _BINARY_SEARCH_TREE_TESTER_HPP

// Project includes
#include "BinarySearchTreeNode.h"
#include "BinarySearchTree.h"
#include "../../CuTest/TesterBase.h"

namespace DataStructure
{

//! TESTER for the BinarySearchTree
class BinarySearchTreeTester : public cutest::TesterBase
{
public:
    BinarySearchTreeTester()
        : TesterBase( "test_result_binary_search_tree.html" )
    {
        AddTest(cutest::TestListItem("NodeConsructor()",    std::bind(&BinarySearchTreeTester::Test_NodeConstructor, this)));
        AddTest(cutest::TestListItem("TreeConstructor()",   std::bind(&BinarySearchTreeTester::Test_TreeConstructor, this)));
        AddTest(cutest::TestListItem("Push()",              std::bind(&BinarySearchTreeTester::Test_Push, this)));
        AddTest(cutest::TestListItem("Contains()",          std::bind(&BinarySearchTreeTester::Test_Contains, this)));
        AddTest(cutest::TestListItem("FindNode()",          std::bind(&BinarySearchTreeTester::Test_FindNode, this)));
//        AddTest(cuTest::TestListItem("FindParentOfNode()",  std::bind(&BinarySearchTreeTester::Test_FindParentOfNode, this)));
        AddTest(cutest::TestListItem("GetInOrder()",        std::bind(&BinarySearchTreeTester::Test_GetInOrder, this)));
        AddTest(cutest::TestListItem("GetPreOrder()",       std::bind(&BinarySearchTreeTester::Test_GetPreOrder, this)));
        AddTest(cutest::TestListItem("GetPostOrder()",      std::bind(&BinarySearchTreeTester::Test_GetPostOrder, this)));
        AddTest(cutest::TestListItem("GetMinKey()",         std::bind(&BinarySearchTreeTester::Test_GetMinKey, this)));
        AddTest(cutest::TestListItem("GetMaxKey()",         std::bind(&BinarySearchTreeTester::Test_GetMaxKey, this)));
        AddTest(cutest::TestListItem("GetCount()",          std::bind(&BinarySearchTreeTester::Test_GetCount, this)));
        AddTest(cutest::TestListItem("GetHeight()",         std::bind(&BinarySearchTreeTester::Test_GetHeight, this)));
//        AddTest(cuTest::TestListItem("Delete()",            std::bind(&BinarySearchTreeTester::Test_Delete, this)));

    }

    virtual ~BinarySearchTreeTester() { }

private:
    int Test_NodeConstructor();
    int Test_TreeConstructor();
    int Test_Push();
    int Test_Contains();
    int Test_FindNode();
//    int Test_FindParentOfNode();
    int Test_GetInOrder();
    int Test_GetPreOrder();
    int Test_GetPostOrder();
    int Test_GetMinKey();
    int Test_GetMaxKey();
    int Test_GetCount();
    int Test_GetHeight();
//    int Test_Delete();
};
//


} // End of namespace

#endif
