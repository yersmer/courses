#ifndef _PROGRAM
#define _PROGRAM

#include "../DataStructure/BinarySearchTree/BinarySearchTree.h"
#include "Call.h"
#include "DataGen.h"

#include <map> // temporary
#include <string>
#include <fstream>

class Program
{
public:
	void Run();
	void SetDataPath(std::string path);

private:
	void Setup();
	void Cleanup();

	void ReceiveCall();
	void ResolveCall();
	void RequeueCall();
	void SaveSummary();

	void SetupLog();
	void CleanupLog();

	// DataStructure::BinarySearchTree<int, Call*> m_callPriorityQueue;
	std::vector<Call*> m_callList;

	int m_hour, m_minute, m_timePass;

	std::string m_dataPath;
	std::ofstream m_log;

	DataGen m_generator;
};

#endif
