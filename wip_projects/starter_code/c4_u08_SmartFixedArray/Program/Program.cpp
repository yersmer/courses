#include "Program.h"

#include "../Utilities/Menu.h"

#include <iostream>
#include <iomanip>
#include <fstream>

void Program::SetDataPath(std::string path)
{
	m_dataPath = path;
	std::cout << "Program data should be located at \"" << m_dataPath << "\"..." << std::endl;
}

void Program::Setup()
{
	// Load in the data file
	std::ifstream input(m_dataPath);
	if (input.fail())
	{
		std::cout << "ERROR! Could not find data file at \"" << m_dataPath << "\"! Please double check the path!" << std::endl;
		return;
	}
	int size;
	input >> size;

	for (int i = 0; i < size; i++)
	{
		std::string name;
		float price;
		int quantity;

		input.ignore();
		std::getline(input, name);
		input >> price;
		input >> quantity;

		m_products.push_back(Product(name, price, quantity)); // TODO: Update this to use your SmartFixedArray
	}
}

void Program::Cleanup()
{
	// Save data
	std::ofstream output(m_dataPath);
	output << m_products.size() << std::endl; // TODO: Update this to use your SmartFixedArray

	for (size_t i = 0; i < m_products.size(); i++) // TODO: Update this to use your SmartFixedArray
	{
		output << m_products[i].GetName() << std::endl; // TODO: Update this to use your SmartFixedArray
		output << m_products[i].GetPrice() << std::endl; // TODO: Update this to use your SmartFixedArray
		output << m_products[i].GetQuantity() << std::endl; // TODO: Update this to use your SmartFixedArray
	}
}

void Program::Run()
{
	Setup();
	Menu_Main();
	Cleanup();
}

void Program::Menu_Main()
{
	bool done = false;
	while (!done)
	{
		Utility::Menu::Header("MAIN MENU");

		DisplayProducts(m_products);

		std::cout << std::endl << "OPTIONS" << std::endl;
		int choice = Utility::Menu::ShowIntMenuWithPrompt({
			"Add product",
			"Edit product",
			"Delete product",
			}, true, true);

		switch (choice)
		{
		case 0:
			done = true;
			break;

		case 1:
			Menu_AddProduct();
			break;

		case 2:
			Menu_EditProduct();
			break;

		case 3:
			Menu_DeleteProduct();
			break;
		}

	}
}

void Program::Menu_AddProduct()
{
	Utility::Menu::Header("ADD NEW PRODUCT");

	std::string name;
	float price;
	int quantity;

	std::cout << "Enter new product's NAME: ";
	std::cin.ignore();
	getline(std::cin, name);

	std::cout << "Enter new product's PRICE: $";
	std::cin >> price;

	std::cout << "Enter new product's QUANTITY: ";
	std::cin >> quantity;

	std::cout << std::endl << "Insert where in list?" << std::endl;
	int position = Utility::Menu::ShowIntMenuWithPrompt({
		"Front",
		"Middle",
		"Back"
		});

    Product newProduct( name, price, quantity );
    switch( position )
    {
        case 1: // Front
        {
            int index = 0;
            m_products.insert( m_products.begin()+index, newProduct ); // TODO: Update this to use your SmartFixedArray
            break;
        }

        case 2: // Middle
        {
            int index;
            DisplayProducts( m_products );
            index = Utility::Menu::GetValidChoice( 0, m_products.size() - 1, "Enter ID of location to put product: " );
            m_products.insert( m_products.begin()+index, newProduct ); // TODO: Update this to use your SmartFixedArray
            break;
        }

        case 3: // Back
        {
            m_products.push_back( newProduct ); // TODO: Update this to use your SmartFixedArray
            break;
        }
    }


	std::cout << std::endl << "Product added." << std::endl;

	Utility::Menu::Pause();
}

void Program::Menu_EditProduct()
{
	Utility::Menu::Header("EDIT EXISTING PRODUCT");

	DisplayProducts(m_products);

	std::cout << std::endl << "Edit which item in list?" << std::endl;
	int position = Utility::Menu::ShowIntMenuWithPrompt({
		"Front",
		"Middle",
		"Back"
		}, true, true );

    Product& productRef = m_products[0];
    switch( position )
    {
        case 1: // Front
        {
            productRef = m_products[0]; // TODO: Update this to use your SmartFixedArray
            break;
        }

        case 2: // Middle
        {
            int index;
            DisplayProducts( m_products );
            index = Utility::Menu::GetValidChoice( 0, m_products.size() - 1, "Enter ID of product to edit: " );
            productRef = m_products[index]; // TODO: Update this to use your SmartFixedArray
            break;
        }

        case 3: // Back
        {
            productRef = m_products[ m_products.size()-1 ]; // TODO: Update this to use your SmartFixedArray
            break;
        }

        case 0: // Quit
        {
            return;
            break;
        }
    }

    std::cout << std::endl << "Product to edit:" << std::endl;
    productRef.Display();

	std::cout << std::endl << "Edit which field?" << std::endl;
	int field = Utility::Menu::ShowIntMenuWithPrompt({
		"Name",
		"Price",
		"Quantity"
		}, true, true );

	switch (field)
	{
		case 1:
		{
			std::string newName;
			std::cout << "Enter new name: ";
			std::cin.ignore();
			std::getline(std::cin, newName);
			productRef.SetName(newName);
			break;
		}

		case 2:
		{
			float newPrice;
			std::cout << "Enter new price: $";
			std::cin >> newPrice;
			productRef.SetPrice(newPrice);
			break;
		}

		case 3:
		{
			int newQuantity;
			std::cout << "Enter new quantity: ";
			std::cin >> newQuantity;
			productRef.SetQuantity(newQuantity);
			break;
		}

        case 0: // Quit
        {
            return;
            break;
        }
	}

	std::cout << std::endl << "Product updated." << std::endl;

	Utility::Menu::Pause();
}

void Program::Menu_DeleteProduct()
{
	Utility::Menu::Header("DELETE PRODUCT");

	DisplayProducts(m_products);

	std::cout << std::endl << "Edit which item in list?" << std::endl;
	int position = Utility::Menu::ShowIntMenuWithPrompt({
		"Front",
		"Middle",
		"Back"
		}, true, true );

    Product& productRef = m_products[0];
    switch( position )
    {
        case 1: // Front
        {
            int index = 0;
            m_products.erase(m_products.begin() + index); // TODO: Update this to use your SmartFixedArray
            break;
        }

        case 2: // Middle
        {
            int index;
            DisplayProducts( m_products );
            index = Utility::Menu::GetValidChoice(0, m_products.size() - 1, "Enter ID of item to remove: ");
            m_products.erase(m_products.begin() + index); // TODO: Update this to use your SmartFixedArray
            break;
        }

        case 3: // Back
        {
            int index = m_products.size() - 1;
            m_products.erase(m_products.begin() + index); // TODO: Update this to use your SmartFixedArray
            break;
        }

        case 0: // Quit
        {
            return;
            break;
        }
    }

	std::cout << std::endl << "Product removed." << std::endl;

	Utility::Menu::Pause();
}

void Program::DisplayProducts(const std::vector<Product>& productList)
{
	std::cout << std::left;
	std::cout << std::setw(5) << "ID"
		<< std::setw(50) << "NAME"
		<< std::setw(10) << "PRICE"
		<< std::setw(10) << "QUANTITY"
		<< std::endl << std::string(80, '-') << std::endl;

	for (size_t i = 0; i < m_products.size(); i++) // TODO: Update this to use your SmartFixedArray
	{
		std::cout << std::setw(5) << i;
		m_products[i].Display(); // TODO: Update this to use your SmartFixedArray
	}
}
