#ifndef _DATAGEN
#define _DATAGEN

#include <string>
#include <vector>
#include <fstream>
#include <iostream>

class DataGen
{
public:
    DataGen()
    {
    }

	void Setup(std::string dataPath)
	{
		m_emails.open(dataPath + "emails.txt");
		m_names.open(dataPath + "names.txt");
	}

	std::string GetEmail()
	{
		std::string buffer;
		getline(m_emails, buffer);
		return buffer;
	}

	std::string GetName()
	{
		std::string buffer;
		getline(m_names, buffer);
		return buffer;
	}

	char GetLetter()
	{
		char letter = char( rand() % 25 + 65 );
		return letter;
	}

private:
	std::ifstream m_emails;
	std::ifstream m_names;
};

#endif
