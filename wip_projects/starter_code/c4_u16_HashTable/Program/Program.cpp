#include "Program.h"

#include "../Utilities/Menu.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <functional>
#include <string>

Program::Program()
	: m_employees(101)
{
}

void Program::SetDataPath(std::string path)
{
	m_dataPath = path;
	std::cout << "Program data should be located at \"" << m_dataPath << "\"..." << std::endl;
}

void Program::Setup()
{
	m_dataGen.Setup(m_dataPath);
	std::hash<std::string> stringHash;

	std::cout << std::left;

	// Create employees
}

void Program::Cleanup()
{
}

void Program::Run()
{
	Setup();

	std::cout << std::endl << std::string(80, '-') << std::endl;
	std::cout << "LOGIN" << std::endl << std::endl;
	std::cout << "Enter employee ID: ";
	size_t id;
	std::cin >> id;

	// Login process
}
