#include <iostream>
#include <string>
#include <array>
#include <vector>
using namespace std;



/*
class Product
{
    private:
    //member variables
    string m_title;
    float m_price;
    
public:
    //Member functions/methods
    void Setup(string title, float price);
    void ReducePrice(float amount);

    
};


 Write a class declaration (NOT the definitions) of a class that meets the following requirements:
     The class is named Product.
     It has the following private member variables:
         m_title, a string
         m_price, a float
     It has the following public member functions:
         Setup, which takes in parameters for each member variable to set up their values. It returns nothing.
         ReducePrice, which takes in a float amount and returns nothing.

 */
