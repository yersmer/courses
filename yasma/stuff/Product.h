#ifndef Product_h
#define Product_h

#include <string>
using namespace std;

class Product
{
    public:
    //! Assigns the class' member variable values from the corresponding parameters
    void Setup( string new_name, float new_price );
    
    //! Returns the corresponding class member variable
    string GetName();
    
    //! Returns the corresponding class member variable
    float GetPrice();
    
    private:
    string name;
    float price;
};

#endif

