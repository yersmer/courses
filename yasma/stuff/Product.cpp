#include <iostream>
#include <string>
#include <array>
#include <vector>
using namespace std;
#include "Product.h"

/*
 
 Write a Product.cpp file and implement the Setup, GetName, and GetPrice functions.
 To test properly, you may want to create a main.cpp, include the .h file.
 Declare an object variable of type Product and call each function.

 */
void Product::Setup( string new_name, float new_price )
{
    name = new_name;
    price = new_price;
}

string Product::GetName()
{
    return name;
}

float Product::GetPrice()
{
    return price;
}


