#include <iostream>
#include <string>
using namespace std;
#include "Product.h"

int main()
{
    /*
     Write a Product.cpp file and implement the Setup, GetName, and GetPrice functions.
     To test properly, you may want to create a main.cpp, include the .h file.
     Declare an object variable of type Product and call each function.
     */
    string name = "Papaya";
    float price = 2.13;
    
    Product product;
    product.Setup(name, price);
    
    cout << "The product of the year is: " << product.GetName() << endl;
    cout << "And it's price is: " << product.GetPrice() << endl;

}
